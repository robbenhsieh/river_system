<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header ">
      <h4 class="modal-title"><span id="account_title"></span>帳號設定</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modal_user_close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row">
            <div class="col-lg-4 mt-3">
              <label>帳號</label>
              <input type="text" class="form-control m-input" id="user_account" disabled="disabled">
            </div>
            <div class="col-lg-4 mt-3">
              <label class="">密碼</label>
              <input type="password" class="form-control m-input" id="user_password" >
            </div>
            <div class="col-lg-4 mt-3">
              <label>用戶層級</label>
              <select class="form-control m-input" id="user_group_select">
                <option value="administrator">系統管理員</option>
                <option value="user">一般用戶</option>
              </select>
            </div>
            <div class="col-lg-4 mt-3">
              <label>姓名</label>
              <input type="text" class="form-control m-input" placeholder="請輸入姓名" id="user_name">
            </div>
            <div class="col-lg-4 mt-3">
              <label class="">E-mail</label>
              <input type="text" class="form-control m-input" placeholder="請輸入電子郵件" id="user_email">
            </div>
            <div class="col-lg-4 mt-3">
              <label class="">聯絡電話</label>
              <input type="text" class="form-control m-input" placeholder="請輸入聯絡電話" id="user_phone">
            </div>
            <!--<div class="col-lg-4 mt-3">
              <label>性別</label>
              <div class="m-radio-inline" id="user_gender_radio_group">
                <label class="m-radio">
                  <input type="radio" name="gender" value="Male" id="gender_male">男
                  <span></span>
                </label>
                <label class="m-radio">
                  <input type="radio" name="gender" value="Female" id="gender_female" >女
                  <span></span>
                </label>
              </div>
            </div>-->
            <div class="col-lg-4 mt-3">
              <label class="">帳號狀態</label>
                <div class="m-radio-inline" id="user_active_radio_group">
                  <label class="m-radio">
                    <input type="radio" name="active" value="1" id="active_true">啟用
                    <span></span>
                  </label>
                  <label class="m-radio">
                    <input type="radio" name="active" value="0" id="active_false" >停用
                    <span></span>
                  </label>
                </div>
              </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-12 m-form__group-sub">

              <label class="form-control-label">訊息接收</label>
              <div class="m-checkbox-inline">
                <label class="m-checkbox m-checkbox--success">
                  <input type="checkbox" name="" id="ac_gateway_type_26">監測站通道狀態
                  <span></span>
                </label>
                <label class="m-checkbox m-checkbox--success">
                  <input type="checkbox" name="" id="ac_sensor_type_43">水位計
                  <span></span>
                </label>
                <label class="m-checkbox m-checkbox--success">
                  <input type="checkbox" name="" id="ac_sensor_type_44">加速度計
                  <span></span>
                </label>
                <label class="m-checkbox m-checkbox--success">
                  <input type="checkbox" name="" id="ac_sensor_type_45">傾斜儀
                  <span></span>
                </label>
                <label class="m-checkbox m-checkbox--success">
                  <input type="checkbox" name="" id="ac_sensor_type_46">地震儀
                  <span></span>
                </label>
                <label class="m-checkbox m-checkbox--success">
                  <input type="checkbox" name="" id="maintain">維護提醒
                  <span></span>
                </label>
              </div>
              <span class="m-form__help">請勾選欲收到警訊的裝置種類，若有異常發生，系統將會傳送訊息至您的E-mail，請確認您的mail可正常使用。</span>
            </div>
          </div>
        </div>
      </form>
      <!--end::Form-->
    </div>    
    <div class="modal-footer">      
      <div class="modal_footer_action_btn">
        <!--<button type="button" class="btn btn-danger" data-dismiss="modal" id="adm_remove_user" onclick="RemoveUser();">
	        <span class="pl-1">移除</span>
        </button>-->
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
	        <span class="pl-1">取消</span>
        </button>
        <button type="button" class="btn btn-primary" id="adm_upadte_userinfo" onclick="UpdateUserInfo();">
          <i class="la la-save"></i>
          <span class="pl-1">儲存更新</span>
        </button>
      </div>
    </div>
  </div>
</div>