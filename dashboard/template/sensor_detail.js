<div class="info_content">
  <div class="info_item flex_left item_basic" >
    <span class="item_title" id="detail_info_name" style="display: none;"></span>
    <div class="info_table_list">
      <table class="table table-striped table-bordered">
        <thead>
        </thead> 
        <tbody>
          <tr>
            <th style="background-color: transparent" id="detail_info_">監測站</th> 
            <td id="detail_info_gateway"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="detail_info_">區域</th> 
            <td id="detail_info_area"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="">管理機關</th> 
            <td id="detail_info_admin"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="">河川別</th>
            <td id="detail_info_river"></td>
          </tr>
          <tr>
            <th style="background-color: transparent" id="">岸別</th> 
            <td id="detail_info_bank"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="">TWD97座標X</th> 
            <td id="detail_info_X_97"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="">TWD97座標Y</th> 
            <td id="detail_info_Y_97"></td>
          </tr> 
        </tbody>
      </table>
    </div>

  </div>
  <div class="table_loading" id="an_loading">
      <span>資料讀取中，請稍候</span>
      <span>
        <div class = "m-loader m-loader--brand" ></div> 
      </span>    
  </div>
  <div class="info_item item_table" id="detail_page">
    <span class="item_title" id="detail_table_title">數據資料</span>
    <div class="item_content" id="detail_page_table">
      <table class="table table-striped- table-bordered table-hover table-checkable" style="white-space: nowrap;" id="history_table">
        <thead>
          <tr id="history_table_list_title">
          </tr>
        </thead>
        <tbody id="history_table_list">
        </tbody>
      </table>
    </div>
    <span class="item_title" id="detail_table_desc"></span>
  </div>
  <div class="info_item item_chart" id="show_item_chart">
    <div class="chart_modal float-right" id="chart_view"><a href="" data-toggle="modal" data-target="#m_modal_chart" onclick="ShowReportChart();"><i class="fa fa-search-plus"></i>放大顯示</a></div>
    <!-- <div class="float-left">
      <div class="m-radio-inline" id="hours_radio_group" >
        <label class="m-radio">
          <input type="radio" name="active" value="1" id="three_hours" onchange="ChangeWaterPressureReport();">3小時
          <span></span>
        </label>
        <label class="m-radio">
          <input type="radio" name="active" value="0" id="six_hours" onchange="ChangeWaterPressureReport();">6小時
          <span></span>
        </label>
        <label class="m-radio">
          <input type="radio" name="active" value="0" id="twelve_hours" onchange="ChangeWaterPressureReport();">12小時
          <span></span>
        </label>
      </div>
    </div>-->
    <div style="clear:both"></div>
    <div class="item_content">
      <div id="sensor_report" style="height: 250px;">
      </div>
    </div>
    <div class="item_content">
      <span id="sensor_remark"></span>
    </div>
  </div>
</div>
