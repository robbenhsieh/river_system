<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header ">
            <h4 class="modal-title">堤防檢查表資訊</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="tab-pane active" id="modal_tab_checklist_01" role="tabpanel">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="500">
                    <div class="data_table_list">
                        <table class="table table-striped table-bordered">
                            <thead>
                            </thead> 
                            <tbody>
                                <tr>
                                    <th>管理單位</th> 
                                    <td id="modal_cl_admin_text"></td>
                                </tr> 
                                <tr>
                                    <th>堤防名稱</th> 
                                    <td id="modal_cl_embankment_text"></td>
                                </tr> 
                                <tr>
                                    <th>檢查表檔案</th>
                                    <td id="check_list_files">
                                        <div>
                                            <span>
                                                <a href="#" data-toggle="modal" data-target="#m_modal_checklist_img" id="file_btn_id" onclick="ShowModalCheckListFile(this);">
                                                    <span>
                                                        <i class="fa fa-eye"></i>
                                                        <span>查看</span>
                                                    </span>
                                                </a>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>堤防影像</th>
                                    <td id="check_list_images">
                                        <div>
                                            <span>
                                                <a href="#" data-toggle="modal" data-target="#m_modal_checklist_img" id="image_btn_id" onclick="ShowModalCheckListImage(this);">
                                                    <span>
                                                        <i class="fa fa-eye"></i>
                                                        <span>查看</span>
                                                    </span>
                                                </a>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>堤防型式</th> 
                                    <td id="modal_cl_type_text"></td>
                                </tr> 
                                <tr>
                                    <th>檢查日期</th> 
                                    <td id="modal_cl_time_text"></td>
                                </tr> 
                                <tr>
                                    <th>檢查樁號</th> 
                                    <td id="modal_cl_pile_text"></td>
                                </tr> 
                                <tr>
                                    <th>所屬縣市</th> 
                                    <td id="modal_cl_river_text"></td>
                                </tr> 
                                <tr>
                                    <th>位置</th> 
                                    <td>
                                        <div>
                                            <span>97坐標X: <span id="modal_cl_embankment_X"></span></span>
                                        </div>
                                        <div>
                                            <span>97坐標Y: <span id="modal_cl_embankment_Y"></span></span>
                                        </div>
                                    </td>
                                </tr> 
                                <tr>
                                    <th>檢查範圍</th> 
                                    <td id="modal_cl_inspection_text"></td>
                                </tr> 
                                <tr>
                                    <th>安全等級</th> 
                                    <td id="modal_cl_security_text"></td>
                                </tr> 
                                <tr>
                                    <th>問題描述</th> 
                                    <td id="modal_cl_description_textarea">文字描述</td>
                                </tr>
                                <tr>
                                    <th>改善方式</th> 
                                    <td id="modal_cl_improve_textarea">文字描述</td>
                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="modal_footer_action_btn">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
            </div>
        </div>
    </div>
</div>