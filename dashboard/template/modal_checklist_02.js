<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header ">
            <h4 class="modal-title">堤防檢查表資訊</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modal_cl_close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="500">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 mt-3">
                                <label>檢查日期</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input" readonly placeholder="請選擇日期" id="m_datepicker_checkday" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>管理單位*</label>
                                <select class="form-control m-input" id="modal_cl_admin_select">
                                    <option value="0">必填</option>
                                    <option value="十河局">十河局</option>
                                </select>
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>縣市別*</label>
                                <select class="form-control m-input" id="modal_cl_river_select" onchange="OnModalRiverSelectChange(this);">
                                    <option value="0">必填</option>
                                    <option value="1">河川1</option>
                                    <option value="1">河川2</option>
                                </select>
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>堤防型式*</label>
                                <select class="form-control m-input" id="modal_cl_type_select" placeholder="">
									<option value="0">必填</option>
                                    <option value="土堤">土堤</option>
                                    <option value="防洪牆">防洪牆</option>
                                    <option value="石籠護岸">石籠護岸</option>
                                    <option value="混合式堤防">混合式堤防</option>
                                </select>
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>堤防樁位</label>
                                <input type="text" class="form-control m-input" placeholder="" id="modal_cl_pile_text">
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>堤防名稱*</label>
                                <select class="form-control m-input" id="modal_cl_embankment_select" onchange="OnModalEmbankmentSelectChange();">
                                    <option>必填</option>
                                </select>
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>檢查範圍</label>
                                <input type="text" class="form-control m-input" placeholder="" id="modal_cl_inspection_text">
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>安全等級*</label>
                                <select class="form-control m-input" id="modal_cl_security_select">
                                    <option value="0">必填</option>
                                    <option value="立即改善">立即改善</option>
                                    <option value="注意改善">注意改善</option>
                                    <option value="計畫改善">計畫改善</option>
                                    <option value="正常">正常</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <label>問題描述</label>
                                <textarea class="form-control m-input" rows="3" id="modal_cl_description_textarea"></textarea>
                            </div>
                            <div class="col-lg-4">
                                <label>改善方式</label>
                                <textarea class="form-control m-input" rows="3" id="modal_cl_improve_textarea"></textarea>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 mt-3">
                                <label>座標輸入</label>
                                <select class="form-control m-input" disabled="disabled" id="modal_cl_coordinate_type">
                                    <option>TWD97座標</option>
                                    <!--<option>WGS84座標</option>-->
                                </select>
                                <span class="m-form__help">註:系統資料將自動轉換為97座標</span>
                            </div>
                            <div class="col-lg-2 mt-3">
                                <label>X座標</label>
                                <input type="text" class="form-control m-input" disabled="disabled" placeholder="X" id="modal_cl_embankment_X" >
                            </div>
                            <div class="col-lg-2 mt-3">
                                <label>Y座標</label>
                                <input type="text" class="form-control m-input" disabled="disabled" placeholder="Y" id="modal_cl_embankment_Y">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6" id="em_file_list">
                                <div class="custom-file img_upload">
                                    <input type="file" class="custom-file-input" id="customFile_checklist" accept=".jpeg, .jpg" onchange="AddCheckListFile(this);">
                                    <label class="custom-file-label" for="customFile_checklist" id="custom_file_label">檢查表上傳</label>
                                </div>
                                <div id="checklist_file_list">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="custom-file img_upload">
                                    <input type="file" class="custom-file-input" id="customFile_img" accept=".jpeg, .jpg" onchange="AddCheckListImage(this);" >
                                    <label class="custom-file-label" for="customFile_img" id="custom_image_label">堤防影像上傳</label>
                                </div>
                                <div id="checklist_image_list">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 mt-3">
                                <label>建檔人員</label>
                                <input type="text" class="form-control m-input" value="--" disabled="disabled" id="modal_cl_creater">
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>最後編修人員</label>
                                <input type="text" class="form-control m-input" value="--" disabled="disabled" id="modal_cl_editor">
                            </div>
                            <div class="col-lg-4 mt-3">
                                <label>編修日期</label>
                                <input type="text" class="form-control m-input" value="--" disabled="disabled" id="modal_cl_edit_date">
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
        <div class="modal-footer has_del_btn">
            <div class="modal_footer_del_btn">
                <button type="button" class="btn btn-danger m-btn m-btn--icon" id="modal_cl_delete" onclick="DeleteCheckList();">
                    <span>
                        <i class="la la-warning"></i>
                        <span>刪除</span>
                    </span>
                </button>
            </div>
            <div class="modal_footer_action_btn">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal_cl_cancel">取消</button>
                <button type="button" class="btn btn-primary" id="modal_cl_update" onclick="UpdateCheckList();">
                    <i class="la la-save"></i>
                    <span class="pl-1">儲存</span>
                </button>
            </div>
        </div>
    </div>
</div>