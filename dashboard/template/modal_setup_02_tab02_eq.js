<h6 class="ex_title">若設備數值超出安全範圍，即視為有異常發生，系統將會發送警示。</h6>
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
  <div class="m-portlet__body">

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label>是否進行安全值監測</label>
        <select class="form-control m-input" id="surveillance_ctrl">
          <option value=1>是</option>
          <option value=0>否</option>
        </select>
      </div>
      <div class="col-lg-2 mt-3">
        <label class="">範圍上限</label>
      </div>
      <div class="col-lg-2 mt-3">
        <label class="">範圍下限</label>
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label>X軸加速度</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="x_h_limit">
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="x_l_limit">
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label>Y軸加速度</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="y_h_limit">
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="y_l_limit">
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label>Z軸加速度</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="z_h_limit">
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="z_l_limit">
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label>三軸軸加速度</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="mix_h_limit">
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="mix_l_limit">
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label>震度</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="mmi_h_limit">
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="mmi_l_limit">
      </div>
    </div>

  </div>
</form>