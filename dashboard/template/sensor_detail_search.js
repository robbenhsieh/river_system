<div class="m-form m-form--label-align-right m--margin-top-0 search_content">
	<div class="row align-items-center">
		<div class="col-md-9">
			<div class="form-group m-form__group row align-items-center">
				<div class="col-md-2" id="back_btn" onclick="ShowSensorTable();"><h5 class=""><a href="#" class="m-link"><i class="la la-mail-reply"></i>返回查詢列表</a></h5></div>
				<!-- search items -->
				<div class="search_item date_select d_inline">
					<div class="m-form__group m-form__group--inline">
						<div class="m-form__label inline">
							<label>日期:</label>
						</div>
						<div class="m-form__control">
							<div class="input-group datetime_input" id="">
								<input type="text" class="form-control m-input" readonly="" placeholder="起始日期" id="m_datepicker_1">
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o"></i>
									</span>
								</div>
								<input class="form-control m-input" id="m_timepicker_1" readonly="" placeholder="起始時間" type="text">
							</div>
						</div>
					</div>
				</div>
				<div class="search_item date_select d_inline">
					<div class="m-form__group m-form__group--inline">
						<div class="m-form__label inline">
							<label>至</label>
						</div>
						<div class="m-form__control">
							<div class="input-group datetime_input">
								<input type="text" class="form-control m-input" readonly="" placeholder="結束日期" id="m_datepicker_2">
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o"></i>
									</span>
								</div>
								<input class="form-control m-input" id="m_timepicker_2" readonly="" placeholder="結束時間" type="text">
							</div>
						</div>
					</div>
				</div>
				<span></span>
				<div class="search_item d_inline">
					<div class="m-radio-inline" id="hours_radio_group" >
						<label class="m-radio">
							<input type="radio" name="active" value="3" id="radio_time_3hour" onchange="HistoryDataEasySearch();">3小時
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" name="active" value="4" id="radio_time_6hour" onchange="HistoryDataEasySearch();">6小時
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" name="active" value="5" id="radio_time_12hour" onchange="HistoryDataEasySearch();">12小時
							<span></span>
						</label>
					</div>
				</div>
				<!-- end search items -->
			</div>
		</div>
		<!-- search_action -->
		<div class="col-md-3 m--align-right search_action" id="button_Group">
			<a href="#" class="btn m-btn--pill m-btn m-btn--icon btn-accent" onclick="RefreshHistoryDataList();">
				<span>
					<i class="fa fa-search"></i>
					<span>查詢</span>
				</span>
			</a>
			<a href="#" class="btn m-btn--pill m-btn m-btn--icon  btn-primary" data-toggle="modal" data-target="#m_modal_download">
				<span>
					<i class="fa fa-save"></i>
					<span>歷史數據下載</span>
				</span>
			</a>
		</div>
		<!-- /search_action -->
	</div>
</div><!-- /search_content -->
