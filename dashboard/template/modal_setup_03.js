<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header ">
      <h4 class="modal-title"><span id="modal_sensor_tittle_name"></span>設定</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modal_alert_close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="500">
        <ul class="nav nav-tabs  m-tabs-line m-tabs-line--2x m-tabs-line--success modal_tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="" data-target="#modal_03_tab_setup_01">安全值設定</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="" data-target="#modal_03_tab_setup_02">維護提醒</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane tab_alert_num active" id="modal_03_tab_setup_01" role="tabpanel">
          </div>
          <div class="tab-pane tab_alert_num" id="modal_03_tab_setup_02" role="tabpanel">
            <!--begin::Form-->
            <h6 class="ex_title">以起始日期起算，每半年或一年發送提醒通知乙次。</h6>
              <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="m-portlet__body">
                  <div class="form-group m-form__group row">
                    <div class="col-lg-3 mt-3">
                      <label>是否使用維護提醒功能</label>
                      <select class="form-control m-input" id="maintain_reminder_select">
                        <option value=1>是</option>
                        <option value=0>否</option>
                      </select>
                    </div>
                    <div class="col-lg-3 mt-3">
                      <label class="">起始日期</label>
                      <div class="m-form__control">
                          <div class="input-group datetime_input">
                              <input type="text" class="form-control m-input" placeholder="請選擇日期" id="m_datepicker_warn">
                              <div class="input-group-append">
                                <span class="input-group-text">
                                  <i class="la la-calendar-check-o"></i>
                                </span>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="col-md-4 mt-3">
                    <label>間隔</label>
                    <div class="m-radio-inline">
                      <label class="m-radio">
                        <input type="radio" name="year" id="reminder_interval_1" checked>半年
                        <span></span>
                      </label>
                      <label class="m-radio">
                        <input type="radio" name="year" id="reminder_interval_2">一年
                        <span></span>
                      </label>
                    </div>
                  </div>
                  </div>
                </div>
              </form>
              <!--end::Form-->
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <div class="modal_footer_action_btn">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="modal_sensor_update" onclick="UpdateSensorAlertSetting();">
          <i class="la la-save"></i>
          <span class="pl-1">儲存更新</span>
        </button>
      </div>
    </div>
  </div>
</div>