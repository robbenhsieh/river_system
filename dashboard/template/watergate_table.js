<div class="m-form m-form--label-align-right m--margin-top-0 search_content" id="search_content">
	<div class="row align-items-center">
		<div class="col-md-9">
			<div class="form-group m-form__group row align-items-center">
				<!-- search items -->
				<div class="col-md-3 search_item">
					<div class="m-form__group m-form__group--inline">
						<div class="m-form__label">
							<label>河川別:</label>
						</div>
						<div class="m-form__control">
							<select class="form-control" id="cb_RiverList">
								<option value="">全部</option>
								<option value="1">淡水河</option>
								<option value="2">三峽河</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3 search_item">
					<div class="m-form__group m-form__group--inline">
						<div class="m-form__label">
							<label id="txt_FilterName">名稱:</label>
						</div>
						<div class="m-form__control">
							<select class="form-control" id="cb_FilterList">
								<option value="">全部</option>
								<option value="1">--</option>
								<option value="2">--</option>
							</select>
						</div>
					</div>
				</div>
				<!-- /search items -->
			</div>
		</div>
		<!-- search_action -->
		<div class="col-md-3 m--align-right search_action">
			<!--<a href="#" class="btn m-btn--pill m-btn m-btn--icon btn-accent" id="btn_ExecFilter">
				<span>
					<i class="fa fa-search"></i>
					<span>查詢</span>
				</span>
			</a> -->
			<a href="#" class="btn m-btn--pill m-btn m-btn--icon btn-primary" data-toggle="modal" data-target="#m_modal_download">
				<span>
					<i class="fa fa-save"></i>
					<span>下載</span>
				</span>
			</a>
		</div>
	</div>
</div>
<!--end: Search Form -->
<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" style="white-space: nowrap;" id="m_table_1">
	<thead id=table_list_title>
	</thead>
	<tbody id=table_list>
	</tbody>
</table>