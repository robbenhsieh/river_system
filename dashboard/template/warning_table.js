
<div class="m-form m-form--label-align-right m--margin-top-0 search_content">
	<div class="row align-items-center">
		<div class="col-xl-9 order-2 order-xl-1">
			<div class="form-group m-form__group row align-items-center">
				<!-- search items -->
				<div class="col-md-3 search_item">
				  <div class="m-form__group m-form__group--inline">
					<div class="m-form__label">
					  <label>監測站:</label>
					</div>
					<div class="m-form__control">
					  <select class="form-control" id="warning_gateway_select">
					  </select>
					</div>
				  </div>
				</div>
				<div class="col-md-3 search_item">
				  <div class="m-form__group m-form__group--inline">
					<div class="m-form__label">
					  <label>裝置類型:</label>
					</div>
					<div class="m-form__control">
					  <select class="form-control" id="warning_sensor_type_select">
					  	<option value="0">全部</option>
					  	<option value="26">監測站</option>
						<option value="43">水位計</option>
						<option value="44">加速度計</option>
						<option value="45">傾斜儀</option>
						<option value="46">地震儀</option>
					  </select>
					</div>
				  </div>
				</div>
				<div class="col-md-3 search_item">
					<div class="m-form__group m-form__group--inline">
						<div class="m-form__label">
							<label>警示項目:</label>
						</div>
						<div class="m-form__control">
							<select class="form-control" id="warning_type_select">
								<option value="0">全部</option>
								<option value="disconnect">設備離線</option>
								<!--<option value="data_error">數據錯誤</option> -->
								<option value="data_warning">超出安全值</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3 search_item">
					<div class="m-form__group m-form__group--inline">
						<div class="m-form__label">
							<label>日期:</label>
						</div>
						<div class="m-form__control">
							<div class="input-group datetime_input">
								<input type="text" class="form-control m-input" readonly placeholder="起始日期" id="m_datepicker_1" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o"></i>
									</span>
								</div>
								<input class="form-control m-input" id="m_timepicker_1" readonly placeholder="起始時間" type="text" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 search_item">
					<div class="m-form__group m-form__group--inline">
						<div class="m-form__label">
							<label>至</label>
						</div>
						<div class="m-form__control">
							<div class="input-group datetime_input">
								<input type="text" class="form-control m-input" readonly placeholder="結束日期" id="m_datepicker_2" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o"></i>
									</span>
								</div>
								<input class="form-control m-input" id="m_timepicker_2" readonly placeholder="結束時間" type="text" />
							</div>
						</div>
					</div>
				</div>
				<!-- end search items -->
			</div>
		</div>
		<div class="col-xl-3 order-1 order-xl-2 m--align-right search_action">
			<a href="#" class="btn m-btn--pill m-btn m-btn--icon btn-accent" onclick="RefreshWarningList();">
				<span>
					<i class="fa fa-search"></i>
					<span>查詢</span>
				</span>
			</a>
			<a href="#" class="btn m-btn--pill m-btn m-btn--icon  btn-primary" data-toggle="modal" data-target="#m_modal_download">
				<span>
				  <i class="fa fa-save"></i>
				  <span>下載</span>
				</span>
			</a>
		</div>
	</div>
</div>
<!--Datatable content -->
<div class="data_content">
	<table class="table table-striped- table-bordered table-hover" style="white-space: nowrap;" id="warning_table">
		<thead>
			<tr>
				<th></th>
				<th>設備類別</th>
				<th>設備名稱</th>
				<th>警示項目</th>
				<th>警示說明</th>
				<th>警示時間</th>
			</tr>
		</thead>
		<tbody id="warning_table_list">
		</tbody>
	</table>
</div>


