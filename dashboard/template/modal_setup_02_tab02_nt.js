
<h6 class="ex_title">若設備數值超出安全範圍，即視為有異常發生，系統將會發送警示。</h6>
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
  <div class="m-portlet__body">
    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label>是否進行安全值監測</label>
        <select class="form-control m-input" id="surveillance_ctrl">
          <option value=1>是</option>
          <option value=0>否</option>
        </select>
      </div>
      <div class="col-lg-2 mt-3">
        <label class="">範圍上限</label>
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="h_limit">
      </div>
      <div class="col-lg-2 mt-3">
        <label class="">範圍下限</label>
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="l_limit">
      </div>
    </div>
  </div>
</form>
