<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#" data-target="#tab_1">一般資訊</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#tab_2">安全檢查表</a>
	</li>
</ul>
<!-- tab內容 -->
<div class="tab-content">
	<!-- tab 01 wrap -->
	<div class="tab-pane active" id="tab_1" role="tabpanel">
		<!--Search Form -->
		<div class="m-form m-form--label-align-right m--margin-top-0 search_content">
			<div class="row align-items-center">
				<div class="col-xl-9 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center" id="select_Group">
						<!--select-Group-->
						<div class="col-md-3 search_item">
							<div class="m-form__group m-form__group--inline">
								<div class="m-form__label">
									<label>縣市別:</label>
								</div>
								<div class="m-form__control">
									<select class="form-control" id="cb_RiverList">
										<option value="0">全部</option>
										<option value="1">河川別名稱</option>
										<option value="2">河川別名稱</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 order-1 order-xl-2 m--align-right search_action" id="button_Group">
					<!--<a href="#" class="btn m-btn--pill m-btn m-btn--icon btn-accent" id="btn_ExecFilter">
						<span>
							<i class="fa fa-search"></i>
							<span>查詢</span>
						</span>
					</a>-->
					<a href="#" class="btn m-btn--pill m-btn m-btn--icon btn-primary" data-toggle="modal" data-target="#m_modal_download">
						<span>
						  <i class="fa fa-save"></i>
						  <span>下載</span>
						</span>
					</a>
				</div>
			</div>
		</div>
		<!--Datatable content -->
		<div class="data_content">
			<table class="table table-striped- table-bordered table-hover table-checkable" style="white-space: nowrap;" id="m_table_1">
			<thead id=table_list_title>
			</thead>
			<tbody id=table_list>
			</tbody>
			</table>
		</div>
	</div>
	<!-- tab 02 檢查表上傳 wrap -->
	<div class="tab-pane" id="tab_2" role="tabpanel">
		<!--Search Form -->
		<div class="m-form m-form--label-align-right m--margin-top-0 search_content">
			<div class="row align-items-center">
				<div class="col-xl-9 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<!-- search items -->
						<div class="col-md-3 search_item">
							<div class="m-form__group m-form__group--inline">
								<div class="m-form__label">
									<label>河川別:</label>
								</div>
								<div class="m-form__control">
									<select class="form-control" id="index_emcl_river_select" onchange="OnRiverSelectChange(this);">
										<option value="0">全部</option>
										<option value="1">河川別名稱</option>
										<option value="2">河川別名稱</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3 search_item">
							<div class="m-form__group m-form__group--inline">
							   <div class="m-form__label">
									<label>堤防:</label>
								</div>
								<div class="m-form__control">
									<select class="form-control" id="index_emcl_embankment_select">
										<option value="0">全部</option>
										<option value="堤防類型1">堤防類型1</option>
										<option value="堤防類型2">堤防類型2</option>
										<option value="堤防類型3">堤防類型3</option>
										<option value="堤防類型4">堤防類型4</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3 search_item">
							<div class="m-form__group m-form__group--inline">
								<div class="m-form__label">
									<label>安全等級:</label>
								</div>
								<div class="m-form__control">
									<select class="form-control" id="index_emcl_security_select">
										<option value="0">全部</option>
										<option value="立即改善">立即改善</option>
										<option value="注意改善">注意改善</option>
										<option value="計畫改善">計畫改善</option>
										<option value="正常">正常</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-5 search_item">
							<div class="m-form__group m-form__group--inline">
								<div class="m-form__label">
									<label>檢查日期:</label>
								</div>
								<div class="m-form__control">
									<div class="input-group datetime_input">
										<input type="text" class="form-control m-input" readonly placeholder="起始日期" id="m_datepicker_1" />
										<div class="input-group-append">
											<span class="input-group-text">
												至
											</span>
										</div>
										<input type="text" class="form-control m-input" readonly placeholder="結束日期" id="m_datepicker_2" />
									</div>
								</div>
							</div>
						</div>
						<!-- end search items -->
					</div>
				</div>
				<div class="col-xl-3 order-1 order-xl-2 m--align-right search_action" id="button_Group">
					<a href="#" class="btn m-btn--pill m-btn m-btn--icon btn-accent" onclick="RefreshCheckList();">
						<span>
							<i class="fa fa-search"></i>
							<span>查詢</span>
						</span>
					</a>
				</div>
			</div>
		</div>
		<!--Datatable content -->
		<div class="data_content">
			<table class="table table-striped- table-bordered table-hover table-checkable" style="white-space: nowrap;" id="index_emcl_table">
				<thead>
					<tr>
						<th>檢查日期</th>
						<th>堤防名稱</th>
						<th>堤防型式</th>
						<th>堤防樁位</th>
						<th>縣市別</th>
						<th>安全等級</th>
						<th>問題描述</th>
						<th>詳細資料</th>
					</tr>
				</thead>
				<tbody id="index_emcl_table_list">
				</tbody>
			</table>
		</div>
	</div>
</div>