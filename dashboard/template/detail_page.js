
  <div class="info_item flex_left item_basic" >
    <span class="item_title" id="detail_info_name">這是一個名稱很長的地震儀</span>
    <div class="item_content">
      <div class="item_basic_desc">
        <span class="desc_title" id="detail_info_">所屬區域</span>
        <span class="desc_text" id="detail_info_zone">XX區</span>
      </div>
      <div class="item_basic_desc">
        <span class="desc_title" id="">管理機關</span>
        <span class="desc_text" id="detail_info_admin">機關名稱名稱名稱這是一個機關名稱很長的機關</span>
      </div>
      <div class="item_basic_desc">
        <span class="desc_title" id="">岸別</span>
        <span class="desc_text"  id="detail_info_side">左岸</span>
      </div>
      <div class="item_basic_desc">
        <span class="desc_title" id="">河川別</span>
        <span class="desc_text"  id="detail_info_river">OO河</span>
      </div>
      <div class="item_basic_desc">
        <span class="desc_title" id="">負責人</span>
        <span class="desc_text" id="detail_info_person">欄位內容</span>
      </div>
      <div class="item_basic_desc">
        <span class="desc_title" id="">97座標X</span>
        <span class="desc_text" id="detail_info_X_97">欄位內容</span>
      </div>
      <div class="item_basic_desc">
        <span class="desc_title" id="">97座標Y</span>
        <span class="desc_text" id="detail_info_Y_97">欄位內容</span>
      </div>

    </div>
  </div>
  <div class="info_item item_table">
    <span class="item_title"id="detail_table_title">歷史數據</span>
    <div class="item_content" id="detail_page_table">
    </div>
  </div>
  <div class="info_item item_chart">
    <span class="item_title">基本資訊</span>
    <div class="item_content">
      <span class="time_text">更新時間:2019/01/22 13:25:15</span>
      <div>
        圖表放在這裡
      </div>
    </div>
  </div>
