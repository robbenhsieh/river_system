<h6 class="ex_title">若設備數值超出安全範圍，即視為有異常發生，系統將會發送警示。</h6>
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
  <div class="m-portlet__body">

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label>是否進行安全值監測</label>
        <select class="form-control m-input" id="surveillance_ctrl">
          <option value=1>是</option>
          <option value=0>否</option>
        </select>
      </div>
      <div class="col-lg-2 mt-3">
        <label class="">範圍下限</label>
      </div>
      <div class="col-lg-2 mt-3">
        <label class="">範圍上限</label>
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label id="axis_x_lab">X軸加速度 (gal)</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="x_l_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="x_l_limit_fb">
          <i class="la la-warning"></i>
          <span id="x_l_limit_fb_msg"></span>
        </div>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="x_h_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="x_h_limit_fb">
          <i class="la la-warning"></i>
          <span id="x_h_limit_fb_msg"></span>
        </div>
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label id="axis_y_lab">Y軸加速度 (gal)</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="y_l_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="y_l_limit_fb">
          <i class="la la-warning"></i>
          <span id="y_l_limit_fb_msg"></span>
        </div>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="y_h_limit">
          <div class="form-control-feedback m--font-danger danger_feedback" id="y_h_limit_fb">
          <i class="la la-warning"></i>
          <span id="y_h_limit_fb_msg"></span>
        </div>
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label id="axis_z_lab">Z軸加速度 (gal)</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="z_l_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="z_l_limit_fb">
          <i class="la la-warning"></i>
          <span id="z_l_limit_fb_msg"></span>
        </div>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="z_h_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="z_h_limit_fb">
          <i class="la la-warning"></i>
          <span id="z_h_limit_fb_msg"></span>
        </div>
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label id="axis_c_lab">三軸加速度 (gal)</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="mix_l_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="mix_l_limit_fb">
          <i class="la la-warning"></i>
          <span id="mix_l_limit_fb_msg"></span>
        </div>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="mix_h_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="mix_h_limit_fb">
          <i class="la la-warning"></i>
          <span id="mix_h_limit_fb_msg"></span>
        </div>
      </div>
    </div>

    <div class="form-group m-form__group row">
      <div class="col-lg-3 mt-3">
        <label id="axis_m_lab">震度 (級)</label>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入下限值" id="mmi_l_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="mmi_l_limit_fb">
          <i class="la la-warning"></i>
          <span id="mmi_l_limit_fb_msg"></span>
        </div>
      </div>
      <div class="col-lg-2 mt-3">
        <input type="text" class="form-control m-input" placeholder="請輸入上限值" id="mmi_h_limit">
        <div class="form-control-feedback m--font-danger danger_feedback" id="mmi_h_limit_fb">
          <i class="la la-warning"></i>
          <span id="mmi_h_limit_fb_msg"></span>
        </div>
      </div>
    </div>

  </div>
</form>