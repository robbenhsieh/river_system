<div class="m-form m-form--label-align-right m--margin-top-0 search_content" id="search_content">
  <div class="row align-items-center">
    <div class="col-xl-10 order-2 order-xl-1">
      <div class="form-group m-form__group row align-items-center">
        <!-- search items -->
        <div class="col-md-2 search_item">
          <div class="m-form__group m-form__group--inline">
            <div class="m-form__label">
              <label>河川:</label>
            </div>
            <div class="m-form__control">
              <select class="form-control" id="sensor_river_select">
                <option value="0">全部</option>
                <option value="1">--</option>
                <option value="2">--</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-2 search_item">
          <div class="m-form__group m-form__group--inline">
            <div class="m-form__label">
              <label>區域:</label>
            </div>
            <div class="m-form__control">
              <select class="form-control" id="sensor_area_select">
                <option value="0">全部</option>
                <option value="1">--</option>
                <option value="2">--</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-2 search_item">
          <div class="m-form__group m-form__group--inline">
            <div class="m-form__label">
              <label>監測站:</label>
            </div>
            <div class="m-form__control">
              <select class="form-control" id="sensor_gw_select">
                <option value="0">全部</option>
                <option value="1">--</option>
                <option value="2">--</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-2 search_item">
          <div class="m-form__group m-form__group--inline">
            <div class="m-form__label">
              <label>裝置類型:</label>
            </div>
            <div class="m-form__control">
              <select class="form-control" id="sensor_type_select">
                <option value="46">地震儀</option>
                <option value="43">水位計</option>
                <option value="44">加速度計</option>
                <option value="45">傾斜儀</option>
              </select>
            </div>
          </div>
        </div>
        <!-- /search items -->
      </div>
    </div>
    <!-- search_action -->
    <div class="col-xl-2 order-1 order-xl-2 m--align-right search_action">
      <!--<a href="#" class="btn m-btn--pill m-btn m-btn--icon btn-accent" onclick="RefreshSensorList();">
        <span>
          <i class="fa fa-search"></i>
          <span>查詢</span>
        </span>
      </a> -->
      <a href="#" class="btn m-btn--pill m-btn m-btn--icon  btn-primary" data-toggle="modal" data-target="#m_modal_download">
        <span>
          <i class="fa fa-save"></i>
          <span>下載</span>
        </span>
      </a>
    </div>
  </div>
</div>
