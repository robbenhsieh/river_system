<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header ">
      <h4 class="modal-title"><span id="modal_station_tittle_name"></span>設定</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modal_station_close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="500">
        <ul class="nav nav-tabs  m-tabs-line m-tabs-line--2x m-tabs-line--success modal_tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="" data-target="#tab_setup_1">基本設定</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_setup_1" role="tabpanel">
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
              <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                  <div class="col-lg-4 mt-3">
                    <label>名稱</label>
                    <input type="text" class="form-control m-input" id="modal_station_name" value="">
                  </div>
                  <div class="col-lg-4 mt-3">
                    <label class="">所屬區域</label>
                    <select class="form-control m-input" id="modal_station_area_select">
                        <option value="區域">區域</option>
                    </select>
                  </div>
                  <div class="col-lg-4 mt-3">
                    <label class="">管理機關</label>
                    <select class="form-control m-input" id="modal_station_admin_select">
                        <option value="十河局">十河局</option>
                    </select>
                  </div>
                  <div class="col-lg-4 mt-3">
                    <label class="">河川別</label>
                    <select class="form-control m-input" id="modal_station_river_select">
                        <option value="河川別名稱">河川別名稱</option>
                    </select>
                  </div>
                  <div class="col-lg-4 mt-3">
                    <label class="">岸別</label>
                    <select class="form-control m-input" id="modal_station_bank_select">
                      <option value="左岸">左岸</option>
                      <option value="右岸">右岸</option>
                    </select>
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <div class="col-lg-4 mt-3">
                    <label>座標輸入規格</label>
                    <select class="form-control m-input" id="modal_station_coordinate_type_select">
                      <option value="97">TWD97座標</option>
                      <option value="84">WGS84座標</option>
                    </select>
                    <span class="m-form__help">註:系統資料將自動轉換為97座標</span>
                  </div>
                  <div class="col-lg-2 mt-3">
                    <label class="">X座標</label>
                    <input type="text" class="form-control m-input" placeholder="X" id="modal_station_x">
                  </div>
                  <div class="col-lg-2 mt-3">
                    <label class="">Y座標</label>
                    <input type="text" class="form-control m-input" placeholder="Y" id="modal_station_y">
                  </div>
                </div>
              </div>
            </form>
            <!--end::Form-->
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <div class="modal_footer_action_btn">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal_station_cancel">取消</button>
        <button type="button" class="btn btn-primary" id="modal_station_update" onclick="UpdateGatewayInfo();">
          <i class="la la-save"></i>
          <span class="pl-1">儲存更新</span>
        </button>
      </div>
    </div>
  </div>
</div>