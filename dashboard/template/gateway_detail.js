<div class="info_content">
  <div class="info_item flex_left item_basic" >
    <span class="item_title" id="detail_info_name"></span>

    <div class="info_table_list">
      <table class="table table-striped table-bordered">
        <thead>
        </thead> 
        <tbody>
          <tr>
            <th style="background-color: transparent" id="detail_info_">區域</th> 
            <td id="detail_info_area"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="">管理機關</th> 
            <td id="detail_info_admin"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="">河川別</th>
            <td id="detail_info_river"></td>
          </tr>
          <tr>
            <th style="background-color: transparent" id="">岸別</th> 
            <td id="detail_info_bank"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="">TWD97座標X</th> 
            <td id="detail_info_X_97"></td>
          </tr> 
          <tr>
            <th style="background-color: transparent" id="">TWD97座標Y</th> 
            <td id="detail_info_Y_97"></td>
          </tr> 
        </tbody>
      </table>
    </div>

  </div>
  <div class="info_item item_table">
    <span class="item_title" id="detail_table_title">監測站裝置列表</span>
    <div class="item_content" id="detail_page_table">
      <table class="table table-striped- table-bordered table-hover table-checkable" style="white-space: nowrap;" id="sensor_table">
        <thead>
          <tr id="sensor_table_list_title">
              <th>名稱</th>
              <th>裝置類型</th>
              <th>TWD97 座標X</th>
              <th>TWD97 座標Y</th>
          </tr>
        </thead>
        <tbody id="sensor_table_list">
        </tbody>
      </table>
    </div>
  </div>
</div>