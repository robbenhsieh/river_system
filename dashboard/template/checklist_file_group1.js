<div class="file_group">
	<div class="outline">
	    <div><span>{name}</span></div>
	    <div>
	        <a href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only action_btn" onclick="DeleteCheckListFile({index})">
	            <i class="la la-close"></i>
	        </a>
	    </div>
	</div>
	<a href="#" class="show_img" data-toggle="modal" data-target="#m_modal_img" onclick="ShowModalCheckListFile({index});" id="file_0">
	    <span>
	        <i class="fa fa-eye"></i>
	        <span>圖檔</span>
	    </span>
	</a>
</div>
