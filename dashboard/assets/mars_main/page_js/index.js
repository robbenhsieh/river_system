
//-----資料查詢彈出視窗
var PortletTools = function () {
    //== Toastr
    var initToastr = function() {
        toastr.options.showDuration = 1000;
    }

    //== Demo 1
    var demo1 = function()
     {
        // This portlet is lazy initialized using data-portlet="true" attribute. You can access to the portlet object as shown below and override its behavior
        var portlet = new mPortlet('m_portlet_index');

        // //== Toggle event handlers
        // portlet.on('beforeCollapse', function(portlet) {
        //     setTimeout(function() {
        //         toastr.info('Before collapse event fired!');
        //     }, 100);
        // });

        // portlet.on('afterCollapse', function(portlet) {
        //     setTimeout(function() {
        //         toastr.warning('Before collapse event fired!');
        //     }, 2000);            
        // });

        // portlet.on('beforeExpand', function(portlet) {
        //     setTimeout(function() {
        //         toastr.info('Before expand event fired!');
        //     }, 100);  
        // });

        // portlet.on('afterExpand', function(portlet) {
        //     setTimeout(function() {
        //         toastr.warning('After expand event fired!');
        //     }, 2000);
        // });

        // //== Remove event handlers
        // portlet.on('beforeRemove', function(portlet) {
        //     toastr.info('Before remove event fired!');

        //     return confirm('Are you sure to remove this portlet ?');  // remove portlet after user confirmation
        // });

        // portlet.on('afterRemove', function(portlet) {
        //     setTimeout(function() {
        //         toastr.warning('After remove event fired!');
        //     }, 2000);            
        // });

        //== Reload event handlers
        portlet.on('reload', function(portlet) {
            // toastr.info('Leload event fired!');

            mApp.block(portlet.getSelf(), {
                overlayColor: '#ffffff',
                type: 'loader',
                state: 'accent',
                opacity: 0.3,
                size: 'lg'
            });

            // update the content here

            setTimeout(function() {
                mApp.unblock(portlet.getSelf());
            }, 2000);
        });

        //== Reload event handlers
        portlet.on('afterFullscreenOn', function(portlet) {
            // toastr.warning('After fullscreen on event fired!');    
            var scrollable = $(portlet.getBody()).find('> .m-scrollable');

            if (scrollable) {
                var setInfoContent = $(portlet.getBody()).find('.info_content');
                scrollable.data('original-height', scrollable.css('height'));
                scrollable.css('height', '100%');

                setInfoContent.css('height', '100%');
                
                mUtil.scrollerUpdate(scrollable[0]);
            }
        });

        portlet.on('afterFullscreenOff', function(portlet) {
            // toastr.warning('After fullscreen off event fired!');    
            var scrollable = $(portlet.getBody()).find('> .m-scrollable');

            if (scrollable) {
                var scrollable = $(portlet.getBody()).find('> .m-scrollable');
                var setInfoContent = $(portlet.getBody()).find('.info_content');
                scrollable.css('height', scrollable.data('original-height'));
                setInfoContent.css('height','301px');

                mUtil.scrollerUpdate(scrollable[0]);
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            initToastr();

            // init demos
            demo1();
        }
    };
}();

jQuery(document).ready(function() {
    PortletTools.init();
});

//-----資料查詢彈出視窗縮放
 function openPortlet(){
    document.getElementById("m_portlet_index").style.display = "block";
}

function closePortlet(){
    var element = document.getElementById("m_portlet_index");

    if ($(element).hasClass('m-portlet--fullscreen')) {
        document.getElementById("fullscreen_ctl").click();
    } 
    element.style.display = "none";
}

function RefreshCheckBox()
{
    //圖層選單
    $(document).ready(function()
    {
      //全選、全不選
      $(".chechbox_wrap .all_input>input").click(function()
      {
        var all_id = $(this).parent().attr("id"); 
        var banks = $(this).parent().siblings().children();
        var flag = $(this).prop('checked');
        banks.prop('checked', flag);
      });

      //如果有一個沒選中，不勾選全部
      $(".all_input").siblings().children("input").click(function() 
      {
        var all = $(this).parents().find(".all_input").children("input");
        var num = 0;
        $(this).each(function()
         {
          if ($(this).prop("checked")) {
            num++;
          }
        });
        if (num == $(this).length) {
          $(all).prop('checked', true);
        } else {
          $(all).prop('checked', false);
        }
      });
    });
}
RefreshCheckBox()
