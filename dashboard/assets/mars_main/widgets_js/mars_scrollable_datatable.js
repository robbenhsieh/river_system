var DatatablesBasicScrollable = function() {

	var initTable1 = function() {
		var table = $('#adm_station_table');

		// begin  table
		table.DataTable({
        	scrollY: '40vh',
        	scrollCollapse: true,
        	scrollX: true,

        	//== DOM Layout settings-筆數
			dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

        	lengthMenu: [5, 10, 20, 30, 50],
			pageLength: 20,
			language: {
				'lengthMenu': '資料筆數 _MENU_',
			},

			columnDefs: [
				{
					targets: -1,
					title: '編輯',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <a href="#" class="btn btn-outline-accent m-btn m-btn--icon m-btn--pill action_btn"  data-toggle="modal" data-target="#modal_setup_01">
							<span>
								<i class="fa fa-pencil-alt"></i>
								<span>設定</span>
							</span>
						</a>`;
					},
				},
				// {
				// 	targets: 8,
				// 	render: function(data, type, full, meta) {
				// 		var status = {
				// 			1: {'title': 'Pending', 'class': 'm-badge--brand'},
				// 			2: {'title': 'Delivered', 'class': ' m-badge--metal'},
				// 			3: {'title': 'Canceled', 'class': ' m-badge--primary'},
				// 			4: {'title': 'Success', 'class': ' m-badge--success'},
				// 			5: {'title': 'Info', 'class': ' m-badge--info'},
				// 			6: {'title': 'Danger', 'class': ' m-badge--danger'},
				// 			7: {'title': 'Warning', 'class': ' m-badge--warning'},
				// 		};
				// 		if (typeof status[data] === 'undefined') {
				// 			return data;
				// 		}
				// 		return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
				// 	},
				// },
				// {
				// 	targets: 9,
				// 	render: function(data, type, full, meta) {
				// 		var status = {
				// 			1: {'title': 'Online', 'state': 'danger'},
				// 			2: {'title': 'Retail', 'state': 'primary'},
				// 			3: {'title': 'Direct', 'state': 'accent'},
				// 		};
				// 		if (typeof status[data] === 'undefined') {
				// 			return data;
				// 		}
				// 		return '<span class="m-badge m-badge--' + status[data].state + ' m-badge--dot"></span>&nbsp;' +
				// 			'<span class="m--font-bold m--font-' + status[data].state + '">' + status[data].title + '</span>';
				// 	},
				// },
			],
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	DatatablesBasicScrollable.init();
});