//== Class definition

var BootstrapDatepicker = function () {

    var arrows;
    if (mUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    //== Private functions
    var demos = function () {
        // minimum setup
        $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
            rtl: mUtil.isRTL(),
            language: 'zh-TW',
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        // minimum setup for modal demo
        $('#m_datepicker_1_modal').datepicker({
            rtl: mUtil.isRTL(),
            language: 'zh-TW',
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        // input group layout 
        $('#m_datepicker_2, #m_datepicker_2_validate').datepicker({
            rtl: mUtil.isRTL(),
            language: 'zh-TW',
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        // input group layout for modal demo
        $('#m_datepicker_2_modal').datepicker({
            rtl: mUtil.isRTL(),
            language: 'zh-TW',
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

jQuery(document).ready(function() {    
    BootstrapDatepicker.init();
});