/* module.js
export function hello() {
  return "Hello";
}

// main.js
import {hello} from 'module'; // or './module'
let val = hello(); // val is "Hello";*/
/* import * as ClientModule from "/scripts/libs/mars-client.js";
var _script = document.createElement('script');
_script.src = '/scripts/libs/mars-client.js';
document.head.appendChild(_script);*/

var cookie_name = "mars_reiver_system_account";

//document.getElementById("login_Form").action = "";
//document.getElementById("login_Form").method = "post";
//document.getElementById("_pwd").onkeyup = function() {OnKeyup(event) };

document.getElementById("m_login_signin_submit").onclick =  function() { OnLoginClick() };
document.getElementById("btn_UpdateUserInfo").onclick =  function() { OnRegEmailClick() };

//----
function GotoLoginPage()
{
	if(window.location.pathname == '/scripts/river_system/dashboard/login.html'){
		return;
	}
	else{
		window.location='/scripts/river_system/dashboard/login.html';
	}
}
//----
function GotoMainPage()
{
	if(window.location.pathname == '/scripts/river_system/dashboard/index.html'){
		return;
	}
	else{
		window.location='/scripts/river_system/dashboard/index.html';
	}
}
//----
function OnKeyup(_e)
{
	if(_e == null || _e.which != 13) return;
	OnLoginClick();
}
//----
function OnLoginClick()
{
	/*if(_User.WasLogin())
	{
		//alert('Login already, please logout first!');
		GotoMainPage();
		return;
	}*/

	var _usr = document.getElementById("_usr").value;
	var _pwd = document.getElementById("_pwd").value;

	if(_usr.length > 0 && _pwd.length > 0)
	{
		if(_User.DoLoginAdv(_usr, _pwd)&& _User.WasLogin() !== null && _User.WasLogin().length > 0)
		{
			var remember = document.getElementById("remember_user").checked;

			if(remember == true)  { SetCookie(cookie_name, _usr, 7*24); }
			if(remember == false) { SetCookie(cookie_name, "", -1);   }

			GotoMainPage();
			return;
		}
		else 
		{
			//alert('The account or password is incorrect-1');
			return;
		}
	}
	else
	{
		//alert('The account or password is incorrect-2');
	}
	//AddAlert("danger", "Login", "The account or password is incorrect");
}
//----
function CheckIDForMain()
{

	var user_name = GetCookie(cookie_name);

    if(user_name == "")
    {
        document.getElementById("_usr").value = "";
        document.getElementById("_pwd").value = "";
        document.getElementById("remember_user").checked = false;
    }
    else
    {
        document.getElementById("_usr").value = user_name;
        document.getElementById("_pwd").value = "";
        document.getElementById("remember_user").checked = true;
    }

	//----------------------------------

	if(_User.WasLogin()!==null && _User.WasLogin().length > 0)
	{
		GotoMainPage();
		return;
	}
	else
	{
		GotoLoginPage();
		return;
	}
}
//----
function CheckID()
{
	if(_User.WasLogin()==null)
	{
		GotoLoginPage();
		return;
	}
}
//----
function LogOut()
{
	_User.DoLogout();
	GotoLoginPage();
}
//----
function OnLoginByGoogleClick()
{
	_User.DoRegistryByGoogle('https://www.mars-cloud.com.tw/scripts/auth/google_oauth_registry.js');
}
//----
function OnLoginByFBClick()
{
	_User.DoRegistryByFB('https://www.mars-cloud.com.tw/scripts/auth/fb_oauth_registry.js');
}
//----
function OnLoginByIGClick()
{
	_User.DoRegistryByIG('https://www.mars-cloud.com.tw/scripts/auth/ig_oauth_registry.js');
}
//----
function OnRegEmail()
{
	try
	{
		document.getElementById("account").value = "";
	}
	catch(e){console.log(e);}
}
//----
function OnRegEmailClick()
{
	try
	{
		var account = document.getElementById("account").value;

		var resp = _User.RequestNewPassword(account);

		document.getElementById("account").value = "";

		alert("已傳送新密碼至您的信箱");

		document.getElementById("m_modal_reg_close").click();

		/*if(Api_CheckUser(account) == true)
		{
			var resp = _User.RequestNewPassword(account);

			document.getElementById("m_modal_reg_close").click();

			//alert("已傳送新密碼至您的信箱");
		}
		else
		{
			//alert("此帳號不存在，請再重新輸入");
		}*/
	}
	catch(e){console.log(e);}
}
//----
function CheckAccount()
{
	try
	{
		var account = document.getElementById("account").value;

		if(Api_CheckUser(account) == true)
		{
			document.getElementById("input_feedback").style.display = "none";
		}
		else
		{
			document.getElementById("input_feedback").style.display = "";
			document.getElementById("feedback_text").innerHTML = "此帳號不存在，請再重新輸入";
		}
	}
	catch(e){console.log(e);}
}
//----
function SetCookie(name, value, expirehours)
{
	var cookieString = name + "=" + escape(value);

	if(expirehours > 0)
	{
		var date = new Date();
		date.setTime(date.getTime() + expirehours*3600*1000);
		cookieString = cookieString + ";expires=" + date.toGMTString();
	}

	document.cookie = cookieString;
}
//----
function GetCookie(name)
{
	var str_cookie = document.cookie;
	var start = str_cookie.indexOf(name + "=") ;

    if(start != -1 )
    {
        start = start + name.length + 1 ;
        var end = document.cookie.indexOf(";", start);

        if(end == -1){ end = document.cookie.length; }

        return unescape(document.cookie.substring(start, end));
    }
    return "" ;
}
//----
function DelCookie(name)
{
	SetCookie(name, null, -1);
}
//----
//export {CheckID, LogOUt};
