
var current_gateway = {};
var current_sensor = {};
var current_checklist = null;

var em_check_list = [];

var cl_file_array = [];
var cl_image_array = [];

//----- 
function Adm_Init()
{
  try
  {
    if(_User.GetCurrentUserGroup() != "administrator"){
      window.location='/scripts/river_system/dashboard/index.html';
    }

    document.getElementById("info_CurUser").innerHTML = _User.GetCurrentUserName();

    if(WasLogin() == false) return;
    document.getElementById("LogOutButton").onclick = function(){ OnLogOutClick(); };
    setInterval(WasLogin, 60000);
    
    API_Init();

    InitSystemLogTable();
    RefreshSystemLogList();

  }
  catch(e){console.log(e);}
  return null;
}

//----- 
function WasLogin()
{
	try
	{
		if(_User.WasLogin() == null)
		{
			DoLogOut();
			return false;
		}
			
		return true;
	}
	catch(e){console.log(e);}
	return false;
}
//-----
function OnLogOutClick()
{
	try
	{
		if(confirm('是否要登出本系統?'))
			DoLogOut();
	}
	catch(e){console.log(e);}
}
//-----
function DoLogOut()
{
	try
	{
		_User.DoLogout();
		
		window.location = '/scripts/river_system/dashboard/login.html';
	}
	catch(e){console.log(e);}
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------
function InitSystemLogTable()
{
  try
  {
    var arrows = {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>'
    }

    $('#m_datepicker_1, #m_datepicker_2').datepicker({
        language: 'zh-TW',
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows,
        format: 'yyyy/mm/dd'
    });

    $('#m_datepicker_1').datepicker('update', new Date());
    $('#m_datepicker_2').datepicker('update', new Date());

    var m_datepicker_1 = document.getElementById("m_datepicker_1");
	var m_datepicker_2 = document.getElementById("m_datepicker_2");

	document.getElementById("m_datepicker_1").onchange = function(){ CheckDateTimeRange1(); };
	document.getElementById("m_datepicker_2").onchange = function(){ CheckDateTimeRange2(); };

	m_datepicker_1.time_range_flag = m_datepicker_2.time_range_flag = false;

  }
  catch(e){console.log(e);}
}
//-----
function RefreshSystemLogList()
{
  try
  {

    DestroyTable("system_log_table");

    var table_list = document.getElementById("system_log_table_list");
    table_list.innerHTML = "";

    var s_date = $("#m_datepicker_1").val();
    var e_date = $("#m_datepicker_2").val();
    var s_time = "00:00:00.000";
    var e_time = "23:59:59.999";
    var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
    var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

    var day_count = Math.floor((end - start) / (24 * 60 * 60 * 1000)) + 1;

    var data_list = [];

    for(var n = 0; n < day_count; n++)
    {
      var date = new Date(s_date);
      var utc = date.setDate(date.getDate() + n);

      var resp = _User.getOneDaySystemLog(utc, null);

      if(resp != null && resp.length > 0)
      {
        var resp = JSON.parse(resp);
        var results = resp.results;

        for(var i = 0; i < results.length; i++)
        {
          var item = results[i];

          data_list.push(item);
        }
      }
    }

    data_list = data_list.sort(function(a, b) {
      return (parseInt(a.system_time) < parseInt(b.system_time)) ? 1 : -1;
    });

    for(var i = 0; i <　data_list.length; i++)
    {
      var item =　data_list[i];
      var data = item.data
      var time = parseInt(item.system_time);
      time = TimestampToDate(time, true);

      var data_obj = JSON.parse(data);
      var name = data_obj.user;
      var msg = data_obj.msg;

      var str = "";
      str += "<tr>";
      str += "<td>" + name +"</td>";
      str += "<td>" + (time == null ? "--" : time)+"</td>";
      str += "<td>" + (msg == null ? "--" : msg)+"</td>";
      str += "</tr>";

      table_list.innerHTML += str;
    }

    InitTable("system_log_table");
  }
  catch(e){console.log(e);}
}
//-----
function TimestampToDate(timestamp, flag)
{
  try
  {
    var _datetime = new Date(timestamp);

    var _year = _datetime.getFullYear();
    var _month = _datetime.getMonth() + 1;
    var _date = _datetime.getDate();
    var _hour = _datetime.getHours();
    var _min  = _datetime.getMinutes();
    var _sec  = _datetime.getSeconds();

    var _yearStr  = _datetime.getFullYear();
    var _monthStr = ( _month < 10) ? ('0'+ _month) : ('' + _month);
    var _dateStr  = ( _date < 10)  ? ('0'+ _date)  : ('' + _date);
    var _hourStr  = ( _hour < 10)  ? ('0'+ _hour)  : ('' + _hour);
    var _minStr   = ( _min < 10)   ? ('0'+ _min)   : ('' + _min);
    var _secStr   = ( _sec < 10)   ? ('0'+ _sec)   : ('' + _sec);

    var _datetimeStr = "";

    if(flag == false)
      _datetimeStr = _yearStr + '/' + _monthStr +  '/' + _dateStr;
    else if(flag == true)
      _datetimeStr = _yearStr + '/' + _monthStr +  '/' + _dateStr  + ' ' + _hourStr + ':' + _minStr + ':' + _secStr;

    return _datetimeStr;
  }
  catch(e){console.log(e);}

  return null;
}
//-----
function InitTable(table_id) 
{
  try
  {
    var table = "#" + table_id;

    $(table).DataTable({
      scrollCollapse: true,
      scrollX: true,
      retrieve: true,
      destroy: true,
      order: [[ 1, 'desc' ]],
      //== DOM Layout settings-筆數
      dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",

      lengthMenu: [5, 10, 20, 30, 50],
      pageLength: 20,
      language: {
		"loadingRecords": "載入中...",
	    'lengthMenu': '資料筆數 _MENU_',
	    "zeroRecords":  "沒有符合的結果",
	    "info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
	    "infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
	    "infoFiltered": "(從 _MAX_ 項結果中過濾)",
	    "infoPostFix":  "",
	    "search":       "搜尋:",
	    /*"paginate": {
	        "first":    "第一頁",
	        "previous": "上一頁",
	        "next":     "下一頁",
	        "last":     "最後一頁"
	    },*/
	    "aria": {
	        "sortAscending":  ": 升冪排列",
	        "sortDescending": ": 降冪排列"
	    }
      }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
       $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
  }
  catch(e){console.log(e);}
}
//-----
function DestroyTable(table_id) 
{
  try
  {
    var table = "#" + table_id;
    if($.fn.DataTable.isDataTable(table)) 
    {
      $(table).DataTable().destroy();

      $(table_id + ' tbody').empty();
    }
  }
  catch(e){console.log(e);}
}
//-----
function CheckDateTimeRange1()
{
	try
	{
		var m_datepicker_1 = document.getElementById("m_datepicker_1");
		var m_datepicker_2 = document.getElementById("m_datepicker_2");
		var m_timepicker_1 = document.getElementById("m_timepicker_1");
		var m_timepicker_2 = document.getElementById("m_timepicker_2");

		if(m_datepicker_1.time_range_flag == true) { return; }

		m_datepicker_1time_range_flag = true;

		var s_date = (m_datepicker_1 != null) ? $("#m_datepicker_1").val() : "";
		var e_date = (m_datepicker_2 != null) ? $("#m_datepicker_2").val() : "";
		var s_time = (m_timepicker_1 != null) ? $('#m_timepicker_1').val() + ":00.000" : "";
		var e_time = (m_timepicker_2 != null) ? $('#m_timepicker_2').val() + ":59.999" : "";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

		if(start > end)
		{
			if(m_datepicker_2 != null) { $('#m_datepicker_2').datepicker('update', s_date); }
			if(m_timepicker_2 != null) { $('#m_timepicker_2').timepicker('setTime', s_time); }
		}

		m_datepicker_1.time_range_flag = false;

	}
	catch(e){console.log(e);}
}
//-----
function CheckDateTimeRange2()
{
	try
	{
		var m_datepicker_1 = document.getElementById("m_datepicker_1");
		var m_datepicker_2 = document.getElementById("m_datepicker_2");
		var m_timepicker_1 = document.getElementById("m_timepicker_1");
		var m_timepicker_2 = document.getElementById("m_timepicker_2");

		if(m_datepicker_1.time_range_flag == true) { return; }

		document.getElementById("m_datepicker_1").time_range_flag = true;

		var s_date = (m_datepicker_1 != null) ? $("#m_datepicker_1").val() : "";
		var e_date = (m_datepicker_2 != null) ? $("#m_datepicker_2").val() : "";
		var s_time = (m_timepicker_1 != null) ? $('#m_timepicker_1').val() + ":00.000" : "";
		var e_time = (m_timepicker_2 != null) ? $('#m_timepicker_2').val() + ":59.999" : "";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

		if(start > end)
		{
			if(m_datepicker_1 != null) { $('#m_datepicker_1').datepicker('update', e_date); }
			if(m_timepicker_1 != null) { $('#m_timepicker_1').timepicker('setTime', e_time); }
		}

		document.getElementById("m_datepicker_1").time_range_flag = false;

	}
	catch(e){console.log(e);}
}
//-----