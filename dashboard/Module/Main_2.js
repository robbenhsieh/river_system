

//var stationId = ["id_", "name_", "zone_", "admin_","river_","side_","person_","X_","Y_","address_","Ip_"];
//var deviceId = ["id", "name", "zone", "admin","river","side","person","X","Y","address","Ip"];
var Station_info =null;
function GetAllGateway()
{
	var _data = _User.GetUserDataSrcList();
	var _plantGateway = [];
	if(_data != null)
	{
	var _root = JSON.parse(_data);
	var _devices = _root.results;

	for(var i=0;i<_devices.length;i++)
		if(_devices[i].data_profile.indexOf('Gateway') >= 0 && _devices[i].suid.length <= 0)
		{
			try
			{
				var _payload = _User.GetUserDataSrcInfo(_devices[i].uuid, 0, null);
				if(_payload != null)
				{
					_payload = JSON.parse(_payload);
					_payload._ph = [];
					_payload._lux = [];
					_payload._temp3 = [];
					_payload._env = [];
					_payload._anemo = [];
					_payload._ec = [];
					_payload._do = [];
					_payload._supple = [];
					_payload._led = [];
					_payload._relay = [];
					_payload._air = [];
					_payload._light = [];
					_payload._info = [];

					//_plantGateway[_payload.uuid] = _payload;
				}
			}
			catch(_e){}
		}
  }
	return _payload;
}
//--------------------------------------------------------------------------------------------------
function updateBykey(source,key,data){
	var status = _User.updateDataByKey(source,key,data);
	return status;
}
//--------------------------------------------------------------------------------------------------
function queryByRows(device,rows)
{
  var _payload = _User.GetLastData(device,rows);
  if(_payload != null && _payload.length > 0)
  {
    _payload = JSON.parse(_payload).results;
    return _payload;
    }
  }
//-------------------------------------------------------------------------------------------------
function queryStations(uid,suid){
	var dataList = queryByRows(uid+"."+suid,1);
	if(dataList != null && dataList.length > 0)
	{
		 for (var i in dataList) {
			 if(dataList[i] != null){
	         Object.assign(dataList[i], {id:suid});
         }
       }
     }
	return dataList;
}
//-------------------------------------------------------------------------------------------------
function queryDevices(uid,suid){
	var dataList = queryByRows(uid+"."+suid,1);
	var deviceList = dataList[0].sensor_array;
	if(deviceList != null && deviceList.length > 0)
	{
		 for (var i in deviceList) {
			 if(deviceList[i] != null){
	         Object.assign(deviceList[i], {stationName: dataList[0].Name, GatewayId:suid});
         }
       }
     }
	return deviceList;
}
function processData(dataList){
	var deviceList = dataList.sensor_array;
	if(deviceList != null && deviceList.length > 0)
	{
		 for (var i in deviceList) {
			 if(deviceList[i] != null){
	         Object.assign(deviceList[i], {stationName: dataList.Name});
         }
       }
     }
	return deviceList;
}
//-------------------------------------------------------------------------------------------------
function queryByKey(source,key){
	var _payload = _User.getDataByKey(source,key, null);
	if(_payload != null && _payload.length > 0)
	{
	  _payload = JSON.parse(_payload).results;
	  return _payload;
	  }
}
//-------------------------------------------------------------------------------------------------
function queryByTime(source,stime,etime){
var _payload = _User.getData(source,stime,etime, null);
if(_payload != null && _payload.length > 0)
{
	_payload = JSON.parse(_payload).results;
	return _payload;
	}
}
//-------------------------------------------------------------------------------------------------
function checkListByID(Sets,Id){
	var check =false;
	if(Sets != null && Sets.length > 0)
	{
		for (var i in Sets) {
			 if(Sets[i].id == Id){
           return check=true;
			 }
		 }
	 }
	 return check;
}
function queryListByID(Sets,Id){
	if(Sets != null && Sets.length > 0)
	{
		for (var i in Sets) {
			 if(Sets[i].id == Id){
           return Sets[i];
			 }
		 }
	 }
	 return null;
}

function MergeZoneAndGateway(_zone, _gatweway)
{
	 if(_zone != null && _gatweway != null)
         for(var _gwID in _gatweway)
         {
        	 var _gwItem = _PlantGateway[_gwID];

        	 _gwItem.zone = null;
        	 for(var i=0;i<_zone.length;i++)
        	 {
        		 var _zoneItem = _zone[i];
        		 var _isInList = false;
        		 for(var j=0;j<_zoneItem.gw_list.length;j++) if(_zoneItem.gw_list[j] == _gwID)
             { _gwItem.zone = _zoneItem; break; }
        	 }
				}
			}
//==========================================Navigation=================================================
function createSelect(data){
	var SelectList = document.createElement('SELECT');
	    SelectList.className = "form-control"
	var option = document.createElement("option");
	    option.value ="0";
	    option.text  = "全部";
	    SelectList.appendChild(option);

	for (var i = 0; i < data.length; i++) {
    var option = document.createElement("option");
    option.value = data[i].value;
    option.text = data[i].text;
    SelectList.appendChild(option);
     }
	return SelectList;
}

function createNav(id,labelText,element){
	var col_Div = document.createElement('DIV');
	    col_Div.className ="col-md-2";
			col_Div.id =id;
	var form_Div = document.createElement('DIV');
			form_Div.className ="m-form__group m-form__group--inline";
	var label_Div = document.createElement('DIV');
			label_Div.className ="m-form__label";
	var label = document.createElement('LABEL');
			label.innerHTML = labelText;
			label_Div.appendChild(label);
  var control_Div = document.createElement('DIV');
	    control_Div.className = "m-form__control";
			control_Div.style.width = "200px";
			control_Div.title ="Fixed: 200px";
			control_Div.appendChild(element);
			form_Div.appendChild(label_Div);
			form_Div.appendChild(control_Div);
			col_Div.appendChild(form_Div);
	return col_Div;
}
//-------------------------------------------------------------------------------------------------
function hideNavs(){
	var navs = document.getElementById("select_Group").childNodes;
	for(var i=0; i<navs.length;i++){
		if(typeof navs[i] != "undefined" && typeof navs[i] == "object"){
		navs[i].style.display = "none";
	  }
	}
}
//-------------------------------------------------------------------------------------------------
function removeNavs(){
	var navs = document.getElementById("select_Group").childNodes;
	console.log(document.getElementById("select_Group"));
	console.log(navs);
	for(var i=0; i<navs.length;i++){
		console.log(navs[i]);
		console.log(i);
		//if(existElementAsObject(navs[i])){
		      document.getElementById("select_Group").removeChild(navs[i]);
			//}

	}
	console.log(document.getElementById("select_Group"));
	console.log(navs);
}
//-------------------------------------------------------------------------------------------------
function existNav(NavsId){
	var Navs = document.getElementById("select_Group").childNodes;
	var exist = false;
	for(var i=0; i<Navs.length;i++){
		if(Navs[i].id == NavsId){
			 exist = true;
		}
	}
	return exist;
}
//-------------------------------------------------------------------------------------------------
function createBtn(id,b_class,i_class,text){
	var button = document.createElement('A');
	    //button.href = "javascript:void(0);";
			button.href = "#";
			button.className = b_class;
			button.id=id;
	var button_span = document.createElement('SPAN');
	var button_i = document.createElement('I');
	    button_i.className = i_class;
	var text_span = document.createElement('SPAN');
	    text_span.innerHTML =text;
			button_span.appendChild(button_i);
			button_span.appendChild(text_span);
			button.appendChild(button_span);
	return button;
}
//-------------------------------------------------------------------------------------------------
function createBack_Btn(){
	var Back_string ="<h5 class=\"\"><a href=\"#\" class=\"m-link\"><i class=\"la la-mail-reply\"></i> 返回查詢列表</a></h5></div>";
			Back_string = Back_string.trim();
	var back_Btn = document.createElement('DIV');
			back_Btn.className = "col-md-2";
			back_Btn.id = "back_Btn";
			back_Btn.innerHTML = Back_string;
		return back_Btn;
}
//-------------------------------------------------------------------------------------------------
function removeBtn(btnId){
	var buttons = document.getElementById("button_Group").childNodes;
	for(var i=0; i<buttons.length;i++){
		if(existElementAsObject(buttons[i])){
		   if(buttons[i].id == btnId){
		      document.getElementById("button_Group").removeChild(buttons[i]);
		      return;
		    }
			}
	  }
}
function queryBtn(buttonId){
	var buttons = document.getElementById("button_Group").childNodes;
	for(var i=0; i<buttons.length;i++){
		if(existElementAsObject(buttons[i])){
		   if(buttons[i].id == buttonId){
			           return buttons[i];
		     }
			}
	 }
}
function queryBtnPosition(buttonId){
	var buttons = document.getElementById("button_Group").childNodes;
	for(var i=0; i<buttons.length;i++){
		if(existElementAsObject(buttons[i])){
		   if(buttons[i].id == buttonId){
			           return i;
		     }
			}
	 }
}
function replaceBtn(new_btn,old_btn){
   document.getElementById("button_Group").insertBefore(new_btn,old_btn);
	 var buttonId = old_btn.id;
   removeBtn(buttonId);
}
//-------------------------------------------------------------------------------------------------
function createDatepicker(){
	var picker_string ="<label>資料日期</label><div class=\"input-daterange input-group\" ><input type=\"text\" class=\"form-control m-input\" name=\"start\" placeholder=\"開始日期\"><div class=\"input-group-append\"><span class=\"input-group-text\">至</span></div><input type=\"text\" class=\"form-control\" name=\"end\" placeholder=\"結束日期\"></div>";
	var picker_string = picker_string.trim();
	var picker = document.createElement('DIV');
	    picker.className = "col-md-2";
			picker.id ="H_DatePicker";
			picker.innerHTML = picker_string;
		  return picker;
}
function StationNav(){
//------------------dropdownList-------------------------------
  var ZoneList =[{text:"淡水區",value:"1"},{text:"北投區",value:"2"},{text:"蘆洲區",value:"3"}];
	var select_group = document.getElementById("select_Group");
	var zone_select  = createSelect(ZoneList);
	var zone_Nav     = createNav("zone_Nav","區域:",zone_select);
	    select_group.appendChild(zone_Nav);
			zone_Nav.style.display = "none";
//-------------------------------------------------------------
  var RiverList =[{text:"淡水河",value:"1"},{text:"三峽河",value:"2"}];
  var river_select  = createSelect(RiverList);
  var river_Nav     = createNav("river_Nav","河川別:",river_select);
		  select_group.appendChild(river_Nav);
			river_Nav.style.display = "none";
//--------------------------------------------------------------
  var SideList =[{text:"左岸",value:"1"},{text:"右岸",value:"2"}];
  var side_select  = createSelect(SideList);
  var side_Nav     = createNav("side_Nav","岸別:",side_select);
		  select_group.appendChild(side_Nav);
			side_Nav.style.display = "none";

//-----------------------Button---------------------------------
var button_group = document.getElementById("button_Group");
var download = createBtn("download_Btn","btn m-btn--pill m-btn m-btn--icon  btn-primary","fa fa-save","下載");
    download.onclick=  function() { $('#m_modal_download').modal()};
/*var search = createBtn("search_Btn","btn m-btn--pill m-btn m-btn--icon btn-accent","fa fa-search","查詢");
    search.onclick=  function() {showHistory("G-300-0-0","46-00-000002",10);};*/
    button_group.appendChild(download);
		//button_group.appendChild(search);


}

function DeviceNav(){
	var TypeList =[{text:"水位計",value:"43"},{text:"地震儀",value:"46"},{text:"加速度計",value:"44"},{text:"傾斜儀",value:"45"}];
	var select_group = document.getElementById("select_Group");
	var type_select  = createSelect(TypeList);
	    type_select.addEventListener( 'change', function() {
			if(this.value == "43") {
					showWaterTable();
			}
			else if(this.value == "46")
			{
					showEarthTable();
			}
			else if(this.value == "44")
			{
					showAccelTable();
			}
			else if(this.value == "45")
			{
					showInclinTable();
			}
	  });
	var type_Nav     = createNav("type_Nav","裝置類型:",type_select);
			select_group.appendChild(type_Nav);
			type_Nav.style.display = "none";

}
function initialNav(){
    StationNav();
		DeviceNav();
}
//================================================================================
			function getDevice(){
			var device_list = queryByRows("River-Management.Device-Basic",2);
		 if(device_list != null && device_list.length > 0)
		 {
			 for (var i in device_list) {
				 if(device_list[i] != null){
      var station = queryByKey("River-Management.Station",device_list[i].GatewayId);
			document.getElementById("id_"+device_list[i].ukey).innerHTML = device_list[i].ukey;
			document.getElementById("name_"+device_list[i].ukey).innerHTML = device_list[i].Name;
			document.getElementById("StationName_"+device_list[i].ukey).innerHTML = station[0].Name;
			document.getElementById("X_"+device_list[i].ukey).innerHTML = device_list[i].CoordinateX;
			document.getElementById("Y_"+device_list[i].ukey).innerHTML = device_list[i].CoordinateY;
			document.getElementById("equitDate_"+device_list[i].ukey).innerHTML = device_list[i].equitDate;
			document.getElementById("equitPerson_"+device_list[i].ukey).innerHTML = device_list[i].equitPerson;
			document.getElementById("maintainDate_"+device_list[i].ukey).innerHTML = device_list[i].maintainDate;
			document.getElementById("maintainPerson_"+device_list[i].ukey).innerHTML = device_list[i].maintainPerosn;
		}
   }
  }
}
//==========================================getDataList==================================================

//---------------------------------------------------------------------------------------------
function getStationList(station_list){
   //var station_list = queryByRows("River-Management.Station",5);

	 if(station_list != null && station_list.length > 0)
	 {
		 for (var i in station_list) {
			 if(station_list[i] != null){
    Station_info = station_list[i];
		document.getElementById("id_"+station_list[i].id).innerHTML = station_list[i].id;
		document.getElementById("name_"+station_list[i].id).innerHTML = station_list[i].Name;
		document.getElementById("zone_"+station_list[i].id).innerHTML = station_list[i].Zone;
		document.getElementById("admin_"+station_list[i].id).innerHTML = station_list[i].AdminUnit;
		document.getElementById("river_"+station_list[i].id).innerHTML = station_list[i].River;
		document.getElementById("side_"+station_list[i].id).innerHTML = station_list[i].RiverSide;
		document.getElementById("person_"+station_list[i].id).innerHTML = station_list[i].StationPerson;
		document.getElementById("X_"+station_list[i].id).innerHTML = station_list[i].CoordinateX;
		document.getElementById("Y_"+station_list[i].id).innerHTML = station_list[i].CoordinateY;
		document.getElementById("address_"+station_list[i].id).innerHTML = station_list[i].Address;
		document.getElementById("Ip_"+station_list[i].id).innerHTML = station_list[i].IP;
		document.getElementById("button_"+station_list[i].id).addEventListener("click", function(){showDetail_Station(this);});
	    }
	  }
	}
}
//-------------------------------------update-EarthQuake-status-------------------------------------------------------
function updateEQ(source,deviceId){
	var list = queryByRows(source+"."+deviceId,1);

	if(list != null && list.length > 0)
	{
		document.getElementById("axis_mix_"+deviceId).innerHTML = list[0].axis_mix;
		document.getElementById("mmi_level_"+deviceId).innerHTML = list[0].mmi_level;
		document.getElementById("x_axis_"+deviceId).innerHTML = list[0].x_axis;
		document.getElementById("y_axis_"+deviceId).innerHTML = list[0].y_axis;
		document.getElementById("z_axis_"+deviceId).innerHTML = list[0].z_axis;
	}

}
//----------------------------------------------------------------------------------------------
function _getDeviceList(list){
	if(list != null && list.length > 0)
	{
		for (var i in list) {
			 if(list[i] != null){
				 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
				 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
				 document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
				 document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
			 }
		 }
	 }
 }
//----------------------------------------------------------------------------------------------
function getDeviceList(list){
	if(list != null && list.length > 0)
	{
		for (var i in list) {
			 if(list[i] != null){
				 var suid = list[i].suid;
	 			 var type = suid.substring(0, 2);
	 		  if(type == "43"){

	/*var search = document.createElement("BUTTON");
			search.type = "button"
			search.innerText = "查詢"
			search.id ="search_Btn_"+list[i].suid;
	 document.getElementById("button_"+list[i].suid).appendChild(search);
	 document.getElementById("search_Btn_"+list[i].suid).addEventListener("click", function(){showHistory("Sensor",list[i].suid,10);});*/
	 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
	 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
	 document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
	 document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
	 document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
	 //updateWS("Sensor",list[i].suid);
        }
				else if(type == "46"){
	 /*var search = document.createElement("BUTTON");
 	     search.type = "button";
			 search.innerText = "查詢"
			 search.id ="search_Btn_"+list[i].suid;

 	 document.getElementById("button_"+list[i].suid).appendChild(search);
 	 document.getElementById("search_Btn_"+list[i].suid).addEventListener("click", function(){showHistory("Sensor",list[i].suid,10);});*/
 	 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
 	 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
	 document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
	 document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
	 document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
	 updateEQ("Sensor",list[i].suid);
				}
				else if(type == "44"){
	 /*var search = document.createElement("BUTTON");
 	     search.type = "button";
			 search.innerText = "查詢"
			 search.id ="search_Btn_"+list[i].suid;
			 console.log(list[i].suid);

 	 document.getElementById("button_"+list[i].suid).appendChild(search);
 	 document.getElementById("search_Btn_"+list[i].suid).addEventListener("click", function(){showHistory("Sensor",list[i].suid,10);});*/
 	 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
 	 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
	 document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
	 document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
	 document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
	 //updateAC("Sensor",list[i].suid);
				}
				else if(type == "45"){
	 /*var search = document.createElement("BUTTON");
 	     search.type = "button";
			 search.innerText = "查詢"
			 search.id ="search_Btn_"+list[i].suid;

 	 document.getElementById("button_"+list[i].suid).appendChild(search);
 	 document.getElementById("search_Btn_"+list[i].suid).addEventListener("click", function(){showHistory("Sensor",list[i].suid,10);});*/
 	 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
 	 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
	 document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
	 document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
	 document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
	 //updateIC("Sensor",list[i].suid);
				}
      }
    }
  }
}
//-------------------------------------get-WaterStage-list-------------------------------------------------------
function getWaterlist(list){
if(list != null && list.length > 0)
{
	for (var i in list) {
		 if(list[i] != null){
			 var suid = list[i].suid;
			 var type = suid.substring(0, 2);
			if(type == "43"){


 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
 document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
 document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
 document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
 document.getElementById("button_"+list[i].suid).addEventListener("click", function(){
 removeTables();
 hideNavs();
 var id = this.id;
 var start = id.indexOf("_");
 var deviceId = id.substring(start+1,id.length);
 if(!existElementById("back_Btn")){
 var back_Btn = createBack_Btn();
 var select_group = document.getElementById("select_Group");
		 select_group.appendChild(back_Btn);
	 }
	 if(!existElementById("H_DatePicker")){
	 var picker = createDatepicker();
	 var select_group = document.getElementById("select_Group");
			 select_group.appendChild(picker);
		 }
 var back = document.getElementById("back_Btn");

 var type = deviceId.substring(0, 2);
	 if(type.includes("46") && deviceId!=null){
					EQ_HistoryTable("Sensor",deviceId,10);
					back.onclick=  function() {showEarthTable();};
	 }
	 else if(type.includes("43") && deviceId!=null){
					WS_History("Sensor",deviceId,10);
					back.onclick=  function() {showWaterTable();};
	 }
	document.getElementById("portlet_title").innerHTML =deviceId;});
 updateWS("Sensor",list[i].suid);
			   }
		   }
	   }
   }
 }
 //-------------------------------------get-EarthQuake-list-------------------------------------------------------
 function getEarthlist(list){
 if(list != null && list.length > 0)
 {
 	for (var i in list) {
 		 if(list[i] != null){
 			 var suid = list[i].suid;
 			 var type = suid.substring(0, 2);
 			if(type == "46"){

		  	 //document.getElementById("button_"+list[i].suid).addEventListener("click", function(){showHistory("Sensor",this,10);});
		  	 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
		  	 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
		 	   document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
		 	   document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
		 	   document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;

				 document.getElementById("button_"+list[i].suid).addEventListener("click", function(){
				 removeTables();
				 hideNavs();

				 var id = this.id;
				 var start = id.indexOf("_");
				 var deviceId = id.substring(start+1,id.length);
				 console.log(deviceId);
				 if(!existElementById("back_Btn")){
				 var back_Btn = createBack_Btn();
				 var select_group = document.getElementById("select_Group");
						 select_group.appendChild(back_Btn);
					 }
				if(!existElementById("H_DatePicker")){
					 var picker = createDatepicker();
					 var select_group = document.getElementById("select_Group");
							 select_group.appendChild(picker);
						 }
				 var back = document.getElementById("back_Btn");

				 var type = deviceId.substring(0, 2);
					 if(type.includes("46") && deviceId!=null){
									EQ_HistoryTable("Sensor",deviceId,10);
									back.onclick=  function() {showEarthTable();};
					 }
					 else if(type.includes("43") && deviceId!=null){
									WS_History("Sensor",deviceId,10);
									back.onclick=  function() {showWaterTable();};
					 }
					document.getElementById("portlet_title").innerHTML =deviceId;});
		 	   updateEQ("Sensor",list[i].suid);

			    }
		    }
	    }
    }
  }
	//-------------------------------------get-acceleration-list-------------------------------------------------------
	function getAccellist(list){
	if(list != null && list.length > 0)
	{
	 for (var i in list) {
			if(list[i] != null){
				 var suid = list[i].suid;
				 var type = suid.substring(0, 2);
			   if(type == "44"){

				 	 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
				 	 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
					 document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
					 document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
					 document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
					 document.getElementById("button_"+list[i].suid).addEventListener("click", function(){
					 removeTables();
					 hideNavs();

					 var id = this.id;
					 var start = id.indexOf("_");
					 var deviceId = id.substring(start+1,id.length);
					 console.log(deviceId);
					 if(!existElementById("back_Btn")){
					 var back_Btn = createBack_Btn();
					 var select_group = document.getElementById("select_Group");
							 select_group.appendChild(back_Btn);
						 }
					if(!existElementById("H_DatePicker")){
						 var picker = createDatepicker();
						 var select_group = document.getElementById("select_Group");
								 select_group.appendChild(picker);
							 }
					 var back = document.getElementById("back_Btn");
					     back.onclick=  function() {showAccelTable();};

               AC_History("Sensor",deviceId,10);


						document.getElementById("portlet_title").innerHTML =deviceId;});
					 updateAC("Sensor",list[i].suid);

			    }
		    }
	    }
   }
}
//----------------------------------get-inclinometer-list-------------------------------------------------------------
function getInclilist(list){
if(list != null && list.length > 0)
{
 for (var i in list) {
		if(list[i] != null){
			 var suid = list[i].suid;
			 var type = suid.substring(0, 2);
			 if(type == "45"){

			 	 document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
			 	 document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
				 document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
				 document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
				 document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
				 document.getElementById("button_"+list[i].suid).addEventListener("click", function(){
				 removeTables();
				 hideNavs();

				 var id = this.id;
				 var start = id.indexOf("_");
				 var deviceId = id.substring(start+1,id.length);
				 console.log(deviceId);
				 if(!existElementById("back_Btn")){
				 var back_Btn = createBack_Btn();
				 var select_group = document.getElementById("select_Group");
						 select_group.appendChild(back_Btn);
					 }
				if(!existElementById("H_DatePicker")){
					 var picker = createDatepicker();
					 var select_group = document.getElementById("select_Group");
							 select_group.appendChild(picker);
						 }
				 var back = document.getElementById("back_Btn");
						 back.onclick=  function() {showInclinTable();};

						 AC_History("Sensor",deviceId,10);


					document.getElementById("portlet_title").innerHTML =deviceId;});
				 updateIC("Sensor",list[i].suid);
			   }
		   }
	   }
   }
 }
//----------------------------------------------------------------------------------------------
function getDetail_Info(station_info){
	document.getElementById("detail_info_name").innerText =station_info.Name;
	document.getElementById("detail_info_zone").innerText =station_info.Zone;
	document.getElementById("detail_info_admin").innerText =station_info.AdminUnit;
	document.getElementById("detail_info_side").innerText =station_info.RiverSide;
	document.getElementById("detail_info_river").innerText =station_info.River;
	document.getElementById("detail_info_person").innerText =station_info.StationPerson;
	document.getElementById("detail_info_X_97").innerText =station_info.CoordinateX;
	document.getElementById("detail_info_Y_97").innerText =station_info.CoordinateY;

}
//-------------------------------------update-WaterStage-status-------------------------------------------------------
function updateWS(source,deviceId){
	var list = queryByRows(source+"."+deviceId,1);

	if(list != null && list.length > 0 && existElementById("water_stage_"+deviceId))
	{
		document.getElementById("water_stage_"+deviceId).innerHTML = list[0].water_stage;
	}
	else if(existElementById("water_stage_"+deviceId)){
		document.getElementById("water_stage_"+deviceId).innerHTML ="無資料";
	}
}
//-------------------------------------update-acceleration-status-------------------------------------------------------
function updateAC(source,deviceId){
	var list = queryByRows(source+"."+deviceId,1);

	if(list != null && list.length > 0 && existElementById("acceleration_"+deviceId))
	{
		document.getElementById("acceleration_"+deviceId).innerHTML = list[0].acceleration;
	}
	else if(existElementById("acceleration_"+deviceId)){
		document.getElementById("acceleration_"+deviceId).innerHTML ="無資料";
	}
}
//-------------------------------------update-inclinometer-status-------------------------------------------------------
function updateIC(source,deviceId){
	var list = queryByRows(source+"."+deviceId,1);

	if(list != null && list.length > 0 && existElementById("inclinometer_"+deviceId))
	{
		document.getElementById("inclinometer_"+deviceId).innerHTML = list[0].inclinometer;
	}
	else if(existElementById("inclinometer_"+deviceId)){
		document.getElementById("inclinometer_"+deviceId).innerHTML ="無資料";
	}
}
//-------------------------------------get-EarthQuake-history--------------------------------------------
function getHistory_EQ(list){
	if(list != null && list.length > 0)
	{
		for (var i in list) {
			  if(list[i] != null){
	         document.getElementById("id_H_"+list[i].ukey).innerHTML =list[i].ukey;
					 document.getElementById("axis_mix_H_"+list[i].ukey).innerHTML =list[i].axis_mix;
					 document.getElementById("mmi_level_H_"+list[i].ukey).innerHTML =list[i].mmi_level;
					 document.getElementById("x_axis_H_"+list[i].ukey).innerHTML =list[i].x_axis;
					 document.getElementById("y_axis_H_"+list[i].ukey).innerHTML =list[i].y_axis;
					 document.getElementById("z_axis_H_"+list[i].ukey).innerHTML =list[i].z_axis;
					 document.getElementById("time_H_"+list[i].ukey).innerHTML =list[i].system_time;
				 }
			 }
		 }
	 }
//-------------------------------------get-EarthQuake-history--------------------------------------------
function getHistory(list){
	 	if(list != null && list.length > 0)
	 	{
	 		for (var i in list) {
	 			  if(list[i] != null){
					 document.getElementById("H_id_"+list[i].ukey).innerHTML =list[i].ukey;
					 document.getElementById("H_waterStage_"+list[i].ukey).innerHTML =list[i].water_stage;
					 document.getElementById("H_time_"+list[i].ukey).innerHTML =Unix_timestampToDate(list[i].system_time);
	 			 }
	 		 }
	 	 }
	 }
//-------------------------------------get-EarthQuake-history--------------------------------------------
function getHistory_AC(list){
	 	if(list != null && list.length > 0)
	 	{
	 		for (var i in list) {
	 			  if(list[i] != null){
						document.getElementById("H_id_"+list[i].ukey).innerHTML =list[i].ukey;
						document.getElementById("H_acceleration_"+list[i].ukey).innerHTML =list[i].acceleration;
						document.getElementById("H_time_"+list[i].ukey).innerHTML =Unix_timestampToDate(list[i].system_time);
	 				 }
	 			 }
	 		 }
	 	 }
		 //-------------------------------------get-EarthQuake-history--------------------------------------------
function getHistory_IC(list){
		 	 	if(list != null && list.length > 0)
		 	 	{
		 	 		for (var i in list) {
		 	 			  if(list[i] != null){
		 						document.getElementById("H_id_"+list[i].ukey).innerHTML =list[i].ukey;
		 						document.getElementById("H_inclinometer_"+list[i].ukey).innerHTML =list[i].inclinometer;
		 						document.getElementById("H_time_"+list[i].ukey).innerHTML =Unix_timestampToDate(list[i].system_time);
		 	 				 }
		 	 			 }
		 	 		 }
		 	 	 }
//=============================================generateID================================================
function genDataID_suid(data_list,id_list){
	//var station_list = queryByRows("River-Management.Station",1);
  var dataId = [];
	for (var i in data_list) {
		var temp =[];
		 for(var j in id_list){
		  temp.push(id_list[j]+data_list[i].suid);
		 }
		dataId.push(temp);
	 }
	return dataId;
}
//-------------------------------------------------------------------------------------
function genDataID_ukey(data_list,id_list){
	//var station_list = queryByRows("River-Management.Station",1);
  var dataId = [];
	for (var i in data_list) {
		var temp =[];
		 for(var j in id_list){
		  temp.push(id_list[j]+data_list[i].ukey);
		 }
		dataId.push(temp);
	 }
	return dataId;
}
//-------------------------------------------------------------------------------------
function _genDataIDByType(data_list,id_list,dataType){
  var dataId = [];
	for (var i in data_list) {
		var suid = data_list[i].suid;
		var type = suid.substring(0, 2);
		if(type == dataType && data_list[i]!= null){
		var temp =[];
		 for(var j in id_list){
		     temp.push(id_list[j]+data_list[i].suid);
		    }
		 dataId.push(temp);
	   }
	return dataId;
   }
}
//-------------------------------------------------------------------------------------
function _genWaterID(data_list,id_list){
	//var list = queryByRows("Area-Tainan.43-00-000001",1);
	var dataId = [];
if(data_list != null && data_list.length > 0){
	  for (var i in data_list) {
			var suid = data_list[i].suid;
			var type = suid.substring(0, 2);
		  if(type == "43" && data_list[i]!= null){
				var temp =[];
				for(var j in id_list){
				    temp.push(id_list[j]+data_list[i].suid);
				    }
			    dataId.push(temp);
			  }
	    }
    }
	 return dataId;
}
//-------------------------------------------------------------------------------------
function _genEarthID(data_list,id_list){
	//var list = queryByRows("Area-Tainan.43-00-000001",1);
	var dataId = [];
if(data_list != null && data_list.length > 0){
	  for (var i in data_list) {
			var suid = data_list[i].suid;
			var type = suid.substring(0, 2);
		  if(type == "46" && data_list[i]!= null){
				var temp =[];
				for(var j in id_list){
				    temp.push(id_list[j]+data_list[i].suid);
				    }
			    dataId.push(temp);
			  }
	    }
    }
	 return dataId;
}
//-------------------------------------------------------------------------------------
function _genAccelID(data_list,id_list){
	//var list = queryByRows("Area-Tainan.43-00-000001",1);
	console.log(data_list);
	var dataId = [];
if(data_list != null && data_list.length > 0){
	  for (var i in data_list) {
			var suid = data_list[i].suid;
			var type = suid.substring(0, 2);
		  if(type == "44" && data_list[i]!= null){
				var temp =[];
				for(var j in id_list){
				    temp.push(id_list[j]+data_list[i].suid);
				    }
			    dataId.push(temp);
			  }
	    }
    }
	 return dataId;
}
//-------------------------------------------------------------------------------------
function _genInclinID(data_list,id_list){
	//var list = queryByRows("Area-Tainan.43-00-000001",1);
	var dataId = [];
if(data_list != null && data_list.length > 0){
	  for (var i in data_list) {
			var suid = data_list[i].suid;
			var type = suid.substring(0, 2);
		  if(type == "45" && data_list[i]!= null){
				var temp =[];
				for(var j in id_list){
				    temp.push(id_list[j]+data_list[i].suid);
				    }
			    dataId.push(temp);
			  }
	    }
    }
	 return dataId;
}
//-------------------------------------------------------------------------------------
function genStationID(station_list){
	//var station_list = queryByRows("River-Management.Station",1);
  var stationId = [];
	for (var i in station_list) {
		stationId[i] = ["id_"+station_list[i].id,
		               "name_"+station_list[i].id,
									 "zone_"+station_list[i].id,
									 "admin_"+station_list[i].id,
									 "river_"+station_list[i].id,
									 "side_"+station_list[i].id,
									 "person_"+station_list[i].id,
									 "X_"+station_list[i].id,
									 "Y_"+station_list[i].id,
									 "address_"+station_list[i].id,
									 "Ip_"+station_list[i].id];

	}
	return stationId;
}
//-------------------------------------------------------------------------------------
function genWaterID(list){
	//var list = queryByRows("Area-Tainan.43-00-000001",1);
	var Id = [];
	var j =0;
if(list != null && list.length > 0){
	  for (var i in list) {
		  if(list[i].Type == "43" && list[i]!= null){
	       Id[j] = ["id_"+list[i].ukey,
 					        "name_"+list[i].ukey,
 					        "StationName_"+list[i].ukey,
 					        "X_"+list[i].ukey,
 					        "Y_"+list[i].ukey,
					        "water_stage_"+list[i].ukey,
								  "button_"+list[i].ukey];
				    j++;
			      }
	       }
      }
	 return Id;
  }
//-------------------------------------------------------------------------------------
function genEarthID(list){
  var Id = [];
	var j =0;
if(list != null && list.length > 0){
	for (var i in list) {
		if(list[i].Type == "46" && list[i]!= null){
			 Id[j] = ["id_"+list[i].ukey,
								"name_"+list[i].ukey,
								"StationName_"+list[i].ukey,
								"X_"+list[i].ukey,
								"Y_"+list[i].ukey,
								"axis_mix_"+list[i].ukey,
								"mmi_level_"+list[i].ukey,
								"x_axis_"+list[i].ukey,
								"y_axis_"+list[i].ukey,
								"z_axis_"+list[i].ukey,
								"button_"+list[i].ukey];
				j++;
			      }
	       }
      }
	 return Id;
  }
//-------------------------------------------------------------------------------------
function genEarthID_H(list){
	var Id = [];
	var j =0;
	for (var i in list) {
			 Id[j] = ["id_H_"+list[i].ukey,
								"axis_mix_H_"+list[i].ukey,
								"mmi_level_H_"+list[i].ukey,
								"x_axis_H_"+list[i].ukey,
								"y_axis_H_"+list[i].ukey,
								"z_axis_H_"+list[i].ukey,
							  "time_H_"+list[i].ukey];
				j++;
			}
	 return Id;
}

//====================================showData=======================================
function showHistory(uid,button,rows){
	removeTables();
	hideNavs();
	//removeNavs();
	console.log(button);
	console.log(button.id);
	var id = button.id;
	var start = id.indexOf("_");
	var deviceId = id.substring(start+1,id.length);
	console.log(deviceId);
	if(!existElementById("back_Btn")){
	var Back_string ="<h5 class=\"\"><a href=\"#\" class=\"m-link\"><i class=\"la la-mail-reply\"></i> 返回查詢列表</a></h5></div>";
			Back_string = Back_string.trim();
	var back_Btn = document.createElement('DIV');
			back_Btn.className = "col-md-2";
			back_Btn.id = "back_Btn";
			back_Btn.innerHTML = Back_string;
	var select_group = document.getElementById("select_Group");
			select_group.appendChild(back_Btn);
		}
	var back = document.getElementById("back_Btn");

	var type = deviceId.substring(0, 2);
		if(type.includes("46") && deviceId!=null){
		       EQ_HistoryTable(uid,deviceId,rows);
					 back.onclick=  function() {showEarthTable();};
    }
		else if(type.includes("43") && deviceId!=null){
		       WS_History(uid,deviceId,rows);
					 back.onclick=  function() {showWaterTable();};
    }


	 document.getElementById("portlet_title").innerHTML =deviceId;
}
//-------------------------------------------------------------------------------------
function showDetail_Station(button){
	removeTables();
	hideNavs();
	//removeNavs();
	var id = button.id;
	var start = id.indexOf("_");
	var stationId = id.substring(start+1,id.length);
	if(Station_info == null || Station_info.id!= stationId){
       var list = queryStations("G-300-0-0",stationId);
					 Station_info = list[0];
}
	if(!existElementById("back_Btn")){
		  var back_Btn = createBack_Btn();
	    var select_group = document.getElementById("select_Group");
			    select_group.appendChild(back_Btn);
		}
	    var back = document.getElementById("back_Btn");
			    back.onclick=  function() {showStationTable();};
			var detail = build_Detail_page();
			var table_div = document.getElementById("table_div");
			    table_div.appendChild(detail);
					getDetail_Info(Station_info);
//--------------------------------------------------------
			var has_devices = processData(Station_info);
			var deviceTable = _DeviceTable(has_devices);
			var detail_table = document.getElementById("detail_page_table");
					detail_table.appendChild(deviceTable);
					document.getElementById("detail_table_title").innerText = "監測站所擁有裝置";
         _getDeviceList(has_devices);

	    document.getElementById("portlet_title").innerHTML = Station_info.Name;
			Station_info = null;
}
//-------------------------------------------------------------------------------------
function showStationTable()
{
	document.getElementById("m_portlet_index").style.display = "block";
	removeTables();
	if(existElementById("back_Btn")){removeElement("select_Group","back_Btn");}
	var table_div = document.getElementById("table_div");
	var station_list = queryStations("G-300-0-0","Gateway000001");
	var stationTable = _stationTable(station_list,"station_table");
	table_div.appendChild(stationTable);
	var search = document.createElement("BUTTON");
	search.type = "button"
	search.innerText = "查詢"
	addColumns(stationTable,"詳情",search,"button");

	getStationList(station_list);
	//showTable("station_table");
	//document.getElementById("station_table").style.display = "";
	document.getElementById("zone_Nav").style.display = "";
	document.getElementById("river_Nav").style.display = "";
	document.getElementById("side_Nav").style.display = "";
	document.getElementById("type_Nav").style.display = "none";
	document.getElementById("portlet_title").innerHTML ="監控站";
}
//-------------------------------------------------------------------------------------
function showWaterTable(){
				document.getElementById("m_portlet_index").style.display = "block";
				removeTables();
				if(existElementById("back_Btn")){removeElement("select_Group","back_Btn");}
				if(existElementById("H_DatePicker")){removeElement("select_Group","H_DatePicker");}
				var deviceList = queryDevices("G-300-0-0","Gateway000001");
				var table_div = document.getElementById("table_div");
				var waterTable = _waterTable(deviceList,"water_table");


				var search = document.createElement("BUTTON");
						search.type = "button"
						search.innerText = "查詢"
				addColumns(waterTable,"詳情",search,"button");
				table_div.appendChild(waterTable);

				getWaterlist(deviceList);
				//showTable("water_table");
				//document.getElementById("station_table").style.display = "none";
				document.getElementById("zone_Nav").style.display = "none";
				document.getElementById("river_Nav").style.display = "none";
				document.getElementById("side_Nav").style.display = "none";
				document.getElementById("type_Nav").style.display = "";
				//document.getElementById("device_table").style.display = "";
				document.getElementById("portlet_title").innerHTML ="水位計";
				}
//-------------------------------------------------------------------------------------
function showEarthTable(){
	              //removeNavs();
								removeTables();
								document.getElementById("m_portlet_index").style.display = "block";

								if(existElementById("back_Btn")){removeElement("select_Group","back_Btn");}
								if(existElementById("H_DatePicker")){removeElement("select_Group","H_DatePicker");}
								var deviceList = queryDevices("G-300-0-0","Gateway000001");
								var table_div = document.getElementById("table_div");
								var earthTable = _earthquakeTable(deviceList);

								var search = document.createElement("BUTTON");
										search.type = "button"
										search.innerText = "查詢"
								addColumns(earthTable,"詳情",search,"button");
								table_div.appendChild(earthTable);
								getEarthlist(deviceList);

								document.getElementById("zone_Nav").style.display = "none";
								document.getElementById("river_Nav").style.display = "none";
								document.getElementById("side_Nav").style.display = "none";
								document.getElementById("type_Nav").style.display = "";
								document.getElementById("portlet_title").innerHTML ="地震儀";
					}
//--------------------------------------------------------------------------------------
function checkDeviceType(device,compare){
	var check = false;
	var suid = device.suid;
	var type = suid.substring(0, 2);
	if(type == compare){
		check = true;
	}
	return check;
}

function showAccelTable(){
				document.getElementById("m_portlet_index").style.display = "block";
				removeTables();
				if(existElementById("back_Btn")){removeElement("select_Group","back_Btn");}
				if(existElementById("H_DatePicker")){removeElement("select_Group","H_DatePicker");}
				var deviceList = queryDevices("G-300-0-0","Gateway000001");
				var table_div = document.getElementById("table_div");
				var accelTable = _accelerationTable(deviceList,"accel_table");

				var search = document.createElement("BUTTON");
						search.type = "button"
						search.innerText = "查詢"
				addColumns(accelTable,"詳情",search,"button");

				table_div.appendChild(accelTable);
				getAccellist(deviceList);
				//showTable("accel_table");
				//document.getElementById("station_table").style.display = "none";
				document.getElementById("zone_Nav").style.display = "none";
				document.getElementById("river_Nav").style.display = "none";
				document.getElementById("side_Nav").style.display = "none";
				document.getElementById("type_Nav").style.display = "";
				//document.getElementById("device_table").style.display = "";
				document.getElementById("portlet_title").innerHTML ="加速度計";
				}
//-------------------------------------------------------------------------------------
function showInclinTable(){
				document.getElementById("m_portlet_index").style.display = "block";
				removeTables();
				if(existElementById("back_Btn")){removeElement("select_Group","back_Btn");}
				if(existElementById("H_DatePicker")){removeElement("select_Group","H_DatePicker");}
				var deviceList = queryDevices("G-300-0-0","Gateway000001");
				var table_div = document.getElementById("table_div");
				var incliTable = _inclinometerTable(deviceList,"incli_table");
				var search = document.createElement("BUTTON");
						search.type = "button"
						search.innerText = "查詢"
				addColumns(incliTable,"詳情",search,"button");
				table_div.appendChild(incliTable);
				getInclilist(deviceList);

				//showTable("inclin_table");
				//document.getElementById("station_table").style.display = "none";
				document.getElementById("zone_Nav").style.display = "none";
				document.getElementById("river_Nav").style.display = "none";
				document.getElementById("side_Nav").style.display = "none";
				document.getElementById("type_Nav").style.display = "";
				//document.getElementById("device_table").style.display = "";
				document.getElementById("portlet_title").innerHTML ="傾斜儀";
				}
//====================================Element=======================================
function existElementById(id){
	var exist=false;
  var element = document.getElementById(id);
	exist = existElementAsObject(element);
	return exist;
}
//-------------------------------------------------------------------------------------
function existElementAsObject(element){
	var exist =false;
	if(element!= undefined && element!= null && typeof element == "object"&& element.nodeType === Node.ELEMENT_NODE){
		  exist = true;
		}
		return exist;
}
//-------------------------------------------------------------------------------------
function existString(text){
	var exist =false;
	if(text!= undefined && text!= null && typeof text === "string"){
		  exist = true;
		}
		return exist;
}
//-------------------------------------------------------------------------------------
function removeElement(parentId,childId){
	var child = document.getElementById(childId);
	var parent = document.getElementById(parentId);
	    parent.removeChild(child);
}

//====================================Table==================================================================
//-----------------------------------------------------------------------------------------------------------
function queryTrOnTBody(table){
	if(existElementAsObject(table)){
		var tbody = table.getElementsByTagName("TBODY");
		var tr = tbody[0].getElementsByTagName("TR");
	}
	return tr;

}
//-----------------------------------------------------------------------------------------------------------
function queryTrOnTH(table){
	//var table = queryTable(tableId);
	if(existElementAsObject(table)){
		var thead = table.getElementsByTagName("THEAD");
		var tr = thead[0].getElementsByTagName("TR");
		return tr[0];
	}

}
//-----------------------------------------------------------------------------------------------------------
function addColumns(table,th,td,tdId){
	//var table = queryTable(tableId);
	var Th = document.createElement('TH');
	    Th.innerHTML = th;
	var TH_Set = queryTrOnTH(table);
	    TH_Set.appendChild(Th);
	var Td = document.createElement('TD');
	   if(existElementAsObject(td)){
	    Td.appendChild(td);
		}
		else if(existString(td)){
			Td.innerHTML = td;
		}
	var Rows = queryTrOnTBody(table);
	for(var i in Rows){
		 if(existElementAsObject(Rows[i])){
			 var id = Rows[i].firstChild.id
			 var start = id.indexOf("_");
			 var idNumber = id.substring(start,id.length);
			  Td.firstChild.id = tdId;
				Td.firstChild.id +=idNumber;
        Rows[i].appendChild(Td);
	    }
   }
}
//-----------------------------------------------------------------------------------------------------------
function queryColumnCheck(tableId){
	var table = queryTable(tableId);
	var checks = queryColumnSet(tableId,"check");


	for(i=0;i<check.length;i++){
	var check  = checks[i].getElementsByTagName("INPUT");
  }
}
//-----------------------------------------------------------------------------------------------------------
function queryColumnSet(tableId,columnId){
	var table = queryTable(tableId);
	var columnset = [];
	    console.log(table);
			if(existElementAsObject(table)){
	var body = table.getElementsByTagName("TBODY");
	   console.log(body);
	var rows = body[0].getElementsByTagName("TR");
	   for(j=0;j<rows.length;j++){
	      var columns = rows[j].childNodes;
	      var column = queryColumnByName(columns,columnId);
				columnset.push(column);
     }
	 return columnset;
  }
}
//-----------------------------------------------------------------------------------------------------------
function queryColumnByName(columns,name){
	for(i=0;i<columns.length;i++){
		if(existElementAsObject(columns[i])){
			if(columns[i].id.includes(name)){
				return columns[i];
			}
		}
	}
}
//-----------------------------------------------------------------------------------------------------------
function _SetDataID(data){
	var Body = document.createElement('TBODY');
	var tableTD;
if(data != null && data.length > 0){

	for(var j=0; j < data.length; j++){

		var BodyTR = document.createElement('TR');
			for(var i=0; i < data[j].length; i++){
			tableTD = document.createElement('TD');

			tableTD.id = data[j][i];

			//stationTD.textContent = THtext[i];
			BodyTR.appendChild(tableTD);
				}

			Body.appendChild(BodyTR);
	 }
 }
 else{
	 var BodyTR = document.createElement('TR');
	     BodyTR.textContent = "無資料";
			 Body.appendChild(BodyTR);
 }
   return Body;

}
//-----------------------------------------------------------------------------------------------------------
function _createTable(data){
	var Table = document.createElement('TABLE');
	var Thead = document.createElement('THEAD');

	var Th;
	for(var j=0; j<1; j++){
	var HeadTR = document.createElement('TR');
		 for(var i=0; i < data.length; i++){
				 Th = document.createElement('TH');
				 Th.textContent = data[i];
				 HeadTR.appendChild(Th);
			 }
		Thead.appendChild(HeadTR);
	}
	Table.appendChild(Thead);
  //TBody = _insertData();
  //Table.appendChild(TBody);
	 return Table;
}
//-----------------------------------------------------------------------------------------------------------
function createTable(tableId,class_Name,th,dataId){
	var Table = document.createElement('TABLE');
	var Thead = document.createElement('THEAD');
	var Body;
	var Th;
	for(var j=0; j<1; j++){
	var HeadTR = document.createElement('TR');
		 for(var i=0; i < th.length; i++){
				 Th = document.createElement('TH');
				 Th.textContent = th[i];
				 HeadTR.appendChild(Th);
			 }
		Thead.appendChild(HeadTR);
	}
	Table.appendChild(Thead);
  Body = _SetDataID(dataId);
  Table.appendChild(Body);
	Table.className =class_Name;
	Table.id =tableId;
	return Table;
}
//-----------------------------------------------------------------------------------------------------------
function showTable(tableId){
				  //document.getElementById("m_portlet_index").style.display = "block";
			var tables = document.getElementById("table_div").childNodes;
			for(var i=0; i<tables.length;i++){
					if(typeof tables[i] != "undefined" && typeof tables[i] == "object"){
             if(tables[i].tagName!=null && tables[i].tagName=="TABLE"){
						if(tables[i].id == tableId){
						tables[i].style.display = "";
						}
						else{
						tables[i].style.display = "none";
						    }
					  }
					}
				}
	    }
//-----------------------------------------------------------------------------------------------------------
function hideTables(){
	var tables = document.getElementById("table_div").childNodes;
	for(var i=0; i<tables.length;i++){
		if(typeof tables[i] != "undefined" && typeof tables[i] == "object"){
		tables[i].style.display = "none";
	  }
	}
}
//-----------------------------------------------------------------------------------------------------------
function queryTable(tableId){
	var tables = document.getElementById("table_div").childNodes;
	for(var i=0; i<tables.length;i++){
		if(existElementAsObject(tables[i])){
		   if(tables[i].id == tableId){
			           return tables[i];
		     }
			}
	 }
}
//-----------------------------------------------------------------------------------------------------------
function existTable(tableId){
	var tables = document.getElementById("table_div").childNodes;
	var exist = false;
	for(var i=0; i<tables.length;i++){
		if(typeof tables[i] != "undefined" && typeof tables[i] == "object"){
		   if(tables[i].id == tableId){
			 exist = true;
		    }
	    }
		}
	return exist;
}
//-----------------------------------------------------------------------------------------------------------
function _removeTable(tableId){
	var tables = document.getElementById("table_div").childNodes;
	for(var i=0; i<tables.length;i++){
		if(typeof tables[i] != "undefined" && typeof tables[i] == "object"){
		   if(tables[i].id == tableId){
		      document.getElementById("table_div").removeChild(tables[i]);
		      return;
		    }
			}
	  }
  }
//-----------------------------------------------------------------------------------------------------------
function removeTables(){
	var tables = document.getElementById("table_div").childNodes;
	if(tables != null && tables.length > 0){
	  for(var i=0; i<tables.length;i++){
		   if(typeof tables[i] != "undefined" && typeof tables[i] == "object"){
		      document.getElementById("table_div").removeChild(tables[i]);
			}
	  }
  }
}
//-----------------------------------------------------------------------------------------------------------
function createCheck(){
	var check = document.createElement("INPUT");
      check.setAttribute("type", "checkbox");
			check.id="";
			check.value="";
  /*var span = document.createElement("span");
	var label = document.createElement("LABEL");
	    label.className="m-checkbox";
			label.appendChild(check);*/
			//label.appendChild(span);
	return check;
}
//===========================================================================================================

//function _initialTable(){}

//--------------------------stationTable--------------------------------------------------------------------
function _stationTable(station_list,id){

              var stationTH = ["GatewayID", "監測站名稱", "所屬區域", "管理機關","河川別","岸別","負責人","97座標X","97座標Y","住址","IP"];
              //var station_list = queryByRows("River-Management.Station",5);
							var stationTable = _createTable(stationTH);
							var stationId = genStationID(station_list);
							var stationBody = _SetDataID(stationId);
							    stationTable.appendChild(stationBody);
									stationTable.className ="table table-striped- table-bordered table-hover table-checkable";
									stationTable.id =id;
									return stationTable;

}
//-------------------------WaterTable-------------------------------------------------------------------------------------
function _waterTable(deviceList,id)
{
  var waterTH = ["裝置ID", "裝置名稱", "所屬監測站","97座標X","97座標Y","目前水位高度"];
  var waterID = ["id_", "name_","StationName_", "X_97_","Y_97_","water_stage_"];
  var waterId = _genWaterID(deviceList,waterID);
  var waterTable = createTable(id,"table table-striped- table-bordered table-hover table-checkable",waterTH,waterId);
  //$('#water_table').DataTable();
  return waterTable;
  //table_div.appendChild(waterTable);
}
//-------------------------earthQuakeTable---------------------------------------------------------------------------------
function _earthquakeTable(deviceList)
{
  var earthTH = ["裝置ID", "裝置名稱", "所屬監測站","97座標X","97座標Y","三軸加速度","及時震度","X軸加速度","Y軸加速度","Z軸加速度"];
  var earthID = ["id_", "name_","StationName_", "X_97_","Y_97_","axis_mix_","mmi_level_","x_axis_","y_axis_","z_axis_"];
  var earthId = _genEarthID(deviceList,earthID);
  var earthTable = createTable("earth_table","table table-striped- table-bordered table-hover table-checkable",earthTH,earthId);
  return earthTable;
}
//-------------------------accelerationTable---------------------------------------------------------------------------------
function _accelerationTable(deviceList,id){
	            var table_div = document.getElementById("table_div");
							var accelTH = ["裝置ID", "裝置名稱", "所屬監測站","97座標X","97座標Y","加速度"];
						  var accelID = ["id_", "name_","StationName_", "X_97_","Y_97_","acceleration_"];
						  var accelId = _genAccelID(deviceList,accelID);
						  var accelTable = createTable(id,"table table-striped- table-bordered table-hover table-checkable",accelTH,accelId);
							return accelTable;
								}
//-------------------------inclinometerTable---------------------------------------------------------------------------------
function _inclinometerTable(deviceList){
	            //var table_div = document.getElementById("table_div");
							var inclinTH = ["裝置ID", "裝置名稱", "所屬監測站","97座標X","97座標Y","傾斜度"];
							var inclinlID = ["id_", "name_","StationName_", "X_97_","Y_97_","inclinometer_"];
							var inclinId = _genInclinID(deviceList,inclinlID);
							var inclinTable = createTable("inclin_table","table table-striped- table-bordered table-hover table-checkable",inclinTH,inclinId);
									//table_div.appendChild(inclinTable);
									return inclinTable;


				}
//-------------------------DeviceTable--------------------------------------------------------------------------------------
function _DeviceTable(deviceList){
	var deviceTH = ["裝置ID", "裝置名稱","97座標X","97座標Y"];
	var deviceID = ["id_", "name_", "X_97_","Y_97_"];
	var deviceId = genDataID_suid(deviceList,deviceID);
	var deviceTable = createTable("device_table_","table table-striped- table-bordered table-hover table-checkable",deviceTH,deviceId);
      return deviceTable;
}
//----------------------acceleration-History-Table-------------------------------------------------------------
function AC_History(uid,deviceId,rows){
	var detail = build_Detail_page();
	var table_div = document.getElementById("table_div");
			table_div.appendChild(detail);
	var HistoryList = queryByRows(uid+"."+deviceId,rows);
	var H_accelTH = ["紀錄ID","加速度","時間"];
	var accelID = ["H_id_", "H_acceleration_","H_time_"];
	var H_accelId = genDataID_ukey(HistoryList,accelID);
	var accelTable = createTable("H_accel_table","table table-striped- table-bordered table-hover table-checkable",H_accelTH,H_accelId);
	var detail_table = document.getElementById("detail_page_table");
			detail_table.appendChild(accelTable);
			getHistory_AC(HistoryList);
	  //return accelTable;

}
function IC_History(uid,deviceId,rows){
	var detail = build_Detail_page();
	var table_div = document.getElementById("table_div");
			table_div.appendChild(detail);
	var HistoryList = queryByRows(uid+"."+deviceId,rows);
	var H_incliTH = ["紀錄ID","傾斜度","時間"];
	var incliID = ["H_id_", "H_inclinometer_","H_time_"];
	var H_incliId = genDataID_ukey(HistoryList,accelID);
	var incliTable = createTable("H_inclin_table","table table-striped- table-bordered table-hover table-checkable",H_incliTH,H_incliId);
	var detail_table = document.getElementById("detail_page_table");
			detail_table.appendChild(incliTable);
			getHistory_IC(HistoryList);
	  //return accelTable;

}
//-----------------------------------------------------------------------------------------------------------
function build_Detail_page(){
var detail_string = _User.http_get_unauth('./template/detail_page.js');
		detail_string = detail_string.trim();
var detail_page = document.createElement('DIV');
		detail_page.className = "info_content";
		detail_page.id = "detail_page";
		detail_page.innerHTML = detail_string;
		return detail_page;
	}
//----------------------earthQuake-History-Table-------------------------------------------------------------
function EQ_HistoryTable(uid,deviceId,rows){
	            var detail_string = _User.http_get_unauth('./template/detail_page.js');
							    detail_string = detail_string.trim();
							var detail_page = document.createElement('DIV');
							    detail_page.className = "info_content";
									detail_page.id = "detail_page";
									detail_page.innerHTML = detail_string;
							/*var detail_page = new DOMParser().parseFromString(detail_string, "text/xml");
							    detail_page = detail_page.childNodes[0];*/
	            var table_div = document.getElementById("table_div");
	            var HistoryList = queryByRows(uid+"."+deviceId,rows);
				      var earthTH_H = ["紀錄ID","三軸加速度","及時震度","X軸加速度","Y軸加速度","Z軸加速度","時間"];
							var earthId_H = genEarthID_H(HistoryList);
							var earthTable_H = createTable("EQ_History_"+deviceId,"table table-striped- table-bordered table-hover table-checkable",earthTH_H,earthId_H);
							//table_div.innerHTML += detail_string;
							table_div.appendChild(detail_page);
							var detail_table = document.getElementById("detail_page_table");
							    detail_table.appendChild(earthTable_H);
							    //table_div.appendChild(earthTable_H);
							    getHistory_EQ(HistoryList);

}
//----------------------waterStage-History-Table-------------------------------------------------------------
function WS_History(uid,deviceId,rows){
	            var detail_string = _User.http_get_unauth('./template/detail_page.js');
							    detail_string = detail_string.trim();
							var detail_page = document.createElement('DIV');
							    detail_page.className = "info_content";
									detail_page.id = "detail_page";
									detail_page.innerHTML = detail_string;

	            var table_div = document.getElementById("table_div");

	            var HistoryList = queryByRows(uid+"."+deviceId,rows);

				      var H_waterTH = ["紀錄ID","水位高度","時間"];
							var waterlID = ["H_id_", "H_waterStage_","H_time_"];
							var H_waterId = genDataID_ukey(HistoryList,waterlID);

							var H_waterTable = createTable("WS_History_"+deviceId,"table table-striped- table-bordered table-hover table-checkable",H_waterTH,H_waterId);

							table_div.appendChild(detail_page);
							var detail_table = document.getElementById("detail_page_table");
							    detail_table.appendChild(H_waterTable);
							    getHistory(HistoryList);

}
//=================================================================================================================
//const dateTime = Date.now();
//const timestamp = Math.floor(dateTime / 1000);
//const timestamp = new Date('2019-01-21').getTime();
function DateToUnix_timestamp(datetime){
	var parts = datetime.trim().split(' ');
  var date = parts[0].split('-');
	var time = (parts[1] ? parts[1] : '00:00:00').split(':');
	// NOTE:: Month: 0 = January - 11 = December.
	var timestamp = new Date(date[0],date[1]-1,date[2],time[0],time[1],time[2]);
	    return timestamp.getTime();
}
function Unix_timestampToDate(timestamp){
var t = new Date(timestamp);
var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
var year = t.getFullYear();
//var month = months[a.getMonth()];
var month = t.getMonth()+1;
var date = t.getDate();
var hour = t.getHours();
var min = t.getMinutes();
var sec = t.getSeconds();
var time = year +'-'+ month + '-'+date + ' '+ hour + ':' + min + ':' + sec ;
    return time;
}
			function Init()
		{
			 initialNav();
			//_initialTable();
			document.getElementById("LogOutButton").onclick=  function() {LogOut()};
			document.getElementById("StationTable").onclick=  function() {showStationTable()};
			document.getElementById("SensorTable").onclick=  function() {showWaterTable()};
			/*document.getElementById("dikeTable").onclick=  function() { showTable()};
			document.getElementById("waterGateTable").onclick=  function() { showTable()};
			document.getElementById("pumpingTable").onclick=  function() { showTable()};
			document.getElementById("rainTable").onclick=  function() { showTable()};
			document.getElementById("riskTable").onclick=  function() { showTable()};
			document.getElementById("warningTable").onclick=  function() { showTable()};*/

				//var device_list = queryByRows("River-Management.Device-Basic",2);
				/*for (let o of device_list) {
					console.log(o);
					console.log(Object.keys(o));
				}
				for (var i in device_list) {
					console.log(device_list[i]);
				}*/
				//console.log(device_list);
				//_accelerationTable();
				var dataList = queryByRows("G-300-0-0.Gateway000001",1);
				var deviceList = dataList[0].sensor_array;
				console.log(dataList);
				console.log(deviceList);

				//var deviceList = queryDevices("G-300-0-0","Gateway000001");
				//console.log(deviceList);

//console.log(document.getElementById("table_div"));

var timestamp_1 = Date.now();
    console.log(timestamp_1);
var datetime = Unix_timestampToDate(timestamp_1);
    console.log(datetime);
var timestamp_2 = DateToUnix_timestamp(datetime);
    console.log(timestamp_2);


				buildMap();
				var G = GetAllGateway();
				console.log(G);






		}
