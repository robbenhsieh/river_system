﻿
var data_gateway = null;
var data_sensor = null;

var current_checklist = null;
var current_gateway = {};
var current_sensor = {};

var em_check_list = [];

var last_value_event = {};
var last_status_event = {};
var last_gw_status_event = {};

var water_pressure_report_list = [];

var day_warning = {};

var day_data_count = 0; 
var day_data_array = [];
var day_time_array = [];

var sensor_report_data = null;

//----- 
function Main_Init()
{
	try
	{
		if(WasLogin() == false) return;
		
		document.getElementById("LogOutButton").onclick =  function(){ OnLogOutClick(); };
		document.getElementById("StationTable").onclick =  function(){ data_gateway = null; ShowStationTable() };
		document.getElementById("SensorTable").onclick  =  function(){ data_sensor = null; ShowSensorTable() };
		
		//document.getElementById("RainChart").onclick  =  function(){ ShowRainChart() };
		
		document.getElementById("Dike").onclick  =  function(){ ShowDike(); ShowCheckListTable();};
		document.getElementById("WaterGate").onclick  =  function(){ ShowWaterGate() };
		document.getElementById("PumpStation").onclick  =  function(){ ShowPumpStation() };
		document.getElementById("RiskMap").onclick  =  function(){ ShowRiskMap() };

		document.getElementById("WarningList").onclick  =  function(){ ShowWarningListTable() };		

		document.getElementById("btn_ShowUserInfo").onclick  =  function(){ ShowUserInfo() };
		document.getElementById("btn_UpdateUserInfo").onclick  =  function(){ UpdateUserInfo() };

		document.getElementById("info_CurUser").innerHTML = _User.GetCurrentUserName();
		if(_User.GetCurrentUserGroup() != "administrator")
			document.getElementById("btn_GotoManagerTool").style.display="none";

		setInterval(WasLogin, 60000);

		SubscribeData();

		//SubscribeEvent();

		LoadDayWarningList();

		setInterval(LoadDayWarningList, 10 * 60 * 1000); 

		//-------------------------------------

		var fullscreen_ctl = document.getElementById("fullscreen_ctl");
		fullscreen_ctl.addEventListener('click', function(e) {
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();
			setTimeout(function(){
				$($.fn.dataTable.tables(true)).DataTable().columns.adjust();
			}, 1);
		}, false);

		//document.getElementById("DrawLine").onclick  =  function(){ DrawTools(TGOS.TGOverlayType.LINESTRING) };
		//document.getElementById("DrawPolygon").onclick  =  function(){ DrawTools(TGOS.TGOverlayType.POLYGON) };

	}
	catch(e){console.log(e);}
	return null;
}
//----- 
function WasLogin()
{
	try
	{
		if(_User.WasLogin() == null)
		{
			DoLogOut();
			return false;
		}
			
		return true;
	}
	catch(e){console.log(e);}
	return false;
}
//-----
function OnLogOutClick()
{
	try
	{
		if(confirm('是否要登出本系統?'))
			DoLogOut();
	}
	catch(e){console.log(e);}
}
//-----
function DoLogOut()
{
	try
	{
		_User.DoLogout();
		
		window.location = '/scripts/river_system/dashboard/login.html';
	}
	catch(e){console.log(e);}
}
//-----
function UpdateUserInfo()
{
	try
	{
		var _data=
		{
			name: document.getElementById("tb_UserName").value,
			email: document.getElementById("tb_UserEMail").value,
			phone1: document.getElementById("tb_UserPhone").value,
			note: document.getElementById("tb_UserNote").value,
			//sex: document.getElementById("cb_Sex_Male").checked ? 1 : 0,
		}

		/*var new_password = document.getElementById("reset_password").value;
		if(new_password != null && new_password.length > 0){
			_data.password = new_password;
		}*/
		
		if(_User.UpdateUser(null, _data) == true)
			alert("更新成功");
		else
			alert("更新失敗");
	}
	catch(e){console.log(e);}
}
//-----
function ShowUserInfo()
{
	try
	{
		var _data = _User.GetUserInfo();
		
		_data = JSON.parse(_data);

		document.getElementById("tb_UserID").value = _data.id;
		document.getElementById("tb_Password").value = _data.password;
		document.getElementById("tb_UserName").value = _data.name;
		document.getElementById("tb_UserEMail").value = _data.email;	
		document.getElementById("tb_UserPhone").value = _data.phone1;
		
		//document.getElementById("cb_Sex_Male").checked = _data.sex == 1 ? true : false;
		//document.getElementById("cb_Sex_Female").checked = _data.sex == 1 ? false : true;
		
		document.getElementById("tb_UserRegTime").value = _data.reg_time;
		document.getElementById("tb_UserLastLoginTime").value = _data.last_login_time;	
		document.getElementById("tb_UserNote").value = _data.note;

		document.getElementById("ac_gateway_type_26").checked = false;
		document.getElementById("ac_sensor_type_43").checked = false;
		document.getElementById("ac_sensor_type_44").checked = false;
		document.getElementById("ac_sensor_type_45").checked = false;
		document.getElementById("ac_sensor_type_46").checked = false;
		document.getElementById("maintain").checked = false;

		if(_data.alert_msg != null && _data.alert_msg.length > 0)
		{	
			var alert_msg = _data.alert_msg;

			for(var i = 0; i < alert_msg.length; i++)
			{
				var tmp = alert_msg[i];
				if(tmp.indexOf(GatewayType) != -1)        document.getElementById("ac_gateway_type_26").checked = true;
				else if(tmp.indexOf(WaterPressure) != -1) document.getElementById("ac_sensor_type_43").checked = true;
				else if(tmp.indexOf(Acceleration) != -1)  document.getElementById("ac_sensor_type_44").checked = true;
				else if(tmp.indexOf(Inclinometer) != -1)  document.getElementById("ac_sensor_type_45").checked = true;
				else if(tmp.indexOf(Earthquake) != -1)    document.getElementById("ac_sensor_type_46").checked = true;
				else if(tmp.indexOf("maintain") != -1)    document.getElementById("maintain").checked = true;
			}
		}

		//-------------------------------------------------------------

		document.getElementById("old_password").value = "";
		document.getElementById("new_password").value = "";
		document.getElementById("confirm_new_password").value = "";

		document.getElementById("confirm_new_password").oninput = function(){ NewPasswordCheck(); };
		document.getElementById("confirm_new_password").onchange = function(){ NewPasswordCheck(); };

		document.getElementById("confirm_psw_fb").style.display = "none";
	}
	catch(e){console.log(e);}
}
//-----
function InitEditPassword()
{
	try
	{
		var old_password = document.getElementById("old_password").value = "";
		var new_password = document.getElementById("new_password").value = "";
		var confirm_new_password = document.getElementById("confirm_new_password").value = "";
	}
	catch(e){console.log(e);}
}
//-----
function ConfirmResetPassword()
{
	try
	{
		var _data = _User.GetUserInfo();
		_data = JSON.parse(_data);

		var now_password = _data.password;
		var old_password = document.getElementById("old_password").value;
		var new_password = document.getElementById("new_password").value;
		var confirm_new_password = document.getElementById("confirm_new_password").value;

		if(old_password.length > 0 && now_password != old_password)
		{
			alert("原密碼錯誤");
			return;
		}

		if(new_password.length == 0 || confirm_new_password.length == 0)
		{
			alert("請輸入新密碼");
			return;
		}

		if(new_password != confirm_new_password)
		{
			document.getElementById("confirm_psw_fb").style.display = "";
			return;
		}

		//------------------------------------------------

		if(new_password != null && new_password.length > 0)
		{
			_data.password = new_password;

			if(_User.UpdateUser(null, _data) == true){	
				document.getElementById("tb_Password").value = new_password;
				alert("密碼更新成功");
			}
			else
				alert("密碼更新失敗");
		}

		document.getElementById("m_modal_edit_psw_close").click();
	}
	catch(e){console.log(e);}
}
//-----
function NewPasswordCheck()
{
	try
	{
		document.getElementById("confirm_psw_fb").style.display = "none";
	}
	catch(e){console.log(e);}
}
//-----
function ShowPumpStation()
{
	try
	{	
		//var _data = _User.getLastData('opendata.pumpstation', 300, null);
		var _data = Api_GetCloudLastData("opendata", "pumpstation", 3000);
		var _rivers = [];
		var _locations = [];
		var _riverOptions = '<option>全部</option>';
		var _locationOptions = '<option>全部</option>';
		  
		_data = JSON.parse(_data); 
		_data = _data.results;
		  
		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i];		
			
			if(_locations.indexOf(_item.owner) < 0)
			{
				_locations.push(_item.owner);
				_locationOptions += '<option>'+_item.owner+'</option>';
			}
		}
				
		document.getElementById("m_data").innerHTML = _User.http_get_unauth('./template/watergate_table.js');
		//document.getElementById("btn_ExecFilter").onclick = function(){ DoPumpStationFilter(); };		
		document.getElementById("m_portlet_index").style.display = "block";		
		document.getElementById("cb_RiverList").innerHTML = _riverOptions;
		document.getElementById("cb_FilterList").innerHTML = _locationOptions;
		document.getElementById("cb_RiverList").onchange = function(){ DoPumpStationFilter(); };
		document.getElementById("cb_FilterList").onchange = function(){ DoPumpStationFilter(); };
		document.getElementById("txt_FilterName").innerHTML = "單位";		
		document.getElementById("table_title").innerHTML = "抽水站查詢";
		
		ListPumpStation(_data, null, null);

	}
	catch(e){console.log(e);}
}
//-----
function ListPumpStation(_data, _riverFilter, _locFilter)
{
	try
	{
		DestroyTable("m_table_1");

		var _out_data = '';
		_out_data += "編號,名稱,單位,地址,電話,WGS84_X,WGS84_Y\r\n";

		var _table_list_title = "<tr><th>編號</th><th>名稱</th><th>單位</th><th>地址</th><th>電話</th><th>座標</th></tr>";
		var _html = "";

		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i];
			var _p = TGOS.WGS84toTWD97(_item.lng, _item.lat);  
			
			if(_riverFilter != null && _riverFilter != "全部" && _item.river != _riverFilter) continue;
			if(_locFilter != null && _locFilter != "全部" && _item.owner != _locFilter) continue;
			
			var _desc = '';
			  
			_desc += '<tr>';
			_desc += '<td>'+_item.ukey+'</td>';
			_desc += '<td>'+_item.name+'</td>';
			_desc += '<td>'+_item.owner+'</td>';
			_desc += '<td>'+_item.address+'</td>';
			_desc += '<td>'+_item.phone+'</td>';
			_desc += '<td>'+_p.x+','+_p.y+'</td>';  
			_desc += '</tr>';
			
			_html += _desc;
			_out_data += _item.ukey+','+_item.name+','+_item.owner+','+_item.address+','+_item.phone+','+_item.lng+','+_item.lat+'\r\n';
		}
		
		document.getElementById("table_list_title").innerHTML = _table_list_title;
		document.getElementById("table_list").innerHTML = _html;

		document.getElementById("btn_ExecOutport").onclick = function()
		{
			var _download = document.getElementById("btn_ExecOutport");		
			var _file_ext = document.getElementById("radioItem_CSV").checked ? ".csv" : ".txt";
			var _blob = new Blob(["\uFEFF" + _out_data], {type: "text/plain;charset=utf-8"});
			var _file_name = "pumpstation"+_file_ext;

			if(window.navigator && window.navigator.msSaveOrOpenBlob) 
			{
				window.navigator.msSaveOrOpenBlob(_blob, _file_name);
			}
			else
			{
				_download.href = URL.createObjectURL(_blob);
				_download.download = _file_name;
			}
			
			$("#m_modal_download").modal('toggle');
		};
		
		InitTable("m_table_1");
	}
	catch(e){console.log(e);}
}
//-----
function DoPumpStationFilter()
{
	try
	{
		var _riverFilter = document.getElementById("cb_RiverList").value;
		var _locFilter = document.getElementById("cb_FilterList").value;
		//var _data=_User.getLastData('opendata.pumpstation', 300, null);
		var _data = Api_GetCloudLastData("opendata", "pumpstation", 3000);
		  
		_data = JSON.parse(_data); 
		_data = _data.results;
		
		ListPumpStation(_data, _riverFilter, _locFilter);
	}
	catch(e){console.log(e);}
}
//-----
function ShowRiskMap()
{
	try
	{	
		//var _data = _User.getLastData('opendata.pumpstation', 300, null);
		var _data = Api_GetCloudLastData("opendata", "riskmap", 3000);

		var _risks = [];
		var _risksOptions = '<option>全部</option>';
		  
		_data = JSON.parse(_data); 
		_data = _data.results;
		  
		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i];		
			
			if(_risks.indexOf(_item.risk) < 0)
			{
				_risks.push(_item.risk);
				_risksOptions += '<option>'+_item.risk+'</option>';
			}
		}

		document.getElementById("m_data").innerHTML = _User.http_get_unauth('./template/riskmap_table.js');
		//document.getElementById("btn_ExecFilter").onclick = function(){ DoRiskmapFilter(); };		
		document.getElementById("m_portlet_index").style.display = "block";
		document.getElementById("cb_RiskList").innerHTML = _risksOptions;
		document.getElementById("cb_RiskList").onchange = function(){ DoRiskmapFilter(); };
		document.getElementById("table_title").innerHTML = "風險地圖查詢";
		
		ListShowRiskMap(_data, null);
	}
	catch(e){console.log(e);}
}
//-----
function ListShowRiskMap(_data, _riskFilter)
{
	try
	{	
		DestroyTable("m_table_1");

		var _out_data = '';
		_out_data += "編號,名稱,類型,風險,長度(公尺)\r\n";

		var _table_list_title = "<tr><th>編號</th><th>名稱</th><th>類型</th><th>風險</th><th>長度(公尺)</th></tr>";
		var _html = "";

		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i];
			var _desc = '';
			var _p = TGOS.WGS84toTWD97(_item.lng, _item.lat);  
			
			if(_riskFilter != null && _riskFilter != "全部" && _item.risk != _riskFilter) continue;
			  
			_item.name=_item.name.replace(/\n/g, " ");

			_desc += '<tr>';
			_desc += '<td>'+_item.ukey+'</td>';
			_desc += '<td>'+_item.name+'</td>';
			_desc += '<td>'+_item.type+'</td>';
			_desc += '<td>'+_item.risk+'</td>';
			_desc += '<td>'+_item.len+'</td>';
			_desc += '</tr>';

			_html += _desc;
			_out_data += _item.ukey+','+_item.name+','+_item.type+','+_item.risk+','+_item.len+'\r\n';
		}

		document.getElementById("table_list_title").innerHTML = _table_list_title;
		document.getElementById("table_list").innerHTML = _html;
		
		//document.getElementById("table_list").innerHTML = _html;		
		document.getElementById("btn_ExecOutport").onclick = function()
		{
			var _download = document.getElementById("btn_ExecOutport");		
			var _file_ext = document.getElementById("radioItem_CSV").checked ? ".csv" : ".txt";
			var _blob = new Blob(["\uFEFF" + _out_data], {type: "text/plain;charset=utf-8"});
			var _file_name = "RiskMap"+_file_ext;

			if(window.navigator && window.navigator.msSaveOrOpenBlob) 
			{
				window.navigator.msSaveOrOpenBlob(_blob, _file_name);
			}
			else
			{
				_download.href = URL.createObjectURL(_blob);
				_download.download = _file_name;
			}
			
			$("#m_modal_download").modal('toggle');
		};

		InitTable("m_table_1");
	}
	catch(e){console.log(e);}
}
//-----
function DoRiskmapFilter()
{
	try
	{
		var _riverFilter = document.getElementById("cb_RiskList").value;
		var _data=Api_GetCloudLastData("opendata", "riskmap", 3000);
		  
		_data = JSON.parse(_data); 
		_data = _data.results;
		
		ListShowRiskMap(_data, _riverFilter);
	}
	catch(e){console.log(e);}
}
//-----
function ShowDike()
{
	try
	{	
		var _data = Api_GetCloudLastData("opendata", "dike-taipei", 3000);
		_data = JSON.parse(_data); 
		_data = _data.results;

		var count = 0;
		for(var i = 0; i <　_data.length; i++)
		{
			var _item = _data[i];
			_item.ukey = i;
			_item.area = "台北市";

			count = i;
		}

		count = count + 1;

		var _data2 = Api_GetCloudLastData("opendata", "dike-taiwan", 3000);
		_data2 = JSON.parse(_data2); 
		_data2 = _data2.results;

		for(var i = 0; i <_data2.length; i++)
		{
			var _item = _data2[i];
			_item.ukey = count + i;
			_item.area = "跨縣市";
		}

		var _Options = '<option>全部</option>';
		_Options += '<option>台北市</option>';
		_Options += '<option>跨縣市</option>';

		_data = _data.concat(_data2);

		document.getElementById("m_data").innerHTML = _User.http_get_unauth('./template/dike_table.js');
		//document.getElementById("btn_ExecFilter").onclick = function(){ DoDikeFilter(); };		
		document.getElementById("m_portlet_index").style.display = "block";		
		document.getElementById("cb_RiverList").innerHTML = _Options;
		document.getElementById("cb_RiverList").onchange = function(){ DoDikeFilter(); };
		document.getElementById("table_title").innerHTML = "堤防查詢";
		
		ListDike(_data, null);
	}
	catch(e){console.log(e);}
}
//-----
function ListDike(_data, _Filter)
{
	try
	{
		DestroyTable("m_table_1");

		var _out_data = '';
		_out_data += "編號,名稱,河川別,長度(公尺)\r\n";

		var _table_list_title = "<tr><th>編號</th><th>名稱</th><th>河川</th><th>長度(公尺)</th></tr>";
		var _html = "";

		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i];
			var _desc = '';
			var _p = TGOS.WGS84toTWD97(_item.lng, _item.lat);  
			
			if(_Filter != null && _Filter != "全部" && _item.area != _Filter) continue;
			  
			_desc += '<tr>';
			_desc += '<td>'+_item.ukey+'</td>';
			_desc += '<td>'+_item.name+'</td>';
			_desc += '<td>'+_item.river+'</td>';
			_desc += '<td>'+_item.len+'</td>';
			_desc += '</tr>';
			
			_html += _desc;
			_out_data += _item.ukey+','+_item.name+','+_item.river+','+_item.len+'\r\n';
		}

		document.getElementById("table_list_title").innerHTML = _table_list_title;
		document.getElementById("table_list").innerHTML = _html;
		
		//document.getElementById("table_list").innerHTML = _html;		
		document.getElementById("btn_ExecOutport").onclick = function()
		{
			var _download = document.getElementById("btn_ExecOutport");		
			var _file_ext = document.getElementById("radioItem_CSV").checked ? ".csv" : ".txt";
			var _blob = new Blob(["\uFEFF" + _out_data], {type: "text/plain;charset=utf-8"});
			var _file_name = "dike"+_file_ext;

			if(window.navigator && window.navigator.msSaveOrOpenBlob) 
			{
				window.navigator.msSaveOrOpenBlob(_blob, _file_name);
			}
			else
			{
				_download.href = URL.createObjectURL(_blob);
				_download.download = _file_name;
			}
			
			$("#m_modal_download").modal('toggle');
		};

		InitTable("m_table_1");
	}
	catch(e){console.log(e);}
}
//-----
function DoDikeFilter()
{
	try
	{
		var _Filter = document.getElementById("cb_RiverList").value;
		//var _data=_User.getLastData('opendata.dike', 300, null);
		var _data = Api_GetCloudLastData("opendata", "dike-taipei", 3000);
		_data = JSON.parse(_data); 
		_data = _data.results;

		var count = 0;
		for(var i = 0; i <　_data.length; i++)
		{
			var _item = _data[i];
			_item.ukey = i;
			_item.area = "台北市";

			count = i;
		}

		count = count + 1;

		var _data2 = Api_GetCloudLastData("opendata", "dike-taiwan", 3000);
		_data2 = JSON.parse(_data2); 
		_data2 = _data2.results;

		for(var i = 0; i <_data2.length; i++)
		{
			var _item = _data2[i];
			_item.ukey = count + i;
			_item.area = "跨縣市";
		}
		
		_data = _data.concat(_data2);
		ListDike(_data, _Filter);
	}
	catch(e){console.log(e);}
}
//-----
function ShowWaterGate()
{
	try
	{	
		//var _data=_User.getLastData('opendata.watergate', 3000, null);
		var _data=Api_GetCloudLastData("opendata", "watergate", 3000);
		var _rivers = [];
		var _locations = [];
		var _riverOptions = '<option>全部</option>';
		var _locationOptions = '<option>全部</option>';
		  
		_data = JSON.parse(_data); 
		_data = _data.results;
		  
		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i];			  
			//if(_item.river == '淡水河' || _item.river == '二重疏洪道' || _item.river == '大漢溪' || _item.river == '景美溪')
			{
				if(_rivers.indexOf(_item.river) < 0)
				{
					_rivers.push(_item.river);
					_riverOptions += '<option>'+_item.river+'</option>';
				}
				
				if(_locations.indexOf(_item.town) < 0)
				{
					_locations.push(_item.town);
					_locationOptions += '<option>'+_item.town+'</option>';
				}
			}
		}
				
		document.getElementById("m_data").innerHTML = _User.http_get_unauth('./template/watergate_table.js');
		//document.getElementById("btn_ExecFilter").onclick = function(){ DoWaterGateFilter(); };		
		document.getElementById("m_portlet_index").style.display = "block";		
		document.getElementById("cb_RiverList").innerHTML = _riverOptions;
		document.getElementById("cb_FilterList").innerHTML = _locationOptions;
		document.getElementById("cb_RiverList").onclick = function(){ DoWaterGateFilter(); };	
		document.getElementById("cb_FilterList").onclick = function(){ DoWaterGateFilter(); };
		document.getElementById("txt_FilterName").innerHTML = "區域";		
		document.getElementById("table_title").innerHTML = "水門站查詢";
		
		ListWaterGate(_data, null, null);
	}
	catch(e){console.log(e);}
}
//-----
function ListWaterGate(_data, _riverFilter, _locFilter)
{
	try
	{
		DestroyTable("m_table_1");

		var _table_list_title = "<tr><tr><th>編號</th><th>名稱</th><th>河川別</th><th>城市</th><th>區域</th><th>類型</th><th>座標</th></tr>";
		var _html = "";

		var _out_data = '';
		_out_data += "編號,名稱,河川別,城市,區域,類型,WGS84_X,WGS84_Y\r\n";
		
		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i];
			var _p = TGOS.WGS84toTWD97(_item.lng, _item.lat);
			
			if(_riverFilter != null && _riverFilter != "全部" && _item.river != _riverFilter) continue;
			if(_locFilter != null && _locFilter != "全部" && _item.town != _locFilter) continue;
			
			//if(_item.river == '淡水河' || _item.river == '二重疏洪道' || _item.river == '大漢溪' || _item.river == '景美溪')
			{
				var _desc = '';
				  
				_desc += '<tr>';
				_desc += '<td>'+_item.ukey+'</td>';
				_desc += '<td>'+_item.name+'</td>';
				_desc += '<td>'+_item.river+'</td>';
				_desc += '<td>'+_item.city+'</td>';
				_desc += '<td>'+_item.town+'</td>';
				_desc += '<td>'+_item.type+'</td>';
				_desc += '<td>'+_p.x+','+_p.y+'</td>';  
				_desc += '</tr>';
				
				_html += _desc;
				_out_data += _item.ukey+','+_item.name+','+_item.river+','+_item.city+','+_item.town+','+_item.type+','+_item.lng+','+_item.lat+'\r\n';
			}
		}
			
		document.getElementById("table_list_title").innerHTML = _table_list_title;
		document.getElementById("table_list").innerHTML = _html;

		document.getElementById("btn_ExecOutport").onclick = function()
		{
			var _download = document.getElementById("btn_ExecOutport");		
			var _file_ext = document.getElementById("radioItem_CSV").checked ? ".csv" : ".txt";
			var _blob = new Blob(["\uFEFF" + _out_data], {type: "text/plain;charset=utf-8"});
			var _file_name = "watergate"+_file_ext;

			if(window.navigator && window.navigator.msSaveOrOpenBlob) 
			{
				window.navigator.msSaveOrOpenBlob(_blob, _file_name);
			}
			else
			{
				_download.href = URL.createObjectURL(_blob);
				_download.download = _file_name;
			}
			
			$("#m_modal_download").modal('toggle');
		};

		InitTable("m_table_1");
	}
	catch(e){console.log(e);}
}
//-----
function DoWaterGateFilter()
{
	try
	{
		var _riverFilter = document.getElementById("cb_RiverList").value;
		var _locFilter = document.getElementById("cb_FilterList").value;
		//var _data=_User.getLastData('opendata.watergate', 3000, null);
		var _data=Api_GetCloudLastData("opendata", "watergate", 3000);

		  
		_data = JSON.parse(_data); 
		_data = _data.results;
		
		ListWaterGate(_data, _riverFilter, _locFilter);
	}
	catch(e){console.log(e);}
}
//-----
function ShowRainChart()
{
	try
	{
		document.getElementById("m_portlet_index").style.display = "block";			
		document.getElementById("m_data").innerHTML = '<div class="sensor_chart" id="sensor_chart" style="float: bottom; margin-top: 5px; height: 350px;"></div>';
		
		var _title = document.getElementById("table_title");
		//var _data=_User.getLastData('opendata.twrain', 1, null);
		var _data=Api_GetCloudLastData("opendata", "twrain", 1);		
		
		if(_data != null)
		{
			_root=JSON.parse(_data); 

			var _total_rain_cur=0;
			var _total_rain_24hr=0;
			var _labels=[];
							
			if(_root != null)
			{
				var _packet = _root.results[0].packet;	
				
				for (var i = 0; i < _packet.length; i++)
				{
					var _items = _packet[i]; 
					if(_items.city == '臺北市' || _items.city == '新北市')
					{
						var _location = _items.city+'/'+_items.town;
						var _index = _labels.indexOf(_location);
						if(_index < 0) _labels.push(_location);
					}
				}
				
				var _value1=new Array(_labels.length);
				var _value2=new Array(_labels.length);
				var _tick=new Array(_labels.length);
				
				for (var i = 0; i < _labels.length; i++)
				{
					_value1[i]=0;
					_value2[i]=0;
					_tick[i]=0;
				}
				
				_labels.sort();
				for (var i = 0; i < _labels.length; i++)
					_labels[i] = _labels[i].replace("臺北市/", "").replace("新北市/", "");
				
				for (var i = 0; i < _packet.length; i++)
				{
					var _items = _packet[i]; 
					var _location = _items.town;
					var _index = _labels.indexOf(_location);
					
					if(_index >= 0)
					{                                    
					   if(_items.value != null && _items.value > 0) { _value1[_index]+=_items.value; }
					   if(_items.value_24hr != null && _items.value_24hr > 0){ _value2[_index]+=_items.value_24hr; }
										   
					   _tick[_index]++;
					}		        
				}
			  
				var _result1=[];
				var _result2=[];
				
				for (var i = 0; i < _labels.length; i++)
				{
					if(_tick[i] > 0)
					{
						var _avg_value1=Math.floor(_value1[i]/_tick[i]*100)/100;
						var _avg_value2=Math.floor(_value2[i]/_tick[i]*100)/100;

						_result1.push({x: _labels[i], y: _avg_value1});
						_result2.push({x: _labels[i], y: _avg_value2});

						_total_rain_cur+=_avg_value1;
						_total_rain_24hr+=_avg_value2;
					}
					else
					{
						_result1.push({x: _labels[i], y: 0});
						_result2.push({x: _labels[i], y: 0});
					}
				}
			
				_tw_country=_labels;                        
				_title.innerHTML = "大台北地區雨量 ► 即時: "+Math.floor(_total_rain_cur)+'mm ｜ 24小時: '+ Math.floor(_total_rain_24hr)+'mm';
				_data =	[
							{
								key: "即時雨量",
								values: _result1
							},
							{
								key: "24小時雨量",
								values: _result2
							}
						];
			
				nv.addGraph(function()
				{
					var _chart = nv.models.multiBarChart();
					var _graphObj = d3.select('#sensor_chart');    	
					
					_chart.barColor(d3.scale.category20().range());
					_chart.margin({bottom: 20, left: 40});
					_chart.rotateLabels(0);
					_chart.groupSpacing(0.5);
					_chart.duration(0);
					_chart.showControls(false);

					_chart.xAxis.axisLabelDistance(30).showMaxMin(false);
					_chart.xAxis.showMaxMin(false);

					_graphObj.selectAll("*").remove();
					_graphObj.append('svg')
							 .attr('vertical-align', 'top')
							 .attr('width', '100%')
							 .attr('height', '90%')
							 .attr("font-size", "8px")
							 .datum(_data)
							 .call(_chart);
					
					nv.utils.windowResize(_chart.update);
					return _chart;
				});
			}
		}
	}
	catch(e){console.log(e);}	
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ShowCheckListTable()
{
	try
	{
		document.getElementById("m_portlet_index").style.display = "block";

		InitCheckListTable();
		RefreshCheckList();
	}
	catch(e){console.log(e);}
}
//-----
function InitCheckListTable()
{
  try
  {
	var table_list = document.getElementById("index_emcl_table_list");
	table_list.innerHTML = "";

	var river_select = document.getElementById("index_emcl_river_select");
	var embankment_select = document.getElementById("index_emcl_embankment_select");

	river_select.innerHTML = "";
	embankment_select.innerHTML = "";

	var river_options = "<option value='-1'>全部</option>";
	var embankment_options = "<option value='-1'>全部</option>";

	for(var river in _RiverEmbankmentList){
		river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
	}

	river_select.innerHTML = river_options;
	embankment_select.innerHTML = embankment_options;

	//------------------------------------------------------

	river_select.options[0].selected = true;

	OnRiverSelectChange(river_select);

	//------------------------------------------------------

	var arrows = {
		leftArrow: '<i class="la la-angle-left"></i>',
		rightArrow: '<i class="la la-angle-right"></i>'
	}

	$('#m_datepicker_1, #m_datepicker_2').datepicker({
		language: 'zh-TW',
		todayHighlight: true,
		orientation: "bottom left",
		templates: arrows,
		format: 'yyyy/mm/dd'
	});

	$('#m_datepicker_1').datepicker('update', moment(new Date()).subtract(7,'d').format('YYYY-MM-DD'));
	$('#m_datepicker_2').datepicker('update', new Date());

	var m_datepicker_1 = document.getElementById("m_datepicker_1");
	var m_datepicker_2 = document.getElementById("m_datepicker_2");

	document.getElementById("m_datepicker_1").onchange = function(){ CheckDateTimeRange1(); };
	document.getElementById("m_datepicker_2").onchange = function(){ CheckDateTimeRange2(); };

	m_datepicker_1.time_range_flag = m_datepicker_2.time_range_flag = false;

  }
  catch(e){console.log(e);}
}
//-----
function RefreshCheckList()
{
  try
  {
	var uuid = "opendata";
	var suid = "checklist";
	var sh_uuid = uuid + "." + suid;

	var s_date = $("#m_datepicker_1").val();
	var e_date = $("#m_datepicker_2").val();
	var s_time = "00:00:00.000";
	var e_time = "23:59:59.999";
	var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
	var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

	var sh_start = start - (30 * 86400 * 1000);
	var sh_end = end + (7 * 86400 * 1000);

	var river = document.getElementById("index_emcl_river_select").value;
	var embankment = document.getElementById("index_emcl_embankment_select").value;
	var security = document.getElementById("index_emcl_security_select").value;
	
	//-------------------------------------------------------------------------------------

	em_check_list = [];

	//var resp = _User.getData(sh_uuid, sh_start, sh_end, null);
	var resp = Api_GetCloudDataByTime(uuid, suid, sh_start, sh_end, null);
   
	if(resp != null && resp.length > 0)
	{
		var resp = JSON.parse(resp);
		var results = resp.results;

		for(var i = 0; i < results.length; i++)
		{
			var item = results[i];

			var river_flag      = (river == item.river) || (river == "-1") ? true : false;
			var embankment_flag = (embankment == item.em_id) || (embankment == "-1") ? true : false;
			var time_flag       = (item.time >= start &&  item.time <= end) ? true : false;
			var security_flag   = (security == item.security) || (security == "0") ? true : false;

			if(river_flag && embankment_flag && security_flag && time_flag)
			{
				em_check_list.push(item);
			}
		}
	}

	//-------------------------------------------------------------------------------------

	DestroyTable("index_emcl_table");
		
	var edit_btn = _User.http_get_unauth('./template/edit_btn_checklist1.js');

	var table_list = document.getElementById("index_emcl_table_list");
	table_list.innerHTML = "";

	var edit_btn_list = [];
	for(var i = 0; i < em_check_list.length; i++)
	{
		var item = em_check_list[i];

		var str = "";
		str += "<tr>";
		str += "<td>" + (item.time == null ? "--" : TimestampToDate(item.time, false)) + "</td>";
		str += "<td>" + (item.em_name == null ? "--" : item.em_name) + "</td>";
		str += "<td>" + (item.type == null ? "--" : item.type) + "</td>";
		str += "<td>" + (item.pile == null ? "--" : item.pile) + "</td>";
		str += "<td>" + (item.river == null ? "--" : item.river) +"</td>";
		str += "<td>" + (item.security == null ? "--" : item.security) + "</td>";
		str += "<td>" + (item.description == null ? "--" : item.description) + "</td>";
		str += edit_btn.replace("{edit_btn_id}", "checklist_" + i);
		str += "</tr>";

		table_list.innerHTML += str;
	}

	//-------------------------------------------------------------------------------------

	InitTable("index_emcl_table");
  }
  catch(e){console.log(e);}
}
//-------------
function InitModalCheckList01()
{
	try
	{
		current_checklist = null;

		var modal_setup_template = _User.http_get_unauth('./template/modal_checklist_01.js');
		var modal_setup = document.getElementById("modal_checklist_01");

		modal_setup.innerHTML = "";
		modal_setup.innerHTML = modal_setup_template;

	}
	catch(e){console.log(e);}
}
//-------------
function ShowModalCheckList01(btn_info)
{
	try
	{
		InitModalCheckList01();

		//------------------------------------------

		var id = btn_info.id;
		var array = id.split("_");
		var index = parseInt(array[1]);

		var check_list = em_check_list[index];
		current_checklist = check_list;

		var file_list = current_checklist.file_list;
		var image_list = current_checklist.image_list;

		//------------------------------------------

		var checklist_image_template = _User.http_get_unauth('./template/checklist_image.js');
		var check_list_files = document.getElementById("check_list_files");
		var check_list_images = document.getElementById("check_list_images");

		check_list_files.innerHTML = "";
		check_list_images.innerHTML = "";

		for(var i = 0; i < file_list.length; i++)
		{
			var tmp = file_list[i];
			var fileName = "";

			var extIndex = tmp.name.lastIndexOf(".");
			if (extIndex != -1) { fileName = tmp.name.substr(0, extIndex); }

			var template = checklist_image_template.replace(/{image_name}/g, fileName).replace(/{type}/g, "'file'").replace(/{index}/g, i);
			check_list_files.innerHTML += template;
		}

		for(var i = 0; i < image_list.length; i++)
		{
			var tmp = image_list[i];
			var fileName = "";

			var extIndex = tmp.name.lastIndexOf(".");
			if (extIndex != -1) { fileName = tmp.name.substr(0, extIndex); }

			var template = checklist_image_template.replace(/{image_name}/g, fileName).replace(/{type}/g, "'image'").replace(/{index}/g, i);
			check_list_images.innerHTML += template;
		}

		//------------------------------------------

		document.getElementById("modal_cl_time_text").innerHTML = TimestampToDate(check_list.time, false); 
		document.getElementById("modal_cl_improve_textarea").innerHTML = check_list.improve;
		document.getElementById("modal_cl_inspection_text").innerHTML = check_list.inspection;
		document.getElementById("modal_cl_description_textarea").innerHTML = check_list.description;

		document.getElementById("modal_cl_embankment_X").innerHTML = check_list.coordinate.x;
		document.getElementById("modal_cl_embankment_Y").innerHTML = check_list.coordinate.y;

		document.getElementById("modal_cl_type_text").innerHTML = check_list.type;
		document.getElementById("modal_cl_pile_text").innerHTML = check_list.pile;

		document.getElementById("modal_cl_river_text").innerHTML = check_list.river;
		document.getElementById("modal_cl_admin_text").innerHTML = check_list.admin;
		document.getElementById("modal_cl_security_text").innerHTML = check_list.security;
		document.getElementById("modal_cl_embankment_text").innerHTML = check_list.em_name;

		//---------------------------------------------

	}
	catch(e){console.log(e);}
}
//-----
function ShowModalCheckListImage(type, index)
{
	try
	{
		var list = (type == "file") ? current_checklist.file_list : current_checklist.image_list;
		var image = list[index];

		//-----------------------------------------------------------------------

		var iamge_data = "/scripts/river_system/dashboard/image/Image.jpg";

		var check_list_image = document.getElementById("check_list_image");
		check_list_image.src = "";

		if(image != null) { check_list_image.src = image.path; }
	}
	catch(e){console.log(e);}
}
//-----
function OnRiverSelectChange()
{
	try
	{
		var river_value = document.getElementById("index_emcl_river_select").value;

		var embankment_select = document.getElementById("index_emcl_embankment_select");
		var embankment_options = "<option value='-1'>全部</option>";
		
		if(embankment_select != null)
		{
			embankment_select.innerHTML = "";

			for(var river_id in _RiverEmbankmentList) 
			{
				if(river_value == river_id || river_value == "-1")
				{
					var river_item = _RiverEmbankmentList[river_id];
					var embankment_list = river_item.embankment;

					for(var em_id in embankment_list)
					{
						var embankment = embankment_list[em_id];
						embankment_options += '<option value={index}>{name}</option>'.replace(/{index}/g, embankment.ukey).replace(/{name}/g, embankment.name);
					}
				}
			}

			embankment_select.innerHTML = embankment_options;
			embankment_select.options[0].selected = true;
		}
	}
	catch(e){console.log(e);}
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
function LoadDayWarningList()
{
	try
	{
		var time = new Date().getTime();
		var now_date = TimestampToDate(time, false);
		var start = moment(now_date + " 00:00:00", "YYYY/MM/DD HH:mm:ss").utc().valueOf();
		var end   = moment(now_date + " 23:59:59", "YYYY/MM/DD HH:mm:ss").utc().valueOf();

		day_warning = LoadWarningTableList(start, end, "0", "0", "0");
		
		var disconnect_count = day_warning.count_data["disconnect"];
		//var data_error_count = day_warning.count_data["data_error"];
		var data_warning_count = day_warning.count_data["data_warning"];

		//var day_warning_count = disconnect_count + data_error_count + data_warning_count;
		var day_warning_count = disconnect_count + data_warning_count;

		if(day_warning_count == 0){
			document.getElementById("day_warning_count").style.display = "none";
		}
		else if(day_warning_count > 0){
			document.getElementById("day_warning_count").style.display = "";
			document.getElementById("day_warning_count").innerHTML = day_warning_count;
		}
	}
	catch(e){console.log(e);}
}
//-----
function ShowDayWarningList()
{
	try
	{
		//DestroyTable("day_warning_table");

		var data = WarningTableList(day_warning.warning_list, false);

		document.getElementById("disconnect_count").innerHTML = day_warning.count_data["disconnect"];
		//document.getElementById("data_error_count").innerHTML = day_warning.count_data["data_error"];
		document.getElementById("data_warning_count").innerHTML = day_warning.count_data["data_warning"];

		var table_list = document.getElementById("day_warning_table_list");
		table_list.innerHTML = data.table_list;

		//InitTable("day_warning_table");
	}
	catch(e){console.log(e);}
}
//-----
function ShowWarningListTable()
{
	try
	{
		document.getElementById("m_portlet_index").style.display = "block";

		InitWarningListTable();
		RefreshWarningList();
	}
	catch(e){console.log(e);}
}
//-----
function InitWarningListTable()
{
	try
	{
		var m_data = document.getElementById("m_data");
		m_data.innerHTML = "";

		var warning_table = _User.http_get_unauth('./template/warning_table.js');
		m_data.innerHTML += warning_table;

		//------------------------------------------------

		document.getElementById("table_title").innerHTML = "警示查詢";

		var warning_gateway_select = document.getElementById("warning_gateway_select");
		var warning_sensor_type_select = document.getElementById("warning_sensor_type_select");

		warning_gateway_select.innerHTML = "";
		warning_sensor_type_select.innerHTML = "";
		
		var gw_options = "<option value='0'>全部</option>";
		var sensor_options = "<option value='0'>全部</option>";

		for(var gw in _Gatewaylist) {
			var tmp = _Gatewaylist[gw];
			gw_options += '<option value={index}>{name}</option>'.replace(/{index}/g, tmp.uuid+"_"+tmp.suid).replace(/{name}/g, tmp.name);
		}

		sensor_options += '<option value='+GatewayType+'>監測站</option>';
		sensor_options += '<option value='+WaterPressure+'>水位計</option>';
		sensor_options += '<option value='+Acceleration+'>加速度計</option>';
		sensor_options += '<option value='+Inclinometer+'>傾斜儀</option>';
		sensor_options += '<option value='+Earthquake+'>地震儀</option>';

		warning_gateway_select.innerHTML += gw_options;
		warning_sensor_type_select.innerHTML += sensor_options;

		//-------------------------------------------------
		var arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}

		$('#m_datepicker_1, #m_datepicker_2').datepicker({
			language: 'zh-TW',
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			format: "yyyy/mm/dd"
		});

		$('#m_timepicker_1').timepicker({
			minuteStep: 1,
			showSeconds: false,
			showMeridian: false,
			snapToStep: false,
			defaultTime: "00:00"
		});

		$('#m_timepicker_2').timepicker({
			minuteStep: 1,
			showSeconds: false,
			showMeridian: false,
			snapToStep: false,
			defaultTime: "23:59"
		});

		$('#m_datepicker_1').datepicker('update', new Date());
		$('#m_datepicker_2').datepicker('update', new Date());

		var m_datepicker_1 = document.getElementById("m_datepicker_1");
		var m_datepicker_2 = document.getElementById("m_datepicker_2");
		var m_timepicker_1 = document.getElementById("m_timepicker_1");
		var m_timepicker_2 = document.getElementById("m_timepicker_2");

		document.getElementById("m_datepicker_1").onchange = function(){ CheckDateTimeRange1(); };
		document.getElementById("m_datepicker_2").onchange = function(){ CheckDateTimeRange2(); };
		document.getElementById("m_timepicker_1").onchange = function(){ CheckDateTimeRange1(); };
		document.getElementById("m_timepicker_2").onchange = function(){ CheckDateTimeRange2(); };

		m_datepicker_1.time_range_flag = m_datepicker_2.time_range_flag = m_timepicker_1.time_range_flag = m_timepicker_2.time_range_flag = false;
	}
	catch(e){console.log(e);}
}
//-----
function RefreshWarningList()
{
	try
	{
		DestroyTable("warning_table");

		var s_date = $("#m_datepicker_1").val();
		var e_date = $("#m_datepicker_2").val();
		var s_time = $('#m_timepicker_1').val() + ":00.000";
		var e_time = $('#m_timepicker_2').val() + ":59.999";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

		var gateway_id = document.getElementById("warning_gateway_select").value;
		var sensor_type = document.getElementById("warning_sensor_type_select").value;
		var warning_type = document.getElementById("warning_type_select").value;

		//-------------------------------------------------------------------------------------

		var results = LoadWarningTableList(start, end, gateway_id, sensor_type, warning_type);
		var data = WarningTableList(results.warning_list, true);

		//-------------------------------------------------------------------------------------
		
		var table_list = document.getElementById("warning_table_list");
		table_list.innerHTML = data.table_list;

		//-------------------------------------------------------------------------------------

		var out_data = data.out_data;
		document.getElementById("btn_ExecOutport").onclick = function()
		{
			var _download = document.getElementById("btn_ExecOutport");		
			var _file_ext = document.getElementById("radioItem_CSV").checked ? ".csv" : ".txt";
			var _blob = new Blob(["\uFEFF" + out_data], {type: "text/plain;charset=utf-8"});
			var _file_name = "WarningList"+"_"+TimestampToDate2(start)+"_"+TimestampToDate2(end)+_file_ext;
			
			if(window.navigator && window.navigator.msSaveOrOpenBlob) 
			{
				window.navigator.msSaveOrOpenBlob(_blob, _file_name);
			}
			else
			{
				_download.href = URL.createObjectURL(_blob);
				_download.download = _file_name;
			}
			
			$("#m_modal_download").modal('toggle');
		};

		InitTable("warning_table");
	}
	catch(e){console.log(e);}
}
//-------
function LoadWarningTableList(start_time, end_time, gateway_id, sensor_type, wanring_type)
{
	try
	{
		var warning_list = [];
		var count_data = {disconnect: 0, data_warning: 0};

		for(var gw_suid in _Gatewaylist)
		{
			var gw_item = _Gatewaylist[gw_suid];
			var gw_ukey = gw_item.uuid + "_" + gw_item.suid;

			var gw_flag = (gateway_id == gw_ukey) || (gateway_id == "0") ? true : false;
			var sensor_type_flag = (sensor_type == gw_item.type) || (sensor_type == "0") ? true : false;

			if(gw_flag == true && sensor_type_flag == true)
			{
				var resp = Api_GetCloudDataByTime("Gateway", "status-" + gw_suid, start_time, end_time, null);

				if(resp != null && resp.length > 0)
				{
					var resp = JSON.parse(resp);
					var results = resp.results;

					for(var i = 0; i < results.length; i++)
					{
						var item = results[i];
						var base_tunnel = item.base_tunnel == null ? {} : item.base_tunnel;
						var lora_tunnel = item.lora_tunnel == null ? {} : item.lora_tunnel;
						var base_status = base_tunnel.status == null ? "--" : base_tunnel.status;
						var lora_status = lora_tunnel.status == null ? "--" : lora_tunnel.status;
						var base_time = base_tunnel.status_time;
						var lora_time = lora_tunnel.status_time;

						base_status = (base_status == "timeout") ? "disconnect" : base_status;
						lora_status = (lora_status == "timeout") ? "disconnect" : lora_status;

						base_tunnel.status = base_status;
						lora_tunnel.status = lora_status;

						var base_type_flag1 = ((base_status == "disconnect") || (base_status  == "error")) ? true : false;
						var base_type_flag2 = (wanring_type == base_status)  || (wanring_type == "0") ? true : false;

						var lora_type_flag1 = ((lora_status == "disconnect") || (lora_status  == "error")) ? true : false;
						var lora_type_flag2 = (wanring_type == lora_status)  || (wanring_type == "0") ? true : false;

						if(base_time != null && base_type_flag1 == true && base_type_flag2 == true)
						{
							var tmp = {};
							tmp.dev_name = gw_item.name;
							tmp.dev_type = gw_item.type_name;
							tmp.type = gw_item.type;
							tmp.time = base_time;
							tmp.base_tunnel = item.base_tunnel;

							warning_list.push(tmp);

							//------------------------------------------

							count_data[base_status]++;
						}

						if(lora_time != null && lora_type_flag1 == true && lora_type_flag2 == true)
						{
							var tmp = {};
							tmp.dev_name = gw_item.name;
							tmp.dev_type = gw_item.type_name;
							tmp.type = gw_item.type;
							tmp.time = lora_time;
							tmp.lora_tunnel = item.lora_tunnel;

							warning_list.push(tmp);

							//------------------------------------------

							count_data[lora_status]++;
						}
					}
				}
			}
		}

		//----------------------------------------------------

		for(var sensor_suid in _Sensorlist)
		{
			var sensor = _Sensorlist[sensor_suid];
			var gw_ukey = sensor.gateway_uuid + "_" + sensor.gateway_suid;

			var gw_flag = (gateway_id == gw_ukey) || (gateway_id == "0") ? true : false;
			var sensor_type_flag = (sensor_type == sensor.type) || (sensor_type == "0") ? true : false;

			if(gw_flag == true && sensor_type_flag == true)
			{
				var resp = Api_GetCloudDataByTime("Sensor", "status-" + sensor_suid, start_time, end_time, null);

				if(resp != null && resp.length > 0)
				{
					var resp = JSON.parse(resp);
					var results = resp.results;

					for(var i = 0; i < results.length; i++)
					{
						var item = results[i];
						var status = item.status;

						status = (status == "timeout") ? "disconnect" : status;

						var type_flag1 = ((status == "disconnect") || (status == "data_warning")) ? true : false;
						var type_flag2 = (wanring_type == status) || (wanring_type == "0") ? true : false;

						if(type_flag1 && type_flag2)
						{
							item.dev_name = sensor.name;
							item.dev_type = sensor.type_name;
							item.type = sensor.type;
							warning_list.push(item);

							//------------------------------------------

							count_data[status]++;
						}
					}
				}
			}
		}

		//-------------------------------------------------------------------------------------

		warning_list = warning_list.sort(function(a, b) {
			return (a.time < b.time) ? 1 : -1;
		});

		//-------------------------------------------------------------------------------------

		var result = {};
		result.warning_list = warning_list;
		result.count_data = count_data;

		return result;
	}
	catch(e){console.log(e);}

	return null;
}
//-------
function WarningTableList(warning_list, out)
{
	try
	{
		var table_list = "";
		var out_data = "";
		var index = 1;

		if(out == true) {out_data += "項次,設備類別,設備名稱,警示項目,警示說明,警示時間\r\n" };

		for(var i = 0; i < warning_list.length; i++)
		{
			var item = warning_list[i];
			var status_str = "--";
			var warning_desc = "";
			var warning_desc_2 = "";
			var warning_desc_array = [];
			var time = "--";

			if(item.type == GatewayType)
			{
				var base_tunnel = item.base_tunnel;
				var lora_tunnel = item.lora_tunnel;

				var status = "";
				var tmp_desc = ""

				if(base_tunnel != null)
				{
					status = (base_tunnel.status == null) ? "--" : base_tunnel.status;
					time = (base_tunnel.status_time == null) ? "--" : TimestampToDate(base_tunnel.status_time, true);
					
					if(status == "error")		{ status_str = "設備異常"; 	tmp_desc = "主要通道異常"; }
					if(status == "disconnect")	{ status_str = "設備離線"; 	tmp_desc = "主要通道離線"; }
				}
				else if(lora_tunnel != null)
				{
					status = (lora_tunnel.status == null) ? "--" : lora_tunnel.status;
					time = (lora_tunnel.status_time == null) ? "--" : TimestampToDate(lora_tunnel.status_time, true);

					if(status == "error")		{ status_str = "設備異常"; 	tmp_desc = "備用通道異常"; }
					if(status == "disconnect")	{ status_str = "設備離線"; 	tmp_desc = "備用通道離線"; }
				}

				if(status == "error")			{ warning_desc_array.push(tmp_desc); }
				else if(status == "disconnect")	{ warning_desc_array.push(tmp_desc); }
				else {	continue;  }
			}
			else
			{
				var status = (item.status == null) ? "--" : item.status;
				time = (item.time == null) ? "--" : TimestampToDate(item.time, true);

				if(status == "disconnect")	 { status_str = "設備離線";  warning_desc_array.push("設備離線"); }
				else if(status == "timeout") { status_str = "設備離線";  warning_desc_array.push("設備離線"); }
				//else if(status == "error") { status_str = "設備異常";  warning_desc_array.push("設備異常"); }
				//else if(status == "data_error")   { status_str = "數據錯誤";   warning_desc_array.push("數值超出設備的正常範圍"); }
				else if(status == "data_warning") 
				{ 
					status_str = "超出安全值";

					if(item.type == WaterPressure)
					{
						var water_stage = item.water_stage;
						if(water_stage != null) { if(water_stage.status == "data_warning") { warning_desc_array.push(("水位高度: " + water_stage.value + " (公分)，安全值範圍: " + water_stage.low + "~" + water_stage.high + " (公分)")); }}
					}
					else if(item.type == Acceleration)
					{
						var acceleration = item.acceleration;
						if(acceleration != null) { if(acceleration.status == "data_warning") { warning_desc_array.push(("加速度: " + acceleration.value + " (gal)，安全值範圍: " + acceleration.low + "~" + acceleration.high + " (gal)")); }}
					}
					else if(item.type == Inclinometer)
					{
						var inclinometer = item.inclinometer;
						if(inclinometer != null) { if(inclinometer.status == "data_warning") { warning_desc_array.push(("傾斜度: " + inclinometer.value + " (度)，安全值範圍: " + inclinometer.low + "~" + inclinometer.high + " (度)")); }}
					}
					else if(item.type == Earthquake)
					{
						var earthquake = item.earthquake;
						if(earthquake != null)
						{
							var axis_x = earthquake.axis_x;
							var axis_y = earthquake.axis_y;
							var axis_z = earthquake.axis_z;
							var axis_c = earthquake.axis_compose;
							var axis_m = earthquake.mmi_level;

							if(axis_x != null) { if(axis_x.status == "data_warning") { warning_desc_array.push(("X軸加速度: " + axis_x.value + " (gal)，安全值範圍: " + axis_x.low + "~" + axis_x.high + " (gal)")); }}
							if(axis_y != null) { if(axis_y.status == "data_warning") { warning_desc_array.push(("Y軸加速度: " + axis_y.value + " (gal)，安全值範圍: " + axis_y.low + "~" + axis_y.high + " (gal)")); }}
							if(axis_z != null) { if(axis_z.status == "data_warning") { warning_desc_array.push(("Z軸加速度: " + axis_z.value + " (gal)，安全值範圍: " + axis_z.low + "~" + axis_z.high + " (gal)")); }}
							if(axis_c != null) { if(axis_c.status == "data_warning") { warning_desc_array.push(("三軸加速度: " + axis_c.value + " (gal)，安全值範圍: " + axis_c.low + "~" + axis_c.high + " (gal)")); }}
							if(axis_m != null) { if(axis_m.status == "data_warning") { warning_desc_array.push(("震度: " + axis_m.value + " (震度)，安全值範圍: " + axis_m.low + "~" + axis_m.high + " (震度)")); }}
						}
					}
				}
				else
				{
					continue;
				}
			}

			//-------------------------------------------------------------------------------------

			for(var n = 0; n < warning_desc_array.length; n++) {
				warning_desc += warning_desc_array[n] + "<br/>";

				if(out == true) { warning_desc_2 += warning_desc_array[n]; }
			}

			var str = "";
			str += "<tr>";
			str += "<td>" + (index) + "</td>";
			str += "<td>" + (item.dev_type == null ? "--" : item.dev_type) + "</td>";
			str += "<td>" + (item.dev_name == null ? "--" : item.dev_name) + "</td>";
			str += "<td>" + status_str + "</td>";
			str += "<td>" + warning_desc + "</td>";
			str += "<td>" + time.replace(/_/g, " ") + "</td>";
			str += "</tr>";

			table_list += str;

			//------------------------------------------------------------------------------------

			if(out == true) { out_data += (index)+','+item.dev_type+','+item.dev_name+','+status_str+','+warning_desc_2+','+time+'\r\n'; }

			index++;
		}

		var result = {};
		result.table_list = table_list;
		result.out_data = out_data;

		return result;
	}
	catch(e){console.log(e);}

	return null;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ShowStationTable()
{
	document.getElementById("m_portlet_index").style.display = "block";

	InitGatewayTable();

	if(data_gateway != null)
	{
		document.getElementById("gw_area_select").options[data_gateway.area].selected = true;
		document.getElementById("gw_river_select").options[data_gateway.river].selected = true;
	}

	RefreshGatewayList();
}
//-----
function InitGatewayTable()
{
	try
	{
		data_gateway = null;

		var m_data = document.getElementById("m_data");
		m_data.innerHTML = "";

		var gateway_search = _User.http_get_unauth('./template/gateway_table_search.js');
		var gateway_table = _User.http_get_unauth('./template/gateway_table.js');

		m_data.innerHTML += gateway_search;
		m_data.innerHTML += gateway_table;

		//------------------------------------------------------

		document.getElementById("table_title").innerHTML = "監測站查詢";

		//------------------------------------------------------

		var area_select = document.getElementById("gw_area_select");
		var river_select = document.getElementById("gw_river_select");
		
		var area_options = "<option value='0'>全部</option>";
		var river_options = "<option value='0'>全部</option>";

		var area_list = [];
		var river_list = [];

		for(var gw in _Gatewaylist) {
			var tmp = _Gatewaylist[gw];
			area_list[tmp.area] = tmp.area;
			river_list[tmp.river] = tmp.river;
		}

		for(var area in area_list) {
			area_options += '<option value={index}>{name}</option>'.replace(/{index}/g, area).replace(/{name}/g, area);
		}
		
		for(var river in river_list) {
			river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
		}

		area_select.innerHTML = area_options;
		river_select.innerHTML = river_options;

		area_select.onchange = function(){ RefreshGatewayList(); };
		river_select.onchange = function(){ RefreshGatewayList(); };
	}
	catch(e){console.log(e);}
}
//-----
function RefreshGatewayList()
{
	try
	{
		DestroyTable("m_table_1");

		var _info_btn = '<a href="#" id="{info_btn_id}" onclick="ShowGatewayDetail(this);"><span>詳細</span></a>';
		var _out_data = '';
		var _area = document.getElementById("gw_area_select").value;
		var _river = document.getElementById("gw_river_select").value;
		var _table_list = document.getElementById("table_list");
		_table_list.innerHTML = "";

		_out_data += "ID,名稱,區域,管理機關,河川別,岸別,TWD97_X,TWD97_Y\r\n";

		var _index = 1;
		var list = [];
		for(var _gatewaySUID in _Gatewaylist)
		{
			//console.log("GatewaySUID:" +  _gatewaySUID);
			var _obj = Api_GetGatewayData(_gatewaySUID);

			if(_obj != null)
			{
				var _area_flag  = (_area == _obj.area)  || (_area == "0")  ? true : false;
				var _river_flag = (_river == _obj.river) || (_river == "0") ? true : false;

				if(_area_flag && _river_flag)
				{
					var _id = _obj.uuid + "." + _obj.suid;
					var _btn_id = "infobtn_" + _obj.uuid + "_" + _obj.suid;
					var _gw_ukey = _obj.uuid + "_" + _obj.suid;
					var _str = "";
					
					_str += "<tr>";
					_str += "<td>"+(_obj.name==null?"--":_obj.name)+"</td>";
					_str += "<td>"+(_obj.area==null?"--":_obj.area)+"</td>";
					_str += "<td>"+(_obj.admin==null?"--":_obj.admin)+"</td>";
					_str += "<td>"+(_obj.river==null?"--":_obj.river)+"</td>";
					_str += "<td>"+(_obj.bank==null?"--":_obj.bank)+"</td>";
					_str += "<td>"+(_obj.coordinate==null?"--":_obj.coordinate.x)+"</td>";
					_str += "<td>"+(_obj.coordinate==null?"--":_obj.coordinate.y)+"</td>";
					_str += "<td id=base_status_" + _gw_ukey +">--</th>";
					_str += "<td id=lora_status_" + _gw_ukey +">--</th>";
					_str += "<td>"+ _info_btn.replace("{info_btn_id}", _btn_id) + "</td>";
					_str += "</tr>";

					_table_list.innerHTML += _str;
					_index = _index + 1;

					list.push({uuid: _obj.uuid, suid: _obj.suid});

					_out_data += _id+","+_obj.name+","+_obj.area+","+_obj.admin+","+_obj.river+","+_obj.bank+","+_obj.coordinate.x+","+_obj.coordinate.y+"\r\n";
				}
			}
		}

		//----------------------------------------------------------

		for(var i = 0; i < list.length; i++)
		{
			var gw = list[i];
			SensorGetLastData(gw.uuid, "status-" + gw.suid, 1);
		}

		//-----------------------------------------------------------

		document.getElementById("btn_ExecOutport").onclick = function()
		{
			var _download = document.getElementById("btn_ExecOutport");		
			var _file_ext = document.getElementById("radioItem_CSV").checked ? ".csv" : ".txt";
			var _blob = new Blob(["\uFEFF" + _out_data], {type: "text/plain;charset=utf-8"});
			var _file_name = "gateway"+_file_ext;
			
			if(window.navigator && window.navigator.msSaveOrOpenBlob) 
			{
				window.navigator.msSaveOrOpenBlob(_blob, _file_name);
			}
			else
			{
				_download.href = URL.createObjectURL(_blob);
				_download.download = _file_name;
			}
			
			$("#m_modal_download").modal('toggle');
		};

		//---------------------------------------------------------

		InitTable("m_table_1");
	}
	catch(e){console.log(e);}
}
//-----
function ShowGatewayDetail(info_btn)
{
	try
	{
		var id = info_btn.id;
		var tmp_array = id.split("_");
		var gateway_uuid = tmp_array[1];
		var gateway_suid = tmp_array[2];

		current_gateway.uuid = gateway_uuid;
		current_gateway.suid = gateway_suid;

		InitGatewayDetail();
		RefreshGatewayDetail();
	}
	catch(e){console.log(e);}
}
//-----
function InitGatewayDetail()
{
	try
	{
		data_gateway = {};

		var m_data = document.getElementById("m_data");

		data_gateway.innerHTML = m_data.innerHTML;
		data_gateway.area = document.getElementById("gw_area_select").selectedIndex;
		data_gateway.river = document.getElementById("gw_river_select").selectedIndex;

		var gateway_detail = _User.http_get_unauth('./template/gateway_detail.js');
		var gateway_detail_search = _User.http_get_unauth('./template/gateway_detail_search.js');

		m_data.innerHTML = "";
		m_data.innerHTML += gateway_detail_search;
		m_data.innerHTML += gateway_detail;
	}
	catch(e){console.log(e);}
}
//-----
function RefreshGatewayDetail()
{
	try
	{
		var gateway_suid = current_gateway.suid;
		var gateway_item = _Gatewaylist[gateway_suid];
		var sensor_list = gateway_item.sensor_list;

		document.getElementById("table_title").innerHTML = gateway_item.name;
		document.getElementById("detail_info_name").innerHTML = gateway_item.name;
		document.getElementById("detail_info_area").innerHTML = gateway_item.area;
		document.getElementById("detail_info_admin").innerHTML = gateway_item.admin;
		document.getElementById("detail_info_river").innerHTML = gateway_item.river;
		document.getElementById("detail_info_bank").innerHTML = gateway_item.bank;
		document.getElementById("detail_info_X_97").innerHTML = gateway_item.coordinate.x;
		document.getElementById("detail_info_Y_97").innerHTML = gateway_item.coordinate.y;

		//-------------------------------------------------------------------------------------

		var table_list = document.getElementById("sensor_table_list");
		table_list.innerHTML = "";

		var index = 1;
		for(var i = 0; i < sensor_list.length; i++)
		{
			var sensor_suid = sensor_list[i];
			var sensor_item = Api_GetSensorData(sensor_suid);

			if(sensor_item != null)
			{
				var str = "";
				str += "<tr>";
				str += "<td>"+(sensor_item.name==null?"--":sensor_item.name)+"</td>";
				str += "<td>"+(sensor_item.type_name==null?"--":sensor_item.type_name)+"</td>";
				str += "<td>"+(sensor_item.coordinate==null?"--":sensor_item.coordinate.x)+"</td>";
				str += "<td>"+(sensor_item.coordinate==null?"--":sensor_item.coordinate.y)+"</td>";
				str += "</tr>";
				table_list.innerHTML += str;
				index = index + 1;
			}	
		}
	}
	catch(e){console.log(e);}
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ShowSensorTable()
{
	try
	{
		document.getElementById("m_portlet_index").style.display = "block";

		InitSensorTable();

		if(data_sensor != null)
		{
			document.getElementById("sensor_gw_select").options[data_sensor.gw].selected = true;
			document.getElementById("sensor_type_select").options[data_sensor.dtype].selected = true;
			document.getElementById("sensor_area_select").options[data_sensor.area].selected = true;
			document.getElementById("sensor_river_select").options[data_sensor.river].selected = true;
		}

		RefreshSensorList();
	}
	catch(e){console.log(e);}
}
//-----
function InitSensorTable()
{
	try
	{
		var m_data = document.getElementById("m_data");
		m_data.innerHTML = "";

		var sensor_search = _User.http_get_unauth('./template/sensor_table_search.js');
		var sensor_table = _User.http_get_unauth('./template/sensor_table.js');

		m_data.innerHTML += sensor_search;
		m_data.innerHTML += sensor_table;

		//------------------------------------------------------

		document.getElementById("table_title").innerHTML = "感測器查詢";

		//------------------------------------------------------

		var river_select = document.getElementById("sensor_river_select");
		var area_select = document.getElementById("sensor_area_select");
		var gw_select = document.getElementById("sensor_gw_select");
		var type_select = document.getElementById("sensor_type_select");

		var gw_options    = "<option value='0'>全部</option>";
		var area_options  = "<option value='0'>全部</option>";
		var river_options = "<option value='0'>全部</option>";

		var area_list = [];
		var river_list = [];

		for(var gw in _Gatewaylist) {
			var tmp = _Gatewaylist[gw];
			gw_options += '<option value={index}>{name}</option>'.replace(/{index}/g, tmp.uuid+"_"+tmp.suid).replace(/{name}/g, tmp.name);

			area_list[tmp.area] = tmp.area;
			river_list[tmp.river] = tmp.river;
		}

		for(var area in area_list) {
			area_options += '<option value={index}>{name}</option>'.replace(/{index}/g, area).replace(/{name}/g, area);
		}

		for(var river in river_list) {
			river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
		}

		gw_select.innerHTML = gw_options;
		area_select.innerHTML = area_options;
		river_select.innerHTML = river_options;

		gw_select.onchange = function(){ RefreshSensorList(); };
		area_select.onchange = function(){ RefreshSensorList(); };
		river_select.onchange = function(){ RefreshSensorList(); };
		type_select.onchange = function(){ RefreshSensorList(); };

		//------------------------------------------------------
	}
	catch(e){console.log(e);}
}
//-----
function RefreshSensorList()
{
	try
	{
		DestroyTable("m_table_1");

		var info_btn = '<a href="#" id="{info_btn_id}" onclick="ShowSensorDetail(this);"><span>詳細</span></a>';

		var gw = document.getElementById("sensor_gw_select").value;
		var area = document.getElementById("sensor_area_select").value;
		var river = document.getElementById("sensor_river_select").value;
		var type = document.getElementById("sensor_type_select").value;

		var table_list_title = document.getElementById("table_list_title");
		table_list_title.innerHTML = "";

		var table_list = document.getElementById("table_list");
		table_list.innerHTML = "";

		//----------------------------------------------------------------------

		var title_array = ["名稱", "類型", "監測站", "區域", "管理機關", "河川別", "岸別", "TWD97_X", "TWD97_Y", "狀態", "時間"];

		var title_array_sensor = [];
		if(type == WaterPressure) 	  { title_array_sensor = ["水位高度 (公分)", "更多資訊"]; }
		else if(type == Acceleration) { title_array_sensor = ["加速度 (gal)", "更多資訊"]; }
		else if(type == Inclinometer) { title_array_sensor = ["傾斜度 (度)", "更多資訊"]; }
		else if(type == Earthquake)   { title_array_sensor = ["X軸加速度 (gal)", "Y軸加速度 (gal)", "Z軸加速度 (gal)", "三軸加速度 (gal)", "震度 (級)", "更多資訊"]; }

		//----------------------------------------------------------------------

		var str = "<tr>";
		for(var i = 0; i < title_array.length; i++) {
			str += "<th>" + title_array[i] + "</th>";
		}

		for(var i = 0; i < title_array_sensor.length; i++) {
			str += "<th>" + title_array_sensor[i]+ "</th>";
		}
		str += "</tr>";

		table_list_title.innerHTML = str;

		//-----------------------------------------------------------------------

		var index = 1;
		var list = [];
		var out_data = "";
		out_data += "ID,名稱,類型,監測站,區域,管理機關,河川別,岸別,TWD97_X,TWD97_Y\r\n";

		for(var suid in _Sensorlist)
		{
			var obj = Api_GetSensorData(suid);
			if(obj != null)
			{
				var gw_ukey = obj.gateway_uuid + "_" + obj.gateway_suid;
				var sensor_ukey = obj.uuid + "_" + obj.suid;

				var area_flag  = (area  == obj.area)  || (area  == "0") ? true : false;
				var river_flag = (river == obj.river) || (river == "0") ? true : false;
				var type_flag  = (type  == obj.type)  || (type  == "0") ? true : false;
				var gw_flag    = (gw    == gw_ukey)   || (gw    == "0") ? true : false;

				if(gw_flag && area_flag && river_flag && type_flag)
				{
					var btn_id = "infobtn_" + obj.uuid + "_" + obj.suid;

					var str = "";
					str += "<tr>";
					str += "<td>"+(obj.name==null?"--":obj.name)+"</td>";
					str += "<td>"+(obj.type_name==null?"--":obj.type_name)+"</td>";
					str += "<td>"+(obj.gateway_name==null?"--":obj.gateway_name)+"</td>";
					str += "<td>"+(obj.area==null?"--":obj.area)+"</td>";
					str += "<td>"+(obj.admin==null?"--":obj.admin)+"</td>";
					str += "<td>"+(obj.river==null?"--":obj.river)+"</td>";
					str += "<td>"+(obj.bank==null?"--":obj.bank)+"</td>";
					str += "<td>"+(obj.coordinate==null?"--":obj.coordinate.x)+"</td>";
					str += "<td>"+(obj.coordinate==null?"--":obj.coordinate.y)+"</td>";

					if(type == WaterPressure || type == Acceleration || type == Inclinometer)
					{
						str += "<td id=value_status_" + sensor_ukey +">--</th>";
						str += "<td id=value_time_" + sensor_ukey +">--</th>";
						str += "<td id=value_" + sensor_ukey +">--</th>";
					}
					else if(type == Earthquake)
					{
						str += "<td id=value_status_" + sensor_ukey +">--</th>";
						str += "<td id=value_time_" + sensor_ukey +">--</th>";
						str += "<td id=value_x_" + sensor_ukey +">--</th>";
						str += "<td id=value_y_" + sensor_ukey +">--</th>";
						str += "<td id=value_z_" + sensor_ukey +">--</th>";
						str += "<td id=value_c_" + sensor_ukey +">--</th>";
						str += "<td id=value_m_" + sensor_ukey +">--</th>";
					}

					str += "<td>"+ info_btn.replace("{info_btn_id}", btn_id) + "</td>";
					str += "</tr>";

					table_list.innerHTML += str;

					index = index + 1;

					list.push({uuid: obj.uuid, suid: obj.suid, gateway_uuid: obj.gateway_uuid, gateway_suid: obj.gateway_suid});
					
					out_data += (obj.uuid+"_"+obj.suid)+","+obj.name+","+obj.type_name+","+obj.gateway_name+","+obj.area+","+obj.admin+","+obj.river+","+obj.bank+","+obj.coordinate.x+","+obj.coordinate.y+"\r\n";
				}
			}
		}

		//-------------------------------------------------------

		for(var i = 0; i < list.length; i++)
		{
			var sensor = list[i];

			SensorGetLastData(sensor.uuid, "status-" + sensor.suid, 1);
			SensorGetLastData(sensor.uuid, sensor.suid, 1);
		}

		//-------------------------------------------------------

		document.getElementById("btn_ExecOutport").onclick = function()
		{
			var _download = document.getElementById("btn_ExecOutport");		
			var _file_ext = document.getElementById("radioItem_CSV").checked ? ".csv" : ".txt";
			var _blob = new Blob(["\uFEFF" + out_data], {type: "text/plain;charset=utf-8"});
			var _file_name = "sensor"+_file_ext;

			if(window.navigator && window.navigator.msSaveOrOpenBlob) 
			{
				window.navigator.msSaveOrOpenBlob(_blob, _file_name);
			}
			else
			{
				_download.href = URL.createObjectURL(_blob);
				_download.download = _file_name;
			}
			
			$("#m_modal_download").modal('toggle');
		};

		//-------------------------------------------------------

		InitTable("m_table_1");
	}
	catch(e){console.log(e);}
}
//-----
function ShowSensorDetail(info_btn)
{
	try
	{
		var id = info_btn.id;
		var tmp_array = id.split("_");
		var sensor_uuid = tmp_array[1];
		var sensor_suid = tmp_array[2];

		//current_sensor.flag = 0;
		//current_sensor.uuid = sensor_uuid;
		//current_sensor.suid = sensor_suid;

		current_sensor.flag = 0;
		current_sensor = Api_GetSensorData(sensor_suid);

		InitSensorDetail();
		RefreshSensorDetail();
	}
	catch(e){console.log(e);}
}
//-----
function InitSensorDetail()
{
	try
	{
		data_sensor = {};

		var m_data = document.getElementById("m_data");

		data_sensor.innerHTML = m_data.innerHTML;
		data_sensor.gw = document.getElementById("sensor_gw_select").selectedIndex;
		data_sensor.area = document.getElementById("sensor_area_select").selectedIndex;
		data_sensor.river = document.getElementById("sensor_river_select").selectedIndex;
		data_sensor.dtype = document.getElementById("sensor_type_select").selectedIndex;

		var sensor_detail = _User.http_get_unauth('./template/sensor_detail.js');
		var sensor_detail_search = _User.http_get_unauth('./template/sensor_detail_search.js');

		m_data.innerHTML = "";
		m_data.innerHTML += sensor_detail_search;
		m_data.innerHTML += sensor_detail;

		//---------------------------------------------------------------------

		var arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
		
		var now_time_s = moment(new Date()).subtract(1,'hours').toDate();

		$("#m_datepicker_1").datepicker({
			rtl: mUtil.isRTL(),
			language: 'zh-TW',
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			format: "yyyy/mm/dd"
		});

		$("#m_datepicker_2").datepicker({
			rtl: mUtil.isRTL(),
			language: 'zh-TW',
			todayHighlight: true,
			orientation: "bottom left",
			templates: arrows,
			format: "yyyy/mm/dd"
		});

		$('#m_timepicker_1').timepicker(
		{
			minuteStep: 1,
			defaultTime: now_time_s,
			showSeconds: false,
			showMeridian: false,
			snapToStep: false
		});

		$('#m_timepicker_2').timepicker(
		{
			minuteStep: 1,
			defaultTime: 'current',
			showSeconds: false,
			showMeridian: false,
			snapToStep: false
		});

		$('#m_datepicker_1').datepicker('update', now_time_s);
		$('#m_datepicker_2').datepicker('update', new Date());

		//---------------------------------------------------------------------

		var m_datepicker_1 = document.getElementById("m_datepicker_1");
		var m_datepicker_2 = document.getElementById("m_datepicker_2");
		var m_timepicker_1 = document.getElementById("m_timepicker_1");
		var m_timepicker_2 = document.getElementById("m_timepicker_2");

		document.getElementById("m_datepicker_1").onchange = function(){ CheckDateTimeRange1(); };
		document.getElementById("m_datepicker_2").onchange = function(){ CheckDateTimeRange2(); };
		document.getElementById("m_timepicker_1").onchange = function(){ CheckDateTimeRange1(); };
		document.getElementById("m_timepicker_2").onchange = function(){ CheckDateTimeRange2(); };

		m_datepicker_1.time_range_flag = m_datepicker_2.time_range_flag = m_timepicker_1.time_range_flag = m_timepicker_2.time_range_flag = false;

		//---------------------------------------------------------------------

		day_data_count = 0; 
		day_data_array = [];
		day_time_array = [];

		$('#m_modal_chart').on('hidden.bs.modal', function () {
			CloseReportChart();
		});
	}
	catch(e){console.log(e);}
}
//-----
function RefreshSensorDetail()
{
	try
	{
		var sensor_suid = current_sensor.suid;
		var sensor_item = Api_GetSensorData(sensor_suid);

		document.getElementById("table_title").innerHTML = sensor_item.name;
		document.getElementById("detail_table_title").innerHTML = sensor_item.name + " (頁面最多顯示500筆)";

		document.getElementById("detail_info_name").innerHTML = sensor_item.name;
		document.getElementById("detail_info_gateway").innerHTML = sensor_item.gateway_name;
		document.getElementById("detail_info_area").innerHTML = sensor_item.area;
		document.getElementById("detail_info_admin").innerHTML = sensor_item.admin;
		document.getElementById("detail_info_river").innerHTML = sensor_item.river;
		document.getElementById("detail_info_bank").innerHTML = sensor_item.bank;
		document.getElementById("detail_info_X_97").innerHTML = sensor_item.coordinate.x;
		document.getElementById("detail_info_Y_97").innerHTML = sensor_item.coordinate.y;

		//-------------------------------------------------------------------------------------

		InitHistoryDataTable();

		if(sensor_suid.indexOf(WaterPressure) != -1){
			HistoryDataEasySearch();
		}
		else{
			RefreshHistoryDataList();
		}
	}
	catch(e){console.log(e);}
}
//-----
function InitHistoryDataTable()
{
	try
	{
		var sensor_uuid = current_sensor.uuid;
		var sensor_suid = current_sensor.suid;

		var title_array = null;

		if(sensor_suid.indexOf(WaterPressure) != -1) 	 { title_array = ["", "時間", "水位高度 (公分)"]; }
		else if(sensor_suid.indexOf(Acceleration) != -1) { title_array = ["", "時間", "加速度 (gal)"]; }
		else if(sensor_suid.indexOf(Inclinometer) != -1) { title_array = ["", "時間", "傾斜度 (度)"]; }
		else if(sensor_suid.indexOf(Earthquake)   != -1) { title_array = ["", "時間", "X軸加速度 (gal)", "Y軸加速度 (gal)", "Z軸加速度 (gal)", "三軸加速度 (gal)", "震度 (級)"]; }

		var table_list_title = document.getElementById("history_table_list_title");
		table_list_title.innerHTML = "";
		
		var str = "";
		for(var index in title_array) {
			str += "<th>" + title_array[index] + "</th>";
		}

		table_list_title.innerHTML = str;

		if(sensor_suid.indexOf(WaterPressure) != -1)
		{
			document.getElementById("radio_time_3hour").checked = true;
			document.getElementById("hours_radio_group").style.display = "";
		}
		else
		{
			document.getElementById("hours_radio_group").style.display = "none";
		}

		water_pressure_report_list = null;
		sensor_report_data = null;
		sensor_report_data = {};

		d3.select("#sensor_report").selectAll("*").remove();
	}
	catch(e){console.log(e);}
}
function ShowLoading(show)
{
	if(show)
	{
		document.getElementById("an_loading").style.display = "block";
		document.getElementById("detail_page").style.display = "none";
		document.getElementById("show_item_chart").style.display = "none";
	}
	else
	{
		document.getElementById("an_loading").style.display = "none";
		document.getElementById("detail_page").style.display = "block";
		document.getElementById("show_item_chart").style.display = "block";
	}
}
//-----
/*function GetCloudDataByTime(uuid, suid, stime, etime)
{
  try
  {
	var one_hour = 1 * 60 * 60 * 1000;
	var one_day = 24 * one_hour;

	var s_day = new Date(TimestampToDate(stime, false)).getTime();
	var e_day = new Date(TimestampToDate(etime, false)).getTime();
	var days = ((e_day - s_day) / one_day ) + 1;

	day_data_count = 0;
	day_data_array = null
	day_data_array = new Array(days);

	for(var i = 0; i < days; i++) {
		day_data_array[i] = null;
	}

	var day_index = 0;
	for(var i = days-1; i >= 0; i--)
	{
		var d_stime = s_day + i * one_day;
		var d_etime = s_day + (((i+1) * one_day) - 1);

		d_stime = (d_stime < stime) ? stime : d_stime;
		d_etime = (d_etime > etime) ? etime : d_etime;

		Api_GetCloudDataByTime3(uuid, suid, d_stime, d_etime, days, day_index, SuccessCallback, ErrorCallback);

		day_index = day_index + 1;
	}
  }
  catch(e){console.log(e);}
}
//-----
function SuccessCallback(data, days, day_index)
{
	try
	{
		if(data != null)
		{
			day_data_count = day_data_count + 1;
			day_data_array[day_index] = data;
		}

		if(day_data_count == days)
		{
			HistoryDataList();

			ShowLoading(false);
			day_data_count = 0;
			day_data_array = null
		}
	}
	catch(e){console.log(e);}
}
//-----
function ErrorCallback(resp, days, day_index)
{
	try
	{
		day_data_count = day_data_count + 1;

		if(day_data_count == days)
		{
			HistoryDataList();

			ShowLoading(false);
			day_data_count = 0;
			day_data_array = null;
		}

		var table_list = document.getElementById("history_table_list");
		table_list.innerHTML = "";

		d3.select("#sensor_report").selectAll("*").remove();

		ShowLoading(false);

		alert("資料讀取錯誤...");
	}
	catch(e){console.log(e);}
}*/

//-----
function GetCloudDataByTime(uuid, suid, stime, etime)
{
  try
  {
	var one_hour = 1 * 60 * 60 * 1000;
	var one_day = 24 * one_hour;

	var s_day = new Date(TimestampToDate(stime, false)).getTime();
	var e_day = new Date(TimestampToDate(etime, false)).getTime();
	var days = ((e_day - s_day) / one_day ) + 1;

	day_data_count = 0;

	day_data_array = null;
	day_data_array = new Array(days);
	day_time_array = null;
	day_time_array = new Array(days);

	for(var i = 0; i < days; i++) {
		day_data_array[i] = null;
		day_time_array[i] = null;
	}

	var day_index = 0;
	for(var i = days-1; i >= 0; i--)
	{
		var d_stime = s_day + i * one_day;
		var d_etime = s_day + (((i+1) * one_day) - 1);

		d_stime = (d_stime < stime) ? stime : d_stime;
		d_etime = (d_etime > etime) ? etime : d_etime;

		var srch_time = {};
		srch_time.d_stime = d_stime;
		srch_time.d_etime = d_etime;

		day_time_array[day_index] = srch_time;

		day_index = day_index + 1;
	}

	if(day_time_array.length > 0)
	{
		var srch_time = day_time_array[0];
		d_stime = srch_time.d_stime;
		d_etime = srch_time.d_etime;
		day_index = 0;

		Api_GetCloudDataByTime3(uuid, suid, d_stime, d_etime, days, day_index, SuccessCallback, ErrorCallback);
	}
	else
	{
		Api_GetCloudDataByTime3(uuid, suid, 0, 0, 1, 0, SuccessCallback, ErrorCallback);
	}

  }
  catch(e){console.log(e);}
}
//-----
function SuccessCallback(data, uuid, suid, days, day_index)
{
	try
	{
		if(data != null)
		{
			day_index = day_index + 1;
			day_data_count = day_data_count + 1;
			day_data_array[day_index] = data;
		}

		if(day_data_count != days)
		{
			var srch_time = day_time_array[day_index];
			d_stime = srch_time.d_stime;
			d_etime = srch_time.d_etime;

			Api_GetCloudDataByTime3(uuid, suid, d_stime, d_etime, days, day_index, SuccessCallback, ErrorCallback);
		}
		else if(day_data_count == days)
		{
			HistoryDataList();

			ShowLoading(false);
			day_data_count = 0;
			day_data_array = null;
		}
	}
	catch(e){console.log(e);}
}
//-----
function ErrorCallback(resp, uuid, suid, days, day_index)
{
	try
	{
		day_data_count = day_data_count + 1;

		if(day_data_count != days)
		{
			var srch_time = day_time_array[day_index];
			d_stime = srch_time.d_stime;
			d_etime = srch_time.d_etime;

			Api_GetCloudDataByTime3(uuid, suid, d_stime, d_etime, days, day_index, SuccessCallback, ErrorCallback);
		}
		else if(day_data_count == days)
		{
			HistoryDataList();

			ShowLoading(false);
			day_data_count = 0;
			day_data_array = null;
			day_time_array = null;
		}

		/*var table_list = document.getElementById("history_table_list");
		table_list.innerHTML = "";

		d3.select("#sensor_report").selectAll("*").remove();

		ShowLoading(false);

		alert("資料讀取錯誤...");*/
	}
	catch(e){console.log(e);}
}
//-----
function HistoryDataEasySearch()
{
	try
	{
		var before_time = 3 * 60;

		if(document.getElementById("radio_time_3hour").checked == true)		    { before_time = 3 * 60;  }
		else if(document.getElementById("radio_time_6hour").checked == true)	{ before_time = 6 * 60;	 }
		else if(document.getElementById("radio_time_12hour").checked == true)	{ before_time = 12 * 60; }

		var now_time = (Math.round(moment().unix() / 600) * 600) * 1000;
		var time_1 = moment(now_time).subtract(before_time, 'minutes').toDate();
		var time_2 = moment(now_time).toDate();

		$('#m_datepicker_1').datepicker('update', time_1); 
		$('#m_timepicker_1').timepicker('setTime', time_1);

		$('#m_datepicker_2').datepicker('update', time_2); 
		$('#m_timepicker_2').timepicker('setTime', time_2); 

		RefreshHistoryDataList();
	}
	catch(e){console.log(e);}
}
//-----
function RefreshHistoryDataList()
{
	try
	{
		var table_list = document.getElementById("history_table_list");
		table_list.innerHTML = "";

		d3.select("#sensor_report").selectAll("*").remove();

		ShowLoading(true);

		var sensor_uuid = current_sensor.uuid;
		var sensor_suid = current_sensor.suid;

		var s_date = $("#m_datepicker_1").val();
		var e_date = $("#m_datepicker_2").val();
		var s_time = $('#m_timepicker_1').val() + ":00.000";
		var e_time = $('#m_timepicker_2').val() + ":59.999";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf() - 5*60*1000;
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf() + 5*60*1000;

		GetCloudDataByTime(sensor_uuid, sensor_suid, start, end);

	}
	catch(e){console.log(e);}
}
//-----
function HistoryDataList()
{
	try
	{
		//-------------------------------------------------------------------------------------

		var table_list = document.getElementById("history_table_list");
		table_list.innerHTML = "";

		d3.select("#sensor_report").selectAll("*").remove();

		//-------------------------------------------------------------------------------------

		var sensor_uuid = current_sensor.uuid;
		var sensor_suid = current_sensor.suid;

		var sensor_item = Api_GetSensorData(sensor_suid);
		var name = sensor_item.name;

		var sensor_type = CheckSensorType(sensor_suid);

		var unit = "";
		var out_data = "";

		if(_User.GetCurrentUserID() == "data")
		{
			if(sensor_type == WaterPressure)		{ out_data += "項次,時間,水位高度 (公分),Index,備用通道,回補\r\n"; 	unit = "公分"; }
			else if(sensor_type == Acceleration)	{ out_data += "項次,時間,加速度 (gal),Index,備用通道,回補 \r\n"; 		unit = "gal"; }
			else if(sensor_type == Inclinometer)	{ out_data += "項次,時間,傾斜度 (度),Index,備用通道,回補\r\n"; 		unit = "度"; }
			else if(sensor_type == Earthquake)		{ out_data += "項次,時間,X軸加速度 (gal),Y軸加速度 (gal),Z軸加速度 (gal),三軸加速度 (gal),震度 (級),Index,備用通道,回補\r\n"; 	unit = "gal / 級"; }
		}
		else
		{
			if(sensor_type == WaterPressure)		{ out_data += "項次,時間,水位高度 (公分),備用通道,回補\r\n"; 	unit = "公分"; }
			else if(sensor_type == Acceleration)	{ out_data += "項次,時間,加速度 (gal),備用通道,回補 \r\n"; 		unit = "gal"; }
			else if(sensor_type == Inclinometer)	{ out_data += "項次,時間,傾斜度 (度),備用通道,回補\r\n"; 		unit = "度"; }
			else if(sensor_type == Earthquake)		{ out_data += "項次,時間,X軸加速度 (gal),Y軸加速度 (gal),Z軸加速度 (gal),三軸加速度 (gal),震度 (級),備用通道,回補\r\n"; 	unit = "gal / 級"; }
		}

		var s_date = $("#m_datepicker_1").val();
		var e_date = $("#m_datepicker_2").val();
		var s_time = $('#m_timepicker_1').val() + ":00.000";
		var e_time = $('#m_timepicker_2').val() + ":59.999";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

		//-------------------------------------------------------------------------------------

		var results = [];
		if(day_data_array != null && day_data_array.length > 0)
		{
			for(var i = 0; i < day_data_array.length; i++)
			{
				var data = day_data_array[i];
				if(data != null){
					results = results.concat(data.results);
				}
			}
		}

		//-------------------------------------------------------------------------------------

		if(results != null && results.length > 0)
		{
			document.getElementById("detail_table_desc").innerHTML = "";
			document.getElementById("sensor_remark").innerHTML = "標註:" + current_sensor.remark;

			results = results.sort(function(a, b) {
				return (a.time < b.time) ? 1 : -1;
			});

			var tmp_results = [];

			for(var t = 0; t <　results.length; t++) {
				var data = results[t];
				tmp_results.push(data);
			}

			results = [];

			for(var t = 0; t < tmp_results.length; t++)
			{
				var data = tmp_results[t];
				var time = data.time;

				if(time >= start && time <= end) {
					results.push(data);
				}
			}

			HistoryData(sensor_type, results);

			sensor_report_data.uuid = sensor_uuid;
			sensor_report_data.sensor_name = name;
			sensor_report_data.sensor_type = sensor_type;
			sensor_report_data.results = results;
			sensor_report_data.start = start;
			sensor_report_data.end = end;
			sensor_report_data.unit = unit;
			sensor_report_data.alert_limit = sensor_item.alert_limit;

			ShowSensorReport(sensor_report_data); 

			//-------------------------------------------------------

			for(var i = 0; i < results.length; i++) 
			{
				var data = results[i];

				var time = ((data.time == null) ? "--" : TimestampToDate(data.time, true));
				var value = "";
				var ret = (data.retransmission == true) ? "*" : "";
				var tunnel = (data.tunnel == "lora_tunnel") ? "@" : "";
				var index = data.index;

				if(sensor_type == WaterPressure)     { value += ((data.water_stage == null) ? "--" : data.water_stage.value); }
				else if(sensor_type == Acceleration) { value += ((data.acceleration ==  null) ? "--" : data.acceleration.value); }
				else if(sensor_type == Inclinometer) { value += ((data.inclinometer ==  null) ? "--" : data.inclinometer.value); }
				else if(sensor_type == Earthquake) 
				{
					var earthquake = data.earthquake;
					if(earthquake != null)
					{
						value += ((earthquake.axis_x == null) ? "--" : earthquake.axis_x.value) + ",";
						value += ((earthquake.axis_y == null) ? "--" : earthquake.axis_y.value) + ",";
						value += ((earthquake.axis_z == null) ? "--" : earthquake.axis_z.value) + ",";
						value += ((earthquake.axis_compose ==  null) ? "--" : earthquake.axis_compose.value) + ",";
						value += ((earthquake.mmi_level ==  null) ? "--" : earthquake.mmi_level.value);
					}
				}

				if(_User.GetCurrentUserID() == "data"){
					time = ((data.time == null) ? "--" : TimestampToDate3(data.time));
					out_data += (i+1)+","+time+","+value+","+index+","+tunnel+","+ret+"\r\n";
				}
				else{
					out_data += (i+1)+","+time+","+value+","+tunnel+","+ret+"\r\n";
				}
			}	
		}
		else
		{
			document.getElementById("detail_table_desc").innerHTML = "該搜尋時段沒有資料";
			document.getElementById("sensor_remark").innerHTML = "標註:" + current_sensor.remark;

			sensor_report_data.uuid = sensor_uuid;
			sensor_report_data.sensor_name = name;
			sensor_report_data.sensor_type = sensor_type;
			sensor_report_data.results = [];
			sensor_report_data.start = start;
			sensor_report_data.end = end;
			sensor_report_data.unit = unit;
			sensor_report_data.alert_limit = sensor_item.alert_limit;

			var tmp_s = {};
			var tmp_e = {};

			if(sensor_type == WaterPressure) 
			{ 
				tmp_s = { time: (start+1000), water_stage: { value: 0} };
				tmp_e = { time: (end-1000),   water_stage: { value: 0} };

				sensor_report_data.results.push(tmp_s);
				sensor_report_data.results.push(tmp_e);

				ShowSensorReport(sensor_report_data); 
			}
			else
			{ 
				if(sensor_type == Acceleration)
				{ 
					tmp_s = { time: start, acceleration: { value: 0} };
					tmp_e = { time: end,   acceleration: { value: 0} };
				}
				else if(sensor_type == Inclinometer)
				{
					tmp_s = { time: start, inclinometer: { value: 0} };
					tmp_e = { time: end,   inclinometer: { value: 0} };
				}
				else if(sensor_type == Earthquake)		
				{
					tmp_s = { time: start, earthquake: {mmi_level:{ value: 0}, axis_compose:{ value: 0}, axis_x:{ value: 0}, axis_y:{ value: 0}, axis_z:{ value: 0} } };
					tmp_e = { time: end,   earthquake: {mmi_level:{ value: 0}, axis_compose:{ value: 0}, axis_x:{ value: 0}, axis_y:{ value: 0}, axis_z:{ value: 0} } };
				}

				sensor_report_data.results.push(tmp_s);
				sensor_report_data.results.push(tmp_e);

				ShowSensorReport(sensor_report_data); 
			}
		}

		//-------------------------------------------------------

		document.getElementById("btn_ExecOutport").onclick = function()
		{
			var _download = document.getElementById("btn_ExecOutport");		
			var _file_ext = document.getElementById("radioItem_CSV").checked ? ".csv" : ".txt";
			var _blob = new Blob(["\uFEFF" + out_data], {type: "text/plain;charset=utf-8"});
			//var _file_name = sensor_uuid+"_"+sensor_suid+"_sensor_history_data_"+start+"~"+end+_file_ext;
			var _file_name = name+"_"+TimestampToDate2(start)+"_"+TimestampToDate2(end)+_file_ext;
			
			if(window.navigator && window.navigator.msSaveOrOpenBlob) 
			{
				window.navigator.msSaveOrOpenBlob(_blob, _file_name);
			}
			else
			{
				_download.href = URL.createObjectURL(_blob);
				_download.download = _file_name;
			}
			
			$("#m_modal_download").modal('toggle');
		};

		//------------------------------------------
	}
	catch(e){console.log(e); alert(e); }
}
//-----
function GetTDValue(data)
{
	try
	{
		if(data != null) 
		{
			return "<td>" + ((data.value == null) ? "--" : data.value) +"</td>";
		}
	}
	catch(e){console.log(e);}

	return "<td>--</td>";
}
//-----
function HistoryData(sensor_type, data_array)
{
	try
	{
		var table_list = document.getElementById("history_table_list");
		table_list.innerHTML = "";

		for(var i = 0; i < data_array.length && i < 500; i++) 
		{
			var data = data_array[i];

			var str = "";
			str += "<tr>";
			str += "<td>"+ (i+1) +"</td>";
			str += "<td>"+ ((data.time == null) ? "--" : TimestampToDate(data.time, true).replace(/_/g, " ")) +"</td>";

			if(sensor_type == WaterPressure)     { str += GetTDValue(data.water_stage); }
			else if(sensor_type == Acceleration) { str += GetTDValue(data.acceleration); }
			else if(sensor_type == Inclinometer) { str += GetTDValue(data.inclinometer); }
			else if(sensor_type == Earthquake) 
			{
				var earthquake = data.earthquake;
				if(earthquake != null)
				{
					str += GetTDValue(earthquake.axis_x);
					str += GetTDValue(earthquake.axis_y);
					str += GetTDValue(earthquake.axis_z);
					str += GetTDValue(earthquake.axis_compose);
					str += GetTDValue(earthquake.mmi_level); 
				}
			}
			str += "</tr>";

			table_list.innerHTML += str;
		}
	}
	catch(e){console.log(e);}
}
//-----
function ShowReportChart()
{
	try
	{
		var isMobile = false; 

		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

		var sensor_name = sensor_report_data.sensor_name;

		document.getElementById("report_title").innerHTML = sensor_name + "資料圖表";

		//var index = 1;
		//if(document.getElementById("three_hours").checked == true)	index = 0;
		//if(document.getElementById("six_hours").checked == true)	index = 1;
		//if(document.getElementById("twelve_hours").checked == true)	index = 2;

		var show_item_chart_innerHTML = document.getElementById("show_item_chart").innerHTML;
		document.getElementById("modal_sensor_report").innerHTML = show_item_chart_innerHTML;
		document.getElementById("show_item_chart").innerHTML = "";
		document.getElementById("sensor_report").innerHTML = "";
		document.getElementById("chart_view").style.display = "none";

		if(isMobile == false)
			document.getElementById("sensor_report").style.height = "480px";

		//if(index == 0) document.getElementById("three_hours").checked = true;
		//if(index == 1) document.getElementById("six_hours").checked = true;
		//if(index == 2) document.getElementById("twelve_hours").checked = true;

		//--------------------------------------------------------------

		setTimeout(function()
		{
			ShowSensorReport(sensor_report_data);

		}, 300);
	}
	catch(e){console.log(e);}
}
//-----
function CloseReportChart()
{
	try
	{
		if(document.getElementById("modal_sensor_report").innerHTML == "") {
			return;
		}

		var modal_sensor_report = document.getElementById("modal_sensor_report").innerHTML;
		document.getElementById("show_item_chart").innerHTML = modal_sensor_report;
		document.getElementById("modal_sensor_report").innerHTML = "";
		document.getElementById("sensor_report").innerHTML = "";
		document.getElementById("chart_view").style.display = "";
		document.getElementById("sensor_report").style.height = "250px";

		//--------------------------------------------------------------

		setTimeout(function()
		{
			ShowSensorReport(sensor_report_data);

		}, 300);
	}
	catch(e){console.log(e);}
}
//-----
function ShowSensorReport(report_data)
{
	try
	{
		//document.getElementById("hours_radio_group").style.display = "none";

		var sensor_type = report_data.sensor_type;
		var data_array = report_data.results;
		var unit = report_data.unit;
		var alert_limit = report_data.alert_limit;
		var title_array = [];

		if(sensor_type == WaterPressure)      { title_array = ["水位高度"];   }
		else if(sensor_type == Acceleration)  { title_array = ["加速度"];   }
		else if(sensor_type == Inclinometer)  { title_array = ["傾斜度"];   }
		else if(sensor_type == Earthquake)    { title_array = ["X軸", "Y軸", "Z軸", "三軸", "震度"]; }

		d3.select("#sensor_report").selectAll("*").remove();

		var data = ParseData(sensor_type, title_array, data_array, alert_limit);

		nv.addGraph(function()
		{
			var chart = nv.models.lineChart();
			chart.useInteractiveGuideline(true);
			chart.showLegend(true);
			chart.showYAxis(true);
			chart.showXAxis(true);
			chart.options
			({
				transitionDuration: 0,
				useInteractiveGuideline: true
			});
			chart.margin({right: 50});

			chart.xAxis.tickFormat(function(d) {
				return d3.time.format("%m/%d %X")(new Date(d))
			});

			chart.yAxis.tickFormat(d3.format(".4f"));

			chart.yAxis.axisLabel(unit);
			chart.yAxis.staggerLabels(false);

			chart.lines.padData(false);

			d3.select("#sensor_report").append('svg').datum(data).call(chart);
			nv.utils.windowResize(chart.update);

			return chart;
		});

	}
	catch(e){console.log(e);}
}
//-----
function ParseData(type, title_array, data_array, alert_limit)
{
	try
	{
		var results = null;

		if(data_array != null)
		{
			var key = "";
			if(type == WaterPressure)		key = "water_stage";
			else if(type == Acceleration)	key = "acceleration";
			else if(type == Inclinometer)	key = "inclinometer";
			else if(type == Earthquake)	    key = "earthquake";

			if(type == WaterPressure || type == Acceleration || type == Inclinometer)
			{
				var result = [];

				var tmp = (data_array.length >= 500) ? 500 - 1 : data_array.length -1;

				var tmp_x = null;
				var first_x = null;
				var last_x = null;

				for(var i = tmp; i >= 0; i--)
				{
					var data = data_array[i];
					var value = data[key];
					var time = data.time;

					if(time != null)
					{
						result.push({x: data.time, y: value.value});

						if(first_x == null) { 
							first_x = parseInt(data.time);
						}
						tmp_x = parseInt(data.time);
					}
				}

				if(last_x == null) { last_x = tmp_x; }

				//---------------------------------
				results = [
					{
						key: title_array[0],
						values: result,
						color: "#97bbcd"
					}
				];
			}
			else if(type == Earthquake)
			{
				var result_x = [];
				var result_y = [];
				var result_z = [];
				var result_c = [];
				var result_m = [];

				var tmp = (data_array.length >= 500) ? 500 - 1 : data_array.length -1;

				for(var i = tmp; i >= 0; i--)
				{
					var data = data_array[i];
					var value = data[key];
					var time = data.time;

					var value_x = value["axis_x"];
					var value_y = value["axis_y"];
					var value_z = value["axis_z"];
					var value_c = value["axis_compose"];
					var value_m = value["mmi_level"];

					if(time != null)
					{
						result_x.push({x: time, y: value_x.value});
						result_y.push({x: time, y: value_y.value});
						result_z.push({x: time, y: value_z.value});
						result_c.push({x: time, y: value_c.value});
						result_m.push({x: time, y: value_m.value});
					}
				}

				//---------------------------------

				results = [
					{
						key: title_array[0],
						values: result_x,
						color: "#97bbcd",
					},
					{
						key: title_array[1],
						values: result_y,
						color: "#ff7f0e",
					},
					{
						key: title_array[2],
						values: result_z,
						color: "#2ca02c",
					},
					{
						key: title_array[3],
						values: result_c,
						color: "#7777ff",
					},
					{
						key: title_array[4],
						values: result_m,
						color: "#77ff77",
					}
				];
			}
		}

		return results;
	}
	catch(e){console.log(e);}
	return null;
}
//-----
function ShowWaterPressureReport(report_data)
{
	try
	{
		var index = 1;
		document.getElementById("hours_radio_group").style.display = "";

		if(document.getElementById("three_hours").checked == true)	index = 0;
		if(document.getElementById("six_hours").checked == true)	index = 1;
		if(document.getElementById("twelve_hours").checked == true)	index = 2;

		d3.select("#sensor_report").selectAll("*").remove();

		var data_array = report_data.results;
		var start = report_data.start;
		var end = report_data.end;
		var alert_limit = report_data.alert_limit;
		var unit = report_data.unit;

		water_pressure_report_list = ParseDataWaterPressure(data_array, start, end, alert_limit);

		if(water_pressure_report_list != null)
		{
			var data = water_pressure_report_list[index];

			nv.addGraph(function()
			{
				var chart = nv.models.lineChart();
				chart.useInteractiveGuideline(true);
				chart.showLegend(true);
				chart.showYAxis(true);
				chart.showXAxis(true);

				chart.options
				({
					transitionDuration: 0,
					useInteractiveGuideline: true
				});

				chart.margin({right: 50});

				chart.xAxis.tickFormat(function(d) {
					return d3.time.format("%m/%d %H:%M")(new Date(d))
				});

				chart.yAxis.tickFormat(d3.format(".4f"));

				chart.yAxis.axisLabel(unit);
				chart.yAxis.staggerLabels(false);

				chart.lines.padData(false);

				d3.select("#sensor_report").append('svg').datum(data).call(chart);
				nv.utils.windowResize(chart.update);

				return chart;
			});
		}
	}
	catch(e){console.log(e);}
}
//-----
function ParseDataWaterPressure(data_array, start_time, end_time, alert_limit)
{
	try
	{
		var results = null;
		var results_array = [];
		var hours_array = [3, 6, 12];
		var key = "water_stage";

		var one_hour = 1 * 60 * 60 * 1000;
		var one_day = 24 * one_hour;

		var s_day = new Date(TimestampToDate(start_time, false)).getTime();
		var e_day = new Date(TimestampToDate(end_time, false)).getTime();

		//var low_limit = alert_limit.water_stage.low;
		//var high_limit = alert_limit.water_stage.high;

		if(data_array != null)
		{
			var day_number = ((e_day - s_day) / one_day) + 1;
			var day_array = new Array(day_number);

			for(var i = 0; i < hours_array.length; i++)
			{
				var t_hours = hours_array[i];
				var t_number = 24 / t_hours;

				var tmp_day_index = Math.floor((start_time - s_day) / one_day);
				var tmp_ds_time = s_day + (tmp_day_index * one_day);
				var tmp_ds_hour_index = Math.floor(Math.floor((start_time - tmp_ds_time) / one_hour) / t_hours);
				var tmp_ds_next_time = tmp_ds_time + ((tmp_ds_hour_index + 1) * t_hours * one_hour);

				//------------------------------------------

				var last_data = data_array[0];
				var last_data_time = last_data.time;

				var list_map = {};
				for(var n = 0; n < day_number; n++)
				{
					for(var k = 0; k < t_number; k++)
					{
						var ds_time = s_day + (n * one_day);
						var hs_time = ds_time + (k * t_hours * one_hour);
						hs_time = (hs_time < tmp_ds_next_time) ? start_time : hs_time;

						if(list_map[hs_time] == null && hs_time <= last_data_time)
						{
							var tmp = {};
							tmp.value = 0;
							tmp.tick = 0;
							list_map[hs_time] = tmp;
						}
					}
				}

				//------------------------------------------

				for(var n = data_array.length-1; n > 0; n--)
				{
					var data = data_array[n];
					var value = data[key];

					if(data.time != null)
					{
						var day_index = Math.floor((data.time - s_day) / one_day);
						var ds_time = s_day + (day_index * one_day);
						var hour_index = Math.floor(Math.floor((data.time - ds_time) / one_hour) / t_hours);
						var hs_time = ds_time + (hour_index * t_hours * one_hour);
						hs_time = (hs_time < tmp_ds_next_time) ? start_time : hs_time;
						
						list_map[hs_time].value += value.value;
						list_map[hs_time].tick += 1;
					}
				}

				var data = data_array[0];
				var value = data[key];

				var tmp = {};
				tmp.value = value.value;
				tmp.tick = 1;
				list_map[last_data_time] = tmp;

				//------------------------------------------

				var result = [];
				var tmp_x = "";
				var first_x = null;
				var last_x = null;

				for(var map_key in list_map)
				{
					var item = list_map[map_key];
					var value = item.value;
					var tick = item.tick;
					var time = map_key;
					var avg_value = (tick != 0 ) ? value / tick : 0;
					avg_value = parseFloat(avg_value.toFixed(4));

					result.push({x: parseInt(time), y: avg_value });

					//---------------------------------------------

					if(first_x == null) { first_x = parseInt(time); }

					tmp_x = parseInt(time);
				}

				if(last_x == null) { last_x = tmp_x; }

				//------------------------------------------

				results = [
					{
						key: "水位高度",
						values: result,
						color: "#97bbcd",
					}/*,
					{
						key : "Low",
						values : [ { x: first_x, y: low_limit}, { x: last_x, y: low_limit } ],
						color: "#FF5511"
					},
					{
						key : "High",
						values : [ { x: first_x, y: high_limit}, { x: last_x, y: high_limit} ],
						color: "#FF5511"
					}*/
				];

				results_array.push(results);
			}
		}

		return results_array;
	}
	catch(e){console.log(e);}
	return null;
}
//-----
function ChangeWaterPressureReport()
{
	try
	{
		var index = 1;
		if(document.getElementById("three_hours").checked == true)	index = 0;
		if(document.getElementById("six_hours").checked == true)	index = 1;
		if(document.getElementById("twelve_hours").checked == true)	index = 2;

		var unit = sensor_report_data.unit;

		d3.select("#sensor_report").selectAll("*").remove();

		if(water_pressure_report_list != null)
		{
			var data = water_pressure_report_list[index];

			nv.addGraph(function()
			{
				var chart = nv.models.lineChart();
				chart.useInteractiveGuideline(true);
				chart.showLegend(true);
				chart.showYAxis(true);
				chart.showXAxis(true);
				chart.options
				({
					transitionDuration: 0,
					useInteractiveGuideline: true
				});

				chart.margin({right: 50});

				chart.xAxis.tickFormat(function(d) {
					return d3.time.format("%m/%d %H:%M")(new Date(d))
				});

				chart.yAxis.tickFormat(d3.format(".4f"));

				chart.yAxis.axisLabel(unit);
				chart.yAxis.staggerLabels(false);

				chart.lines.padData(false);

				d3.select('#sensor_report').append('svg').datum(data).call(chart);
				nv.utils.windowResize(chart.update);

				return chart;
			});
		}
	}
	catch(e){console.log(e);}
}
//-----
function SensorsValue(td_value, data)
{
	try
	{
		if(td_value != null && data != null) 
		{
			var status = data.status;
			td_value.style.color = (status != null && (status == "data_warning")) ? "#FF4500" : "";
			td_value.innerHTML = (data.value == null) ? "--" : data.value;
		}
	}
	catch(e){console.log(e);}
}
//-----
function UpdateSensorsValue(ukey, time, data)
{
	try
	{
		if(data != null)
		{
			var td_value = document.getElementById("value_" + ukey);
			var td_value_time = document.getElementById("value_time_" + ukey);

			if(td_value_time != null) { td_value_time.innerHTML = ((time == null) ? "--" : TimestampToDate(time, true).replace(/_/g, " "));  }  	
			if(td_value != null) { SensorsValue(td_value, data); }
		}
	}
	catch(e){console.log(e);}
}
//-----
function UpdateSeismographValue(ukey, time, data)
{
	try
	{
		var td_value_time = document.getElementById("value_time_" + ukey);
		var td_value_x = document.getElementById("value_x_" + ukey);
		var td_value_y = document.getElementById("value_y_" + ukey);
		var td_value_z = document.getElementById("value_z_" + ukey);
		var td_value_c = document.getElementById("value_c_" + ukey);
		var td_value_m = document.getElementById("value_m_" + ukey);

		var axis_x = (data.axis_x != null) ? data.axis_x : {};
		var axis_y = (data.axis_y != null) ? data.axis_y : {};
		var axis_z = (data.axis_z != null) ? data.axis_z : {};
		var axis_c = (data.axis_compose != null) ? data.axis_compose : {};
		var axis_m = (data.mmi_level != null) ? data.mmi_level : {};

		if(td_value_time != null)  td_value_time.innerHTML = ((time == null) ? "--" : TimestampToDate(time, true).replace(/_/g, " ")); 

		if(td_value_x != null) { SensorsValue(td_value_x, axis_x); }
		if(td_value_y != null) { SensorsValue(td_value_y, axis_y); }
		if(td_value_z != null) { SensorsValue(td_value_z, axis_z); }
		if(td_value_c != null) { SensorsValue(td_value_c, axis_c); }
		if(td_value_m != null) { SensorsValue(td_value_m, axis_m); }
	}
	catch(e){console.log(e);}
}
//-----
function UpdateSensorsStatus(sensor)
{
	try
	{
		var ukey = sensor.uuid + "_" + sensor.suid;

		var td_value_time = document.getElementById("value_time_" + ukey);
		var td_value_status = document.getElementById("value_status_" + ukey);
		var td_value = document.getElementById("value_" + ukey);
		var td_value_x = document.getElementById("value_x_" + ukey);
		var td_value_y = document.getElementById("value_y_" + ukey);
		var td_value_z = document.getElementById("value_z_" + ukey);
		var td_value_c = document.getElementById("value_c_" + ukey);
		var td_value_m = document.getElementById("value_m_" + ukey);

		//------------------------------------------------------------------------

		var status_info = sensor.status_info;
		var status = (status_info.status == null) ? "--" : status_info.status;

		var status_str = GetStatusString(status);

		//--------------------------------------------

		if(td_value_status != null)  td_value_status.innerHTML = status_str;

		if(status == "disconnect" || status == "timeout" || status == "pending" || status == "connecting") 
		{
			if(td_value_time != null) 	 td_value_time.innerHTML = (status_info.time == null) ? "--" : TimestampToDate(status_info.time, true).replace(/_/g, " "); 
			if(td_value != null)   { td_value.style.color = ""; td_value.innerHTML = "--"; }
			if(td_value_x != null) { td_value_x.style.color = ""; td_value_x.innerHTML = "--"; }
			if(td_value_y != null) { td_value_y.style.color = ""; td_value_y.innerHTML = "--"; }
			if(td_value_z != null) { td_value_z.style.color = ""; td_value_z.innerHTML = "--"; }
			if(td_value_c != null) { td_value_c.style.color = ""; td_value_c.innerHTML = "--"; }
			if(td_value_m != null) { td_value_m.style.color = ""; td_value_m.innerHTML = "--"; }
		}

		//--------------------------------------------
	}
	catch(e){console.log(e);}
}
//-----
function UpdateGatewayStatus(gateway)
{
	try
	{
		var ukey = gateway.uuid + "_" + gateway.suid;

		var td_base_status = document.getElementById("base_status_" + ukey);
		var td_lora_status = document.getElementById("lora_status_" + ukey);

		//------------------------------------------------------------------------

		var status_info = gateway.status_info;
		var base_tunnel = (status_info.base_tunnel == null) ?　{} : status_info.base_tunnel;
		var lora_tunnel = (status_info.lora_tunnel == null) ?　{} : status_info.lora_tunnel;
		var base_status = (base_tunnel.status == null) ? "--" : base_tunnel.status;
		var lora_status = (lora_tunnel.status == null) ? "--" : lora_tunnel.status;
		var base_status_str = GetStatusString(base_status);
		var lora_status_str = GetStatusString(lora_status);

		if(td_base_status != null)  td_base_status.innerHTML = base_status_str;
		if(td_lora_status != null)  td_lora_status.innerHTML = lora_status_str;

		//--------------------------------------------
	}
	catch(e){console.log(e);}
}
//-----
function GetStatusString(status)
{    		
	try
	{
		var status_str = "--";	
		if(status == "normal")		status_str = "正常";
		if(status == "error")		status_str = "設備異常";
		if(status == "data_error")	status_str = "數據錯誤";
		if(status == "data_warning")status_str = "超出安全值";
		if(status == "disconnect")	status_str = "斷線";
		if(status == "timeout")		status_str = "斷線";  //逾時
		if(status == "connecting")	status_str = "連線中";
		if(status == "pending")		status_str = "等待連線";

		return status_str;
	}
	catch(e){console.log(e);}
}
//-----
function OnSensorsData(payload)
{    		
	try
	{
		var payload = JSON.parse(payload.data);
		var data = payload.data == null ? payload : payload.data;
		var value = data.values != null ? data.values[0] : data.results[0];
		var realtime = (data.flag == null) ? true : false;

		if(data.uuid != null && data.uuid.length > 0 && data.suid != null && data.suid.length > 0 && value != null)
		{
			var uuid = data.uuid;
			var suid = data.suid;
			var event_type = (suid.indexOf("status") != -1) ? "status" : "value";
			if(event_type == "status") { suid = suid.split("status-")[1]; }

			if(data.uuid == "Gateway")
			{
				var gateway = Api_GetGatewayData(suid);

				if(gateway != null)
				{
					if(event_type == "status") 
					{
						var flag = true;

						if(last_gw_status_event[suid] != null)
						{
							var last_gw_event_time = last_gw_status_event[suid].time;
							if(last_gw_event_time == value.time) { flag = false; }
						}

						//-----------------------------------------------------------

						if(flag)
						{
							gateway.status_info = value;

							if(realtime == true)
							{ 
								value.dev_name = gateway.name;
								value.dev_type = gateway.type_name;
								value.type = gateway.type;

								last_gw_status_event[suid] = value;

								//------------------------------------------

								var base_tunnel = value.base_tunnel == null ? {} : value.base_tunnel;
								var lora_tunnel = value.lora_tunnel == null ? {} : value.lora_tunnel;
								var base_status = base_tunnel.status == null ? "--" : base_tunnel.status;
								var lora_status = lora_tunnel.status == null ? "--" : lora_tunnel.status;
								var base_time = base_tunnel.status_time;
								var lora_time = lora_tunnel.status_time;

								base_status = (base_status == "timeout") ? "disconnect" : base_status;
								lora_status = (lora_status == "timeout") ? "disconnect" : lora_status;

								base_tunnel.status = base_status;
								lora_tunnel.status = lora_status;

								var base_type_flag1 = ((base_status == "disconnect") || (base_status  == "error")) ? true : false;
								var lora_type_flag1 = ((lora_status == "disconnect") || (lora_status  == "error")) ? true : false;

								if(base_time != null && base_type_flag1 == true)
								{
									var tmp = {};
									tmp.dev_name = gateway.name;
									tmp.dev_type = gateway.type_name;
									tmp.type = gateway.type;
									tmp.time = base_time;
									tmp.base_tunnel = base_tunnel;

									day_warning.warning_list.splice(0, 0, tmp);
									day_warning.count_data[base_status]++;
								}

								if(lora_time != null && lora_type_flag1 == true)
								{
									var tmp = {};
									tmp.dev_name = gateway.name;
									tmp.dev_type = gateway.type_name;
									tmp.type = gateway.type;
									tmp.time = lora_time;
									tmp.lora_tunnel = lora_tunnel;

									day_warning.warning_list.splice(0, 0, tmp);
									day_warning.count_data[lora_status]++;
								}

								//------------------------------------------

								var disconnect_count = day_warning.count_data["disconnect"];
								//var data_error_count = day_warning.count_data["data_error"];
								var data_warning_count = day_warning.count_data["data_warning"];
								//var day_warning_count = disconnect_count + data_error_count + data_warning_count;
								var day_warning_count = disconnect_count + data_warning_count;

								if(day_warning_count == 0){
									document.getElementById("day_warning_count").style.display = "none";
								}
								else if(day_warning_count > 0){
									document.getElementById("day_warning_count").style.display = "";
									document.getElementById("day_warning_count").innerHTML = day_warning_count;
								}

								ShowDayWarningList();
							}
						}
					}
				}

				UpdateGatewayStatus(gateway);
			}
			else if(data.uuid == "Sensor" && value.retransmission == false)
			{
				var sensor_type = CheckSensorType(suid);
				var sensor = Api_GetSensorData(suid);

				if(sensor != null)
				{
					if(event_type == "status") 
					{
						var flag = true;

						if(last_status_event[suid] != null)
						{
							var last_event_time = last_status_event[suid].time;
							var last_sensor_type = last_status_event[suid].sensor_type;

							if(last_event_time == value.time) {
								flag = false;
							}
						}

						//-----------------------------------------------------------

						if(flag)
						{
							value.status = (value.status == "timeout") ? "disconnect" : value.status;

							sensor.status_info = value;

							if(realtime == true)
							{ 
								value.dev_name = sensor.name;
								value.dev_type = sensor.type_name;
								value.type = sensor.type;
								value.sensor_type = sensor_type;
								value.event_type = event_type;

								last_status_event[suid] = value; 

								day_warning.count_data[value.status]++;
								day_warning.warning_list.splice(0, 0, value);

								var disconnect_count = day_warning.count_data["disconnect"];
								//var data_error_count = day_warning.count_data["data_error"];
								var data_warning_count = day_warning.count_data["data_warning"];
								//var day_warning_count = disconnect_count + data_error_count + data_warning_count;
								var day_warning_count = disconnect_count + data_warning_count;
								document.getElementById("day_warning_count").innerHTML = day_warning_count;

								ShowDayWarningList();
							}

							//-----------------------------------------------------------
						}
					}
					else if(event_type == "value") 
					{
						value.dev_name = sensor.name;
						value.dev_type = sensor.type_name;
						value.type = sensor.type;
						value.sensor_type = sensor_type;
						value.event_type = event_type;

						if(realtime == true) { last_value_event[suid] = value; }

						var ukey = sensor.uuid + "_" + sensor.suid;

						switch(sensor_type)
						{
							case WaterPressure:
								if(value.water_stage != null) { UpdateSensorsValue(ukey, value.time, value.water_stage); }
								break;
							case Acceleration:
								if(value.acceleration != null) { UpdateSensorsValue(ukey, value.time, value.acceleration); }
								break;
							case Inclinometer:
								if(value.inclinometer != null) { UpdateSensorsValue(ukey, value.time, value.inclinometer); }
								break;
							case Earthquake:
								if(value.earthquake != null) { UpdateSeismographValue(ukey, value.time, value.earthquake); }
								break;
						}
					}
				}

				//------------------------------------------------------

				UpdateSensorsStatus(sensor);
			}
		}
	}
	catch(e){console.log(e);}
}
//-----
function SensorGetLastData(uuid, suid, count)
{
	try
	{
		var resp = Api_GetCloudLastData(uuid, suid, count);

		if(resp != null && resp.length > 0) 
		{ 
			resp = JSON.parse(resp);
			resp.uuid = uuid;
			resp.suid = suid;
			resp.flag = false;

			var content = {};
			content.data = JSON.stringify(resp); 

			OnSensorsData(content);
		}
	}
	catch(e){console.log(e);}
}
//-----
function SubscribeData()
{
	try
	{
		Api_SubscribeData('*', OnSensorsData);

		//_User.SubscribeData('*', OnSensorsData);
	}
	catch(e){console.log(e);}
}

function OnSensorsEvent(payload)
{
	try
	{
		var payload = JSON.parse(payload.data);
		var data = payload.data == null ? payload : payload.data;
		var value = data.values != null ? data.values[0] : data.results[0];
		var realtime = (data.flag == null) ? true : false;

		if(data.uuid != null && data.uuid.length > 0 && data.suid != null && data.suid.length > 0 && value != null)
		{
			var uuid = data.uuid;
			var suid = data.suid;
		}
	}
	catch(e){console.log(e);}
}
//-----
function SubscribeEvent()
{
	try
	{
		Api_SubscribeEvent("*", OnSensorsEvent);

	}
	catch(e){console.log(e);}
}
//-----
function TimestampToDate(timestamp, flag)
{
	try
	{
		var _datetime = new Date(timestamp);

		var _year = _datetime.getFullYear();
		var _month = _datetime.getMonth() + 1;
		var _date = _datetime.getDate();
		var _hour = _datetime.getHours();
		var _min  = _datetime.getMinutes();
		var _sec  = _datetime.getSeconds();
		var _millisec  = _datetime.getMilliseconds();
		
		var _yearStr  = _datetime.getFullYear();
		var _monthStr = ( _month < 10) ? ('0'+ _month) : ('' + _month);
		var _dateStr  = ( _date < 10)  ? ('0'+ _date)  : ('' + _date);
		var _hourStr  = ( _hour < 10)  ? ('0'+ _hour)  : ('' + _hour);
		var _minStr   = ( _min < 10)   ? ('0'+ _min)   : ('' + _min);
		var _secStr   = ( _sec < 10)   ? ('0'+ _sec)   : ('' + _sec);

		var _datetimeStr = "";

		if(flag == false)
			_datetimeStr = _yearStr + '/' + _monthStr +  '/' + _dateStr;
		else if(flag == true)
			_datetimeStr = _yearStr + '/' + _monthStr +  '/' + _dateStr  + '_' + _hourStr + ':' + _minStr + ':' + _secStr;

		return _datetimeStr;
	}
	catch(e){console.log(e);}

	return null;
}
//-----
function TimestampToDate2(timestamp)
{
	try
	{
		var _datetime = new Date(timestamp);

		var _year = _datetime.getFullYear();
		var _month = _datetime.getMonth() + 1;
		var _date = _datetime.getDate();
		var _hour = _datetime.getHours();
		var _min  = _datetime.getMinutes();
		var _sec  = _datetime.getSeconds();

		var _yearStr  = _datetime.getFullYear();
		var _monthStr = ( _month < 10) ? ('0'+ _month) : ('' + _month);
		var _dateStr  = ( _date < 10)  ? ('0'+ _date)  : ('' + _date);
		var _hourStr  = ( _hour < 10)  ? ('0'+ _hour)  : ('' + _hour);
		var _minStr   = ( _min < 10)   ? ('0'+ _min)   : ('' + _min);
		var _secStr   = ( _sec < 10)   ? ('0'+ _sec)   : ('' + _sec);

		var _datetimeStr = _yearStr + '' + _monthStr +  '' + _dateStr  + '' + _hourStr + '' + _minStr + '' + _secStr;

		return _datetimeStr;
	}
	catch(e){console.log(e);}

	return null;
}
//-----
function TimestampToDate3(timestamp)
{
	try
	{
		var _datetime = new Date(timestamp);

		var _year = _datetime.getFullYear();
		var _month = _datetime.getMonth() + 1;
		var _date = _datetime.getDate();
		var _hour = _datetime.getHours();
		var _min  = _datetime.getMinutes();
		var _sec  = _datetime.getSeconds();
		var _millisec  = _datetime.getMilliseconds();
		
		var _yearStr  = _datetime.getFullYear();
		var _monthStr = ( _month < 10) ? ('0'+ _month) : ('' + _month);
		var _dateStr  = ( _date < 10)  ? ('0'+ _date)  : ('' + _date);
		var _hourStr  = ( _hour < 10)  ? ('0'+ _hour)  : ('' + _hour);
		var _minStr   = ( _min < 10)   ? ('0'+ _min)   : ('' + _min);
		var _secStr   = ( _sec < 10)   ? ('0'+ _sec)   : ('' + _sec);
		var _millisecStr  = ( _millisec < 100)  ? ('0'+ _millisec)   : ('' + _millisec);

		if(_millisec < 100 && _millisecStr > 10)	{_millisecStr = '0'+ _millisec;}
		else if(_millisecStr < 10) {_millisecStr = '00'+ _millisec;}
		else {_millisecStr = _millisec;}

		var _datetimeStr = _yearStr + '/' + _monthStr +  '/' + _dateStr  + '_' + _hourStr + ':' + _minStr + ':' + _secStr + '.' + _millisecStr;

		return _datetimeStr;
	}
	catch(e){console.log(e);}

	return null;
}
//-----
function CheckSensorType(suid) 
{
	try
	{
		var sensor_type = "0";
		if(suid.indexOf(WaterPressure) != -1) 	  sensor_type = WaterPressure;
		else if(suid.indexOf(Acceleration) != -1) sensor_type = Acceleration;
		else if(suid.indexOf(Inclinometer) != -1) sensor_type = Inclinometer;
		else if(suid.indexOf(Earthquake) != -1)   sensor_type = Earthquake;

		return sensor_type;
	}
	catch(e){console.log(e);}

	return null;
}
//-----
function InitTable(table_id) 
{
	try
	{
		var table = "#" + table_id;

		$(table).DataTable({
			scrollCollapse: true,
			scrollX: true,
			retrieve: true,
			destroy: true,

			//== DOM Layout settings-筆數
			dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",

			lengthMenu: [5, 10, 20, 30, 50],
			pageLength: 50,
			language: {
			    "loadingRecords": "載入中...",
			    'lengthMenu': '資料筆數 _MENU_',
			    "zeroRecords":  "沒有符合的結果",
			    "info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
			    "infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
			    "infoFiltered": "(從 _MAX_ 項結果中過濾)",
			    "infoPostFix":  "",
			    "search":       "搜尋:",
			    /*"paginate": {
			        "first":    "第一頁",
			        "previous": "上一頁",
			        "next":     "下一頁",
			        "last":     "最後一頁"
			    },*/
			    "aria": {
			        "sortAscending":  ": 升冪排列",
			        "sortDescending": ": 降冪排列"
			    }
			}
		});

		$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
			$($.fn.dataTable.tables(true)).DataTable().columns.adjust();
		});
	}
	catch(e){console.log(e);}
}
//-----
function DestroyTable(table_id) 
{
	try
	{
		var table = "#" + table_id;
		if($.fn.DataTable.isDataTable(table)) 
		{
		  $(table).DataTable().destroy();

		  $(table_id + ' tbody').empty();
		}
	}
	catch(e){console.log(e);}
}
//-----
function CheckDateTimeRange1()
{
	try
	{
		var m_datepicker_1 = document.getElementById("m_datepicker_1");
		var m_datepicker_2 = document.getElementById("m_datepicker_2");
		var m_timepicker_1 = document.getElementById("m_timepicker_1");
		var m_timepicker_2 = document.getElementById("m_timepicker_2");

		if(m_datepicker_1.time_range_flag == true) { return; }

		m_datepicker_1time_range_flag = true;

		var s_date = (m_datepicker_1 != null) ? $("#m_datepicker_1").val() : "";
		var e_date = (m_datepicker_2 != null) ? $("#m_datepicker_2").val() : "";
		var s_time = (m_timepicker_1 != null) ? $('#m_timepicker_1').val() + ":00.000" : "";
		var e_time = (m_timepicker_2 != null) ? $('#m_timepicker_2').val() + ":59.999" : "";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

		if(start > end)
		{
			if(m_datepicker_2 != null) { $('#m_datepicker_2').datepicker('update', s_date); }
			if(m_timepicker_2 != null) { $('#m_timepicker_2').timepicker('setTime', s_time); }
		}

		m_datepicker_1.time_range_flag = false;

	}
	catch(e){console.log(e);}
}
//-----
function CheckDateTimeRange2()
{
	try
	{
		var m_datepicker_1 = document.getElementById("m_datepicker_1");
		var m_datepicker_2 = document.getElementById("m_datepicker_2");
		var m_timepicker_1 = document.getElementById("m_timepicker_1");
		var m_timepicker_2 = document.getElementById("m_timepicker_2");

		if(m_datepicker_1.time_range_flag == true) { return; }

		document.getElementById("m_datepicker_1").time_range_flag = true;

		var s_date = (m_datepicker_1 != null) ? $("#m_datepicker_1").val() : "";
		var e_date = (m_datepicker_2 != null) ? $("#m_datepicker_2").val() : "";
		var s_time = (m_timepicker_1 != null) ? $('#m_timepicker_1').val() + ":00.000" : "";
		var e_time = (m_timepicker_2 != null) ? $('#m_timepicker_2').val() + ":59.999" : "";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

		if(start > end)
		{
			if(m_datepicker_1 != null) { $('#m_datepicker_1').datepicker('update', e_date); }
			if(m_timepicker_1 != null) { $('#m_timepicker_1').timepicker('setTime', e_time); }
		}

		document.getElementById("m_datepicker_1").time_range_flag = false;

	}
	catch(e){console.log(e);}
}
//-----