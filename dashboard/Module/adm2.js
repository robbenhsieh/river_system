
var StationSet =[];
var station_info = null;
function adm_Navigation(){
  //------------------dropdownList-------------------------------
    var ZoneList =[{text:"淡水區",value:"1"},{text:"北投區",value:"2"},{text:"蘆洲區",value:"3"}];
  	var select_group = document.getElementById("select_Group");
  	var zone_select  = createSelect(ZoneList);
  	var zone_Nav     = createNav("zone_Nav","區域:",zone_select);
  	    select_group.appendChild(zone_Nav);
  			//zone_Nav.style.display = "none";
  //-------------------------------------------------------------
    var RiverList =[{text:"淡水河",value:"1"},{text:"三峽河",value:"2"}];
    var river_select  = createSelect(RiverList);
    var river_Nav     = createNav("river_Nav","河川別:",river_select);
  		  select_group.appendChild(river_Nav);
  			//river_Nav.style.display = "none";
  //--------------------------------------------------------------
    var SideList =[{text:"左岸",value:"1"},{text:"右岸",value:"2"}];
    var side_select  = createSelect(SideList);
    var side_Nav     = createNav("side_Nav","岸別:",side_select);
  		  select_group.appendChild(side_Nav);
  			//side_Nav.style.display = "none";
}
//----------------------------------------------------------------------
function modal_cleanStationInfo(){
  station_info = null;
}
//----------------------------------------------------------------------
function modal_getStationInfo(Station_info){

  document.getElementById("modal_Station_Tittle_name").innerHTML = Station_info.Name;
  document.getElementById("modal_station_name").value = Station_info.Name;
  document.getElementById("modal_station_zone").value = Station_info.Zone;
  document.getElementById("modal_station_admin").value = Station_info.AdminUnit;
  document.getElementById("modal_station_river").value = Station_info.River;
  document.getElementById("modal_station_side").value = Station_info.RiverSide;
  document.getElementById("modal_station_address").value = Station_info.Address;
  document.getElementById("modal_station_person").value = Station_info.StationPerson;
  document.getElementById("modal_station_X").value = Station_info.CoordinateX;
  document.getElementById("modal_station_Y").value = Station_info.CoordinateY;
  document.getElementById("modal_station_id").value = Station_info.id;
  document.getElementById("modal_station_ip").value = Station_info.IP;
  document.getElementById("modal_station_update").addEventListener("click", function(){adm_UpdateStationList()});
  document.getElementById("modal_station_close").addEventListener("click", function(){modal_cleanStationInfo()});
  document.getElementById("modal_station_cancel").addEventListener("click", function(){modal_cleanStationInfo()});
}
//----------------------------------------------------------------------
function showStationEdit(button){

  var id = button.id;
	var start = id.indexOf("_");
	var stationId = id.substring(start+1,id.length);
	if(StationSet == null || !checkListByID(StationSet,stationId)){
       var list = queryStations("G-300-0-0",stationId);
           //station_info = list[0];
					 StationSet.push(list[0]);
}
           station_info = queryListByID(StationSet,stationId);
           modal_getStationInfo(station_info);

  //Station_info =null;
}
//----------------------------------------------------------------------
function adm_getStationList(station_list){
   //var station_list = queryByRows("River-Management.Station",5);

	 if(station_list != null && station_list.length > 0)
	 {
		 for (var i in station_list) {
			 if(station_list[i] != null){
         /*var button_edit = document.getElementById("edit_"+station_list[i].ukey);
             button_edit.addEventListener('click', function(){showStationEdit(station_list[i].Name);});*/
            StationSet.push(station_list[i]);
            document.getElementById("id_"+station_list[i].id).innerHTML = station_list[i].id;
         		document.getElementById("name_"+station_list[i].id).innerHTML = station_list[i].Name;
         		document.getElementById("zone_"+station_list[i].id).innerHTML = station_list[i].Zone;
         		document.getElementById("admin_"+station_list[i].id).innerHTML = station_list[i].AdminUnit;
         		document.getElementById("river_"+station_list[i].id).innerHTML = station_list[i].River;
         		document.getElementById("side_"+station_list[i].id).innerHTML = station_list[i].RiverSide;
         		document.getElementById("person_"+station_list[i].id).innerHTML = station_list[i].StationPerson;
         		document.getElementById("X_"+station_list[i].id).innerHTML = station_list[i].CoordinateX;
         		document.getElementById("Y_"+station_list[i].id).innerHTML = station_list[i].CoordinateY;
         		document.getElementById("address_"+station_list[i].id).innerHTML = station_list[i].Address;
         		document.getElementById("Ip_"+station_list[i].id).innerHTML = station_list[i].IP;
         		document.getElementById("edit_"+station_list[i].id).addEventListener("click", function(){showStationEdit(this)});

	    }
	  }
	}
}
//----------------------------------------------------------------------------------------------------
function adm_UpdateStationList(){
  if(station_info==null || station_info.id!=document.getElementById("modal_station_id").value){
    var list = queryStations("G-300-0-0",document.getElementById("modal_station_id").value);
        station_info = list[0];
  }
  station_info.Name = document.getElementById("modal_station_name").value;
  station_info.Zone = document.getElementById("modal_station_zone").value;
  station_info.AdminUnit = document.getElementById("modal_station_admin").value;
  station_info.River = document.getElementById("modal_station_river").value;
  station_info.RiverSide = document.getElementById("modal_station_side").value;
  station_info.Address = document.getElementById("modal_station_address").value;
  station_info.StationPerson = document.getElementById("modal_station_person").value;
  station_info.CoordinateX = document.getElementById("modal_station_X").value;
  station_info.CoordinateY = document.getElementById("modal_station_Y").value;
  var done = updateBykey("G-300-0-0."+station_info.id,station_info.ukey,station_info);
  if(done){
  var _list = queryStations("G-300-0-0",station_info.id);
      adm_getStationList(_list);
      modal_getStationInfo(_list[0]);
    }

}
//----------------------------------------------------------------------------------------------------
function adm_getDeviceList(list){
	if(list != null && list.length > 0)
	{
		for (var i in list) {
			 if(list[i] != null){
				  if(checkDeviceType(list[i],"43")){
      document.getElementById("id_"+list[i].suid).innerHTML = list[i].suid;
      document.getElementById("name_"+list[i].suid).innerHTML = list[i].name_c;
      document.getElementById("StationName_"+list[i].suid).innerHTML = list[i].stationName;
      document.getElementById("X_97_"+list[i].suid).innerHTML = list[i].X_97;
      document.getElementById("Y_97_"+list[i].suid).innerHTML = list[i].Y_97;
      document.getElementById("edit_"+list[i].suid).addEventListener("click",function(){document.getElementById("modal_setup_01_name").innerHTML =list[i].name_c ;});

        //}
				/*else if(list[i].Type == "46"){
    var button_string ="<a href=\"#\" class=\"btn btn-outline-accent m-btn m-btn--icon m-btn--pill action_btn\"  data-toggle=\"modal\" data-target=\"#modal_setup_01\"><span><i class=\"flaticon-edit\"></i><span>設定</span></span></a>";
  document.getElementById("edit_"+list[i].ukey).innerHTML = button_string;
  var button_edit = document.getElementById("edit_"+list[i].ukey).childNodes[0];
      button_edit.addEventListener('click', function(){showStationEdit(list[i].Name);});
	 document.getElementById("id_"+list[i].ukey).innerHTML = list[i].ukey;
	 document.getElementById("name_"+list[i].ukey).innerHTML = list[i].Name;
	 document.getElementById("StationName_"+list[i].ukey).innerHTML = list[i].GatewayId;
	 document.getElementById("X_"+list[i].ukey).innerHTML = list[i].CoordinateX;
	 document.getElementById("Y_"+list[i].ukey).innerHTML = list[i].CoordinateY;
 }*/
        }
      }
    }
  }
}
//-----------------------------------------------------------------------------------------------------------
function adm_initialTable(){
				      var adm_station_table = document.getElementById("adm_station_table");
//--------------------------stationTable--------------------------------------------------------------------
              var station_list = queryStations("G-300-0-0","Gateway000001");
							var stationTable = _stationTable(station_list,"adm_station");
              var button_string ="<a href=\"#\" class=\"btn btn-outline-accent m-btn m-btn--icon m-btn--pill action_btn\"  data-toggle=\"modal\" data-target=\"#modal_setup_01\"><span><i class=\"flaticon-edit\"></i><span>設定</span></span></a>";
                  addColumns(stationTable,"設定",button_string,"edit");
                  adm_station_table.appendChild(stationTable);
									adm_getStationList(station_list);
//-------------------------WaterTable-------------------------------------------------------------------------------------
              var adm_device_table = document.getElementById("adm_device_table");

              var deviceList = queryDevices("G-300-0-0","Gateway000001");
              var waterTable = _waterTable(deviceList,"adm_water_table");
              var button_string ="<a href=\"#\" class=\"btn btn-outline-accent m-btn m-btn--icon m-btn--pill action_btn\"  data-toggle=\"modal\" data-target=\"#modal_setup_01\"><span><i class=\"flaticon-edit\"></i><span>設定</span></span></a>";
                  addColumns(waterTable,"設定",button_string,"edit");

              		//$('#water_table').DataTable();
                  adm_device_table.appendChild(waterTable);
               console.log(adm_device_table);
//-------------------------earthQuakeTable---------------------------------------------------------------------------------
              /*var earthTH = ["裝置ID", "裝置名稱", "所屬監測站","97座標X","97座標Y","三軸加速度","及時震度","X軸加速度","軸加速度","編輯"];
              var earthID = ["id_", "name_","StationName_", "X_","Y_","water_stage_","person_","X_","Y_","edit_"];
              var earthId = genDataID(deviceList,earthID);
              var earthTable = createTable("adm_earth_table","table table-striped- table-bordered table-hover table-checkable",earthTH,earthId);
                  adm_device_table.appendChild(earthTable);*/
              		//$('#earth_table').DataTable();
                  console.log(deviceList);
              		adm_getDeviceList(deviceList);


}


function Init_adm(){
 adm_Navigation();
 adm_initialTable();
}
