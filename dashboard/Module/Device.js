function GetColor(_alert)
{
  var _color = null;

  switch(_alert)
  {
    case 0:  _color = "#F87C00";  break;
    case 1:  _color = "#000000";  break;
    case 2:  _color = "#95989A";  break;
    //case 3:  _color = "#F87C00";  break;
  }

  return _color;
}

function GetValue(_devType, _alert, _value)
{
  var _str = null;

  if(_alert == 2 || _alert == null)  return "離線";

  switch(_devType)
  {
    case "temp":  _str = _value + ' ℃';    break;
    case "humi":  _str = _value + ' %';     break;
    case "co2":   _str = _value + ' ppm';   break;
    case "ph":    _str = _value;            break;
    case "ec":    _str = _value + ' dS/m';  break;
    case "wtemp": _str = _value + ' ℃';    break;
    case "air":   _str = (_value == 0) ? '不控制' : _value + ' ℃';    break;
    case "led":   _str = (_alert == 1) ? ((_value == 1) ? '開' : '關') : "異常";  break;
    case "pump":  _str = (_alert == 1) ? ((_value == 1) ? '開' : '關') : "異常";  break;
    case "ppm":   _str = (_alert == 1) ? ((_value == 1) ? '開' : '關') : "異常";  break;
    case "relay": _str = (_alert == 1) ? ((_value == 1) ? '開' : '關') : "異常";  break;
    default:
      _data = JSON.stringify(_data);
      if(_data.length < 500) { _str += _data; }
    break;
  }

  return _str;
}

function GetValue(_devType, _alert, _value)
{
  var _str = null;

  if(_alert == 2 || _alert == null)  return "離線";

  switch(_devType)
  {
    case "temp":  _str = _value + ' ℃';    break;
    case "humi":  _str = _value + ' %';     break;
    case "co2":   _str = _value + ' ppm';   break;
    case "ph":    _str = _value;            break;
    case "ec":    _str = _value + ' dS/m';  break;
    case "wtemp": _str = _value + ' ℃';    break;
    case "air":   _str = (_value == 0) ? '不控制' : _value + ' ℃';    break;
    case "led":   _str = (_alert == 1) ? ((_value == 1) ? '開' : '關') : "異常";  break;
    case "pump":  _str = (_alert == 1) ? ((_value == 1) ? '開' : '關') : "異常";  break;
    case "ppm":   _str = (_alert == 1) ? ((_value == 1) ? '開' : '關') : "異常";  break;
    case "relay": _str = (_alert == 1) ? ((_value == 1) ? '開' : '關') : "異常";  break;
    default:
      _data = JSON.stringify(_data);
      if(_data.length < 500) { _str += _data; }
    break;
  }

  return _str;
}
//-----
function GetIcon(_devType, _alert, _value)
{
  var _icon = null;
  var _type = null;

  try
  {
    _value = (_value == 0) ? "_off" : "";

    switch(_alert)
    {
      case 0:  _type = _devType + "_warning";  break;
      case 1:  _type = _devType + _value;     break;
      case 2:  _type = _devType + "_offline";  break;
      case null :  _type = _devType + "_offline";  break;
    }

    switch(_type)
    {
      case "temp": _icon = '/scripts/plant_factory/images/temperature.jpg'; break;
      case "temp_warning": _icon = '/scripts/plant_factory/images/temperature_o.jpg'; break;
      case "temp_offline": _icon = '/scripts/plant_factory/images/temperature_gray.jpg'; break;

      case "humi": _icon = '/scripts/plant_factory/images/humidity.jpg'; break;
      case "humi_warning": _icon = '/scripts/plant_factory/images/humidity_o.jpg'; break;
      case "humi_offline": _icon = '/scripts/plant_factory/images/humidity_g.jpg'; break;

      case "co2": _icon = '/scripts/plant_factory/images/co2.jpg'; break;
      case "co2_warning": _icon = '/scripts/plant_factory/images/co2_o.jpg'; break;
      case "co2_offline": _icon = '/scripts/plant_factory/images/co2_gray.jpg'; break;

      case "ph": _icon = '/scripts/plant_factory/images/ph.jpg'; break;
      case "ph_warning": _icon = '/scripts/plant_factory/images/ph_o.jpg'; break;
      case "ph_offline": _icon = '/scripts/plant_factory/images/ph_gray.jpg'; break;

      case "ec": _icon = '/scripts/plant_factory/images/ec.jpg'; break;
      case "ec_warning": _icon = '/scripts/plant_factory/images/ec_o.jpg'; break;
      case "ec_offline": _icon = '/scripts/plant_factory/images/ec_gray.jpg'; break;

      case "wtemp": _icon = '/scripts/plant_factory/images/w_temperature.jpg'; break;
      case "wtemp_warning": _icon = '/scripts/plant_factory/images/w_temperature_o.jpg'; break;
      case "wtemp_offline": _icon = '/scripts/plant_factory/images/w_temperature_gray.jpg'; break;

      case "do": _icon = '/scripts/plant_factory/images/do.jpg'; break;
      case "do_warning": _icon = '/scripts/plant_factory/images/do_o.jpg'; break;
      case "do_offline": _icon = '/scripts/plant_factory/images/do_gray.jpg'; break;

      case "led": _icon = '/scripts/plant_factory/images/light_on.jpg'; break;
      case "led_off": _icon = '/scripts/plant_factory/images/light_off.jpg'; break;
      case "led_offline": _icon = '/scripts/plant_factory/images/light_g.jpg'; break;

      case "air": _icon = '/scripts/plant_factory/images/air_on.jpg'; break;
      case "air_off": _icon = '/scripts/plant_factory/images/air_off.jpg'; break;
      case "air_warning": _icon = '/scripts/plant_factory/images/air_o.jpg'; break;
      case "air_offline": _icon = '/scripts/plant_factory/images/air_g.jpg'; break;

      case "ppm": _icon = '/scripts/plant_factory/images/co2relay_on.jpg'; break;
      case "ppm_off": _icon = '/scripts/plant_factory/images/co2relay_off.jpg'; break;
      case "ppm_warning": _icon = '/scripts/plant_factory/images/co2relay_o.jpg'; break;
      case "ppm_offline": _icon = '/scripts/plant_factory/images/co2relay_g.jpg'; break;

      case "pump": _icon = '/scripts/plant_factory/images/motor_on.jpg'; break;
      case "pump_off": _icon = '/scripts/plant_factory/images/motor_off.jpg'; break;
      case "pump_warning": _icon = '/scripts/plant_factory/images/motor_o.jpg'; break;
      case "pump_offline": _icon = '/scripts/plant_factory/images/motor_g.jpg'; break;
    }
  }
  catch(_ept){};

  return _icon;
}

function WarningList(_devid, _devType, _alert)
{
    if(_warning_list.length == 0)
    {
        var _warning_data = {};
        _warning_data.id = _devid;
        _warning_data.alert  = _alert;
        _warning_data.type = _devType;

        _warning_list.push(_warning_data);
    }

    var _flag = 0;
    for(var n = 0; n < _warning_list.length; n++)
    {
        var _item = _warning_list[n];

        if(_item.id == _devid)
        {
             _flag = 1;
            if(_alert != 1) { _item.alert = _alert;  }
            if(_alert == 1) { _warning_list.splice(n, 1);   n = n - 1;  }
        }
    }

    if(_flag == 0)
    {
        if(_alert == 0 || _alert == 2 || _alert == 3)
        {
            var _warning_data = {};
            _warning_data.id = _devid;
            _warning_data.alert = _alert;
            _warning_data.type = _devType;

            _warning_list.push(_warning_data);
        }
    }
}

function OnSensorsData(_payload)
{}

function SubscribeData()
{
  _User.SubscribeData('*', OnSensorsData);
}
function RefreshDevicesInGateway()
