//----- 
var DataProfile = "data_profile";
var _api_ip = "http://127.0.0.1:8005";
var _agent_ip = "http://127.0.0.1:8005";

var _database = "data";
var _DataSheet = {};
var _Gatewaylist = {};
var _Sensorlist = {};


var _RiverList = ["淡水河","大漢溪","新店溪","基隆河"];

var _AreaList = ["三峽區","鶯歌區","樹林區","土城區","板橋區","三重區","新莊區","蘆洲區","五股區","八里區",
                 "淡水區","新店區","深坑區","汐止區","瑞芳區","平溪區","泰山區","石碇區","坪林區","中和區","永和區"];

/*var _AreaList = ["大同區","萬華區","中正區","文山區","南港區","內湖區","松山區","中山區","士林區","北投區",
                  "三峽區","鶯歌區","樹林區","土城區","板橋區","三重區","新莊區","蘆洲區","五股區","八里區",
                  "淡水區","新店區","深坑區","汐止區","瑞芳區","平溪區","泰山區","石碇區","坪林區","中和區","永和區",
                  "暖暖區","七堵區","安樂區","信義區","復興區","龍潭區","大溪區","龜山區","尖石鄉","關西鎮","大同鄉"];*/

var _RiverEmbankmentList = [];
var _GatewayNameList = [];

var StatusNormal = "normal";
var StatusWarning = "warning";
var StatusError = "error";
var StatusDisconnect = "disconnect";

var SensorDescription_WaterPressure = "43";
var SensorDescription_Acceleration  = "44";
var SensorDescription_Inclinometer  = "45";
var SensorDescription_Earthquake_APlart = "46";
var SENSOR_TYPE_LIST = [SensorDescription_WaterPressure, SensorDescription_Acceleration, SensorDescription_Inclinometer, SensorDescription_Earthquake_APlart];

var GatewayType   = "26";
var WaterPressure = "43";
var Acceleration  = "44";
var Inclinometer  = "45";
var Earthquake    = "46";

//----- 
function API_Init()
{
  try
  {
    console.log("API_Init");

    //Api_Device(_database);
    Api_GetGatewayList(_database);
    Api_GetSensorList();
    Api_GetEmbankmentList();
  }
  catch(e){console.log(e);}
}
//----- 
function Api_GetGatewayList(_user)
{
  try
  {
    var _resultArray = Api_GetDataSrcList(_user);

    if(_resultArray != null)
    {
      for(var i = 0; i < _resultArray.length; i++)
      {
        var _obj = _resultArray[i];
        if(_obj != null)
        {
          if(_obj.uuid == "Gateway" && _obj.data_profile == "both.Gateway")
          {
            var _gatewayUUID = _obj.uuid;
            var _gatewaySUID = _obj.suid;

            var _resp = Api_GetCloudData(_gatewayUUID, _gatewaySUID, "setting");

            if(_resp != null && _resp.length > 0)
            {
              var _resp_json = JSON.parse(_resp);
              var _results = Api_GetResultArray(_resp_json);
              var _result = null;
              if(_results != null) { _result = _results[0]; }

              var _gatewayItem = {};
              _gatewayItem.uuid  = _gatewayUUID;
              _gatewayItem.suid  = _gatewaySUID;
              _gatewayItem.name  = _result.name;
              _gatewayItem.area  = _result.area
              _gatewayItem.bank  = _result.bank;
              _gatewayItem.river = _result.river;
              _gatewayItem.admin = _result.admin;
              _gatewayItem.coordinate    = _result.coordinate;
              _gatewayItem.install_date  = _result.install_date;
              _gatewayItem.maintain_date = _result.maintain_date;
              _gatewayItem.sensor_list  = _result.sensor_list;
              _gatewayItem.status_info = {};
              
              _gatewayItem.type = GatewayType;
              _gatewayItem.type_name = "監測站";
              _Gatewaylist[_gatewaySUID] = _gatewayItem;
            }
          }
        }
      }
    }
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetSensorList()
{
  try
  {
    for(var _gateway_suid in _Gatewaylist)
    {
      var _gatewayItem = _Gatewaylist[_gateway_suid];

      var _gatewayUUID = _gatewayItem.uuid;
      var _gatewaySUID = _gatewayItem.suid;
      var _sensorCount = _gatewayItem.sensor_list.length*10;

      var _resp = Api_GetCloudLastData(_gatewayUUID, _gatewaySUID, _sensorCount);

      var _coordinateArray = [];
      var _count = 1;
      if(_resp != null)
      {
        _resp = JSON.parse(_resp);
        var _results = Api_GetResultArray(_resp);

        if(_results != null && _results.length > 0)
        {
          for(var n = 0; n < _results.length; n++)
          {
            var _sensor = _results[n];
            var _sensorUUID = _sensor.uuid;
            var _sensorSUID = _sensor.suid;
            var _ukey = _sensor.ukey;
            var _remark = _sensor.remark;
            var _type_index = "";
            var _type_name = "";
            var _uint = "";

            if(_sensorSUID != null)
            {
              if(_ukey != null && _ukey.indexOf("info-") != -1)
              {
                if(_sensorSUID.indexOf(WaterPressure) != -1)     { _type_index = WaterPressure; _type_name = "水位計";   _uint = "cm"; }
                else if(_sensorSUID.indexOf(Acceleration) != -1) { _type_index = Acceleration;  _type_name = "加速度計"; _uint = "gal"; }
                else if(_sensorSUID.indexOf(Inclinometer) != -1) { _type_index = Inclinometer;  _type_name = "傾斜儀";   _uint = "度"; }
                else if(_sensorSUID.indexOf(Earthquake) != -1)   { _type_index = Earthquake;    _type_name = "地震儀";   _uint = "級"; }
                else _sensorSUID = null;

                if(_sensorUUID == "Sensor" && _sensorSUID != null)
                {
                  //190221 .Mick + coordinate shift
                  for(var i=0;i<_coordinateArray.length;i++) 
                    if(_coordinateArray[i].x == _sensor.coordinate.x && _coordinateArray[i].y == _sensor.coordinate.y)
                      _sensor.coordinate["xTemp"] = (parseInt(_sensor.coordinate.x) + (50*_count++));
                    
                  _sensor.coordinate["xTemp"] = (_sensor.coordinate["xTemp"]==null?_sensor.coordinate.x:_sensor.coordinate["xTemp"]);

                  var _coordinate = 
                  {
                    x:_sensor.coordinate["xTemp"],
                    y:_sensor.coordinate.y,
                  }
                  _coordinateArray.push(_coordinate); 

                  _sensor.gateway_uuid  = _gatewayItem.uuid;
                  _sensor.gateway_suid  = _gatewayItem.suid;
                  _sensor.gateway_name  = _gatewayItem.name;
                  _sensor.admin         = _gatewayItem.admin;
                  _sensor.area          = _gatewayItem.area;
                  _sensor.bank          = _gatewayItem.bank;
                  _sensor.river         = _gatewayItem.river;
                  _sensor.type          = _type_index;
                  _sensor.type_name     = _type_name;
                  _sensor.unit          = _uint;
                  _sensor.remark        = _remark;
                  _sensor.status_info   = {};
                  _Sensorlist[_sensorSUID] = _sensor;
                }
              }
            }
          }

          //------------------------------------------------------

          for(var n = 0; n < _results.length; n++)
          {
            var _data = _results[n];
            var _sensorUUID = _data.uuid;
            var _sensorSUID = _data.suid;
            var _ukey = _data.ukey;

            if(_sensorSUID != null)
            {
              var _sensor_tmp = _Sensorlist[_sensorSUID];

              if(_sensor_tmp != null)
              {
                if(_ukey != null && _ukey.indexOf("alert-") != -1)
                {
                  _sensor_tmp.alert_limit = _data.alert_limit;
                  _Sensorlist[_sensorSUID] = _sensor_tmp;
                }
                else if(_ukey != null && _ukey.indexOf("reminder-") != -1)
                {
                  _sensor_tmp.reminder = _data.reminder;
                  _Sensorlist[_sensorSUID] = _sensor_tmp;
                }
              }
            }
          }
          //------------------------------------------------------
        }
      }
    }
    //--------------------------------------------
  }
  catch(e){console.log(e);}
}
//----- 
function Api_GetEmbankmentList()
{
  try
  {
    //var _data = _User.getLastData('opendata.dike', 300, null);
    var _data = Api_GetCloudLastData("opendata", "dike-taipei", 3000);
    _data = JSON.parse(_data); 
    _data = _data.results;

    var count = 0;
    for(var i = 0; i <　_data.length; i++)
    {
      var _item = _data[i];
      _item.ukey = i;
      _item.area = "台北市";

      count = i;
    }

    count = count + 1;

    var _data2 = Api_GetCloudLastData("opendata", "dike-taiwan", 3000);
    _data2 = JSON.parse(_data2); 
    _data2 = _data2.results;

    for(var i = 0; i <_data2.length; i++)
    {
      var _item = _data2[i];
      _item.ukey = count + i;
      _item.area = "跨縣市";
    }

    _data = _data.concat(_data2);

    for (var i = 0; i < _data.length; i++)
    {
      var _item = _data[i];   

      if(_RiverEmbankmentList[_item.area] == null)
      {
        var area = {};
        area.area = "";
        area.embankment = [];
        _RiverEmbankmentList[_item.area] = area;
      }

      var area = _RiverEmbankmentList[_item.area];

      area.area = _item.area;
      area.embankment[_item.ukey] = _item;
    }
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetGatewayData(_gatewaySUID)
{
  try
  {
    if(_Gatewaylist != null) return _Gatewaylist[_gatewaySUID];
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetSensorData(_sensorSUID)
{
  try
  {
    if(_Sensorlist != null) return _Sensorlist[_sensorSUID];
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetCloudLastData(_uuid, _suid, _count)
{
  try
  {
    var _payload = 
    {
        user:_database,
        uuid:_uuid,
        suid:_suid,
        count:_count
    }; 

    var _result = _User.http_post_sync("/api/lastdata?method=read", JSON.stringify(_payload));  

    if(_result != null && _result.length > 0 && _result != "{}") return _result;
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetCloudDataByTime(_uuid, _suid, _stime, _etime)
{
  try
  {
    var _payload = 
    {
        user:_database,
        uuid:_uuid,
        suid:_suid,
        timestamp:_stime+'~'+_etime
    }; 

    var _result = _User.http_post_sync("/api/get?data", JSON.stringify(_payload));  

    if(_result != null && _result.length > 0 && _result != "{}") return _result;
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetCloudDataByTime2(_uuid, _suid, _stime, _etime)
{
  try
  {
    var flag = true;
    var final = null;
    var tmp_etime = _etime;

    var one_hour = 1 * 60 * 60 * 1000;
    var one_day = 24 * one_hour;

    var s_day = new Date(TimestampToDate(_stime, false)).getTime();
    var e_day = new Date(TimestampToDate(_etime, false)).getTime();
    var days = ((e_day - s_day) / one_day ) + 1;

    for(var i = days-1; i >= 0; i--)
    {
        var d_stime = s_day + i * one_day;
        var d_etime = s_day + (((i+1) * one_day) - 1);

        d_stime = (d_stime < _stime) ? _stime : d_stime;
        d_etime = (d_etime > _etime) ? _etime : d_etime;

        do
        {
            var _payload = 
            {
                user:_database,
                uuid:_uuid,
                suid:_suid,
                timestamp:d_stime+'~'+d_etime,
                limit:86400
            }; 

            var _result = _User.http_post_sync("/api/get?data", JSON.stringify(_payload));

            if(_result != null && _result.length > 0 && _result != "{}")
            {
                var data = JSON.parse(_result);
                var results = data.results;

                if(results.length == 86400) 
                {
                    flag = true;
                    tmp_etime = results[results.length-1].system_time - 1;
                }
                else 
                { 
                  flag = false;
                }

                if(final == null)  { final = data; } 
                else               { final.results = final.results.concat(results); }
            }
            else
            {
                flag = false;
            }

        }while(flag);
    }

    if(final != null)
    {
      return JSON.stringify(final);
    }
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetCloudDataByTime3(uuid, suid, stime, etime, days, day_index, success_callback, error_callback)
{
  try
  {
    var token = "Bearer " + localStorage.getItem("mars_cloud_auth_token");

    var payload = {
        user:_database,
        uuid:uuid,
        suid:suid,
        timestamp:stime+'~'+etime
    }; 

    payload = JSON.stringify(payload);

    $.ajax({
        type: "POST", 
        url: "/api/get?data",
        contentType: "application/json; charset=UTF-8",
        data:payload,
        //headers:{ "Authentication": token},
        beforeSend: function(request) {
            request.setRequestHeader("Authentication", token);
        },
        success: function(resp) {
            success_callback(resp, uuid, suid, days, day_index);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          error_callback(payload, uuid, suid, days, day_index);
        }
    });
  }
  catch(e){console.log(e);}
}
//----- 
function Api_GetCloudData(_uuid , _suid , _ukey)
{
  try
  {
    _ukey = (_ukey == null ? "":_ukey);
    var _payload = 
    {
        user:_database,
        uuid:_uuid,
        suid:_suid,
        ukey:_ukey
    }; 
    var _result = _User.http_post_sync("/api/get?data", JSON.stringify(_payload));
    //console.log("Api_GetCloudData : " + _result); 
    if(_result != null && _result.length > 0 && _result != "{}") return _result;
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_PutCloudEvent(_uuid , _suid , _ukey, _data)
{
  try
  {
    _ukey = (_ukey == null ? "":_ukey);
    var _payload = 
    {
        user:_database,
        uuid:_uuid,
        suid:_suid,
        ukey:_ukey,
        values: [_data]
    }; 
    var _result = _User.http_post_sync("/api/put?event", JSON.stringify(_payload));
    //console.log("Api_GetCloudData : " + _result); 
    if(_result != null) return _result;
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_PutCloudData(_uuid , _suid , _ukey, _data)
{
  try
  {
    _ukey = (_ukey == null ? "":_ukey);
    var _payload = 
    {
        user:_database,
        uuid:_uuid,
        suid:_suid,
        ukey:_ukey,
        values: [_data]
    }; 
    var _result = _User.http_post_sync("/api/put?data", JSON.stringify(_payload));
    //console.log("Api_GetCloudData : " + _result); 
    if(_result != null) return _result;
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_DeleteCloudData(_uuid , _suid , _ukey)
{
  try
  {
    _ukey = (_ukey == null ? "":_ukey);

    var _payload = 
    {
        user:_database,
        uuid:_uuid,
        suid:_suid,
        ukey:_ukey
    }; 

    var _result = _User.http_post_sync("/api/del?data", JSON.stringify(_payload));

    if(_result != null) return _result;
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetCloudList(_user)
{
  try
  {
    var _payload = 
    {
        user:_user
    }; 
    var _result = _User.http_post_sync("/api/usrinfo?method=datasrclist", JSON.stringify(_payload));

    return Api_GetResultArray(JSON.parse(_result));
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetCloudList(_user)
{
  try
  {
    var _payload = 
    {
        user:_user
    }; 
    var _result = _User.http_post_sync("/api/usrinfo?method=datasrclist", JSON.stringify(_payload));

    return Api_GetResultArray(JSON.parse(_result));
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetResultArray(_result)
{
  try
  {
    if(_result != null) _result = _result.results;
    if(_result != null)
    {
      return _result;
    } 
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_SubscribeData(src_id, OnSensorsData)
{
  try
  {
    _User.SubscribeData3(_database, src_id, OnSensorsData, null);
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_SubscribeEvent(src_id, OnSensorsData)
{
  try
  {
    _User.SubscribeEvent3(_database, src_id, OnSensorsData, null);
  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function Api_GetDataSrcList(_user)
{
  try
  {
    var _payload = {
        user: _user
    }; 

    var _result = _User.http_post_sync("/api/http_proxy?method=post&url="+_agent_ip+"/api/get?devicelist", JSON.stringify(_payload));

    if(_result != null) _result = JSON.parse(_result);
    if(_result != null) _result = _result.results;
    if(_result != null && _result.length > 0)
    {
      return _result;
    }
  }
  catch(e){console.log(e);}
}
//----- 
function Api_GetDataSrcInfo(_uuid, _suid)
{
  try
  {
    var _payload = {
        user: _database,
        uuid: _uuid,
        suid: _suid
    }; 

    var _result = _User.http_post_sync("/api/http_proxy?method=post&url="+_agent_ip+"/api/get?deviceinfo", JSON.stringify(_payload));

    if(_result != null) _result = JSON.parse(_result);
    if(_result != null)
    {
      return _result;
    }
  }
  catch(e){console.log(e);}

  return null;
}
//----- 
function Api_UploadDikeImage(_payload)
{
  try
  {
    return _User.http_post_sync("/api/http_proxy?method=post&url="+_agent_ip+"/api/dike?updateimage", JSON.stringify(_payload));
  }
  catch(e){console.log(e);}
}
//----- 