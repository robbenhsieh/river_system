var _pMap = null;
var _HeatMap = null;
var _RainCluster = null;
var _AllMarkers  = [];
var _AllDrawings  = [];
var _ClustersMarkers  = [];
//----- 
var MapOptions = 
{
  mapTypeControl:true,
  mapTypeControlOptions:
  { 
  //mapTypeControlOptions(指定提供的地圖類型) //mapTypeId(設定地圖控制項中欲顯示之底圖圖磚類型按鈕
  	mapTypeIds: [
	  						TGOS.TGMapTypeId.TGOSMAP,
	              TGOS.TGMapTypeId.ROADMAP,
	              TGOS.TGMapTypeId.F2IMAGE,
	              TGOS.TGMapTypeId.NLSCMAP,
	              TGOS.TGMapTypeId.HILLSHADEMIX
              ],
              /*
              TGOSMAP TGOS MAP。
              NLSCMAP 通用版電子地圖。
              MOTCMAP 路網數值圖。
              F2IMAGE 福衛二號衛星影像。
              ROADMAP 福衛混合地圖。
              HILLSHADE 地形暈渲圖。
              HILLSHADEMIX  地形暈渲混合地圖。
              SEGISMAP  統計區地圖。)
              */
    //若不設定則預設顯示所有類型的底圖圖磚按鈕供使用者切換
    controlPosition: TGOS.TGControlPosition.LEFT_TOP, //controlPosition(設定地圖類型控制項在地圖的位置)
    mapTypeControlStyle: TGOS.TGMapTypeControlStyle.DEFAULT
    //mapTypeControlstyle(設定地圖類型控制項樣式)
    //(可設定參數有：DEFAULT / HORIZONTAL_BAR / DROPDOWN_MENU)
  }
//disableDefaultUI: true    //關閉所有地圖操作介面
};
var InfoWindowOptions = 
{
  maxWidth:4000, //訊息視窗的最大寬度
  pixelOffset: new TGOS.TGSize(5, -30), //InfoWindow起始位置的偏移量, 使用TGSize設定, 向右X為正, 向上Y為負
  zIndex:99 //視窗堆疊順序
};
//----- 
function ShowMap() 
{
  try
  {
    InitMap();
    
    RefreshGatewayMarkList();
    RefreshSensorMarkList();    
    RefreshPumpStationMarkList();
    RefreshWaterGateMarkList();
    
    RefreshTaipeiDikeLayer();
    RefreshTaiwanDikeLayer();    
    RefreshRiskMapLayer();
    
    RefreshRainCurrent();
    //RefreshRivers();
    
    RefreshCheckBox();
  }
  catch(e){console.log(e);}
}
//-----
function InitMap()
{
  try
  {
    console.log("InitMap");

    var TGMap = document.getElementById("TGMap");  //宣告一個網頁容器
    var _markerPoint = new TGOS.TGPoint(295237.782, 2775514.575);
    var _mapOptiions = 
    {
      scaleControl: false,    	//不顯示比例尺
      navigationControl: true,  //顯示地圖縮放控制項
      navigationControlOptions: //設定地圖縮放控制項
      { 
        controlPosition: TGOS.TGControlPosition.LEFT_CENTER, //控制項位置
        navigationControlStyle: TGOS.TGNavigationControlStyle.SMALL //控制項樣式
      },
      mapTypeControl: false   //不顯示地圖類型控制項
    };
    
    _pMap = new TGOS.TGOnlineMap(TGMap, TGOS.TGCoordSys.EPSG3826 , _mapOptiions);//97座標
    _pMap.setOptions(MapOptions);
    _pMap.setZoom(7);
    _pMap.setCenter(_markerPoint);
  }
  catch(e){console.log(e);}  
}
//----- 
function CreateMarker(_name, _x, _y, _url, _msg)
{
	try
	{
		return CreateMarkerWithSize(_name, _x, _y, 24, 24, _url, _msg);
	}
	catch(e){console.log(e);}  
	return false;
}
//----- 
function CreateMarkerWithSize(_name, _x, _y, _w, _h, _url, _msg)
{
  try
  {
    //console.log("Create suid:" + _name);
	  
    var _markerPoint 	= new TGOS.TGPoint(_x, _y);
    var _markerImg 		= new TGOS.TGImage(_url, new TGOS.TGSize(_w, _h), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(_w/2, _h)); //TGImage(圖片網址,圖片大小,原點位置,錨點位置<X為正圖片向左移動，Y為正圖片向上移動>)
    var _pTGMarker 		= new TGOS.TGMarker(_pMap, _markerPoint, "", _markerImg); 
    var _message 		= new TGOS.TGInfoWindow(_msg , _markerPoint, InfoWindowOptions);//訊息視窗出現位置
    
	TGOS.TGEvent.addListener(_pTGMarker , "click", function () 
	{ 
	  _message.open(_pMap);
	});
	
	TGOS.TGEvent.addListener(_pTGMarker , "closeclick", function () 
	{ 
	  _message.close();
	});

    if (_AllMarkers!= null && _AllMarkers.length > 0) 
    {
      for (var i = 0; i < _AllMarkers.length; i++) 
        if(_name == _AllMarkers[i].name)
        {
          _AllMarkers[i].maker.setMap(null);
          _AllMarkers[i].maker = _pTGMarker;
          _AllMarkers[i].message = _message;
          _AllMarkers[i].maker.setVisible(true);
          return true;
        }
    }

    var _data = 
    {
      name : _name,
      maker : _pTGMarker,
      message : _message
    };

    _AllMarkers.push(_data);
    return true;
  }
  catch(e){console.log(e);}
  return false;
}
//----- 
function ShowMarker(_name)
{
  try
  {
    if (_AllMarkers!=null && _AllMarkers.length > 0) 
      for (var i = 0; i < _AllMarkers.length; i++) 
        if(_name == _AllMarkers[i].name)
          _AllMarkers[i].maker.setVisible(true);
  }
  catch(e){console.log(e);}  
}
//----- 
function HideMarker(_name)
{
  try
  {
    //console.log("hide suid:" + _name);
    if (_AllMarkers!= null && _AllMarkers.length > 0) 
      for (var i = 0; i < _AllMarkers.length; i++) 
        if(_name == _AllMarkers[i].name)
        {
          _AllMarkers[i].message.close();
          _AllMarkers[i].maker.setMap(null);
          _AllMarkers.splice(i, 1);
          return true;
        }
  }
  catch(e){console.log(e);}  
  return false;
}
//----- 
function HideAllMarkers()
{
  try
  {
    if (_AllMarkers.length > 0) 
      for (var i = 0; i < _AllMarkers.length; i++) 
        _AllMarkers[i].maker.setVisible(false);
  }
  catch(e){console.log(e);} 
}
//----- 
function CreateLine(_name, _path, _color, _size, _x, _y, _msg)
{
	try
	{
		if(ShowLine(_name) == false)
		{
			var _markerPoint = new TGOS.TGPoint(_x, _y);
			var _message  = new TGOS.TGInfoWindow(_msg , _markerPoint, InfoWindowOptions);
			var _new_line = new TGOS.TGLine(_pMap, new TGOS.TGLineString(_path), { strokeColor: _color, strokeWeight: _size });

			var _line = 
			{
				name : _name,
				line : _new_line,
				message : _message,
				click : false
			};

			_AllDrawings.push(_line);

			//-----------------------------------------------------------------------------------

			TGOS.TGEvent.addListener(_line.line, "mouseover", function (e) 
			{
				_line.line.setStrokeWeight(8);
			});

			TGOS.TGEvent.addListener(_line.line, "mouseout", function () 
			{
				if(_line.click == false){
					_line.line.setStrokeWeight(2);
				}
			});

			TGOS.TGEvent.addListener(_line.line , "click", function (e) 
			{
				var _showPoint = new TGOS.TGPoint(e.point.x, e.point.y);
				_line.message.setPosition(_showPoint)
				_line.message.open(_pMap);

				_line.click = true;
			});

			TGOS.TGEvent.addListener(_line.message, "closeclick", function () 
			{ 
				//_line.message.close();
				_line.line.setStrokeWeight(2);
				_line.click = false;
			});
		}
	}
	catch(e){console.log(e);} 
}
//----- 
function ShowLine(_name)
{
	try
	{
		for (var i = 0; i < _AllDrawings.length; i++)	
			if(_AllDrawings[i].name == _name)
			{
				_AllDrawings[i].line.setVisible(true);
				return true;
			}
	}
	catch(e){console.log(e);} 
	return false;
}
//----- 
function CreatePolycon(_name, _path, _color, _size, _fill_color)
{
	try
	{
		if(ShowLine(_name) == false)
		{
			var _line = 
			{
				name : _name,
				line : new TGOS.TGFill(_pMap, new TGOS.TGPolygon([new TGOS.TGLinearRing(new TGOS.TGLineString(_path))]), { fillColor: _fill_color, strokeColor: _color, fillOpacity: 0.3, strokeWeight: _size })
			};
			
			_AllDrawings.push(_line);
		}
	}
	catch(e){console.log(e);} 
}
//----- 
function HideDrawings(_name)
{
	try
	{
		for (var i = 0; i < _AllDrawings.length; i++)	
			if(_AllDrawings[i].name == _name)
			{
				_AllDrawings[i].message.close();
				_AllDrawings[i].line.setVisible(false);
				return true;
			}
	}
	catch(e){console.log(e);} 
	return false;
}
//-----
var _MapType = 97;
//-----
function AddCheckEvent(_checkID , _img)
{
  var _item = document.getElementById(_checkID);
  if(_item == null) return;
  _item.addEventListener("click", function() 
  {
    try
    {
      
      for(var _gatewaySUID in _Gatewaylist)
      {
        var _obj = Api_GetGatewayData(_gatewaySUID);
        var _checkID = "check_mark_" + _obj.suid;
        var _checkBox = document.getElementById(_checkID);
        var _suid = _obj.suid;

        if(_checkBox != null && _suid != null)
        {
          if(_checkBox.checked)
          {
            var _x = _obj.coordinate.x;
            var _y = _obj.coordinate.y;

            var _desc = '<br>';
          	_desc += '名稱:'		+ _obj.name + '<br>';
          	_desc += '機關:'		+ _obj.admin+ '<br>';
          	_desc += '區域:'		+ _obj.area + '<br>';
          	_desc += '岸別:'		+ _obj.river + '<br>';
          	_desc += '座標:'+_x+','+_y+'<br>';

            if(_x != null && _y != null) CreateMarker(_suid , _x , _y  , _img , _desc);
          }
          else
          {
            HideMarker(_suid);
          }
        }
      }
    }
    catch(_e){console.log(_e)};
  }); 
}
//-----
function RefreshGatewayMarkList()
{
  try
  {
    var _gateway_mark_list = document.getElementById("gateway_mark_list");
    $('#gateway_mark_list').empty();
    $('#gateway_mark_list').append('<label class="m-checkbox all_input" id="gateway_mark_all"><input type="checkbox" value="">全部<span></span></label>');
    AddCheckEvent("gateway_mark_all" , "./image/mark_gateway.png");

    for(var _gatewaySUID in _Gatewaylist)
    {
      var _obj = Api_GetGatewayData(_gatewaySUID);
      if(_obj != null)
      {
        var _name = (_obj.name==null||_obj.name.length <=0?_obj.suid:_obj.name);
        var _suid = _obj.suid;
        var _key = "gateway_mark_" + _suid;
        var _checkID = "check_mark_" + _suid;

        $('#gateway_mark_list').append('<label class="m-checkbox" id="'+_key+'"><input type="checkbox" id="'+_checkID+'" value="'+_checkID+'">'+_name+'<span></span></label>');
        AddCheckEvent(_checkID , "./image/mark_gateway.png");
      }
    }
  }
  catch(e){console.log(e);}
}
//-----
function RefreshSensorMarkList()
{
  try
  {
    var header = document.getElementById("sensor_mark_list");
    var _item = header.getElementsByClassName("m-sensor-checkbox");
    for (var i = 0; i < _item.length; i++) 
    {
      _item[i].addEventListener("click", function() 
      {
        console.log("click:" + this.id);
        for(var j=0;j<SENSOR_TYPE_LIST.length;j++)
        {
          var _checkBox = document.getElementById("sensor_type_" + SENSOR_TYPE_LIST[j]);
          if(_checkBox != null)
          {
            SetSensorMarkers(SENSOR_TYPE_LIST[j] , _checkBox.checked);
          }
        }
      });
    }    
  }
  catch(e){console.log(e);}
}

//-----
function ShowRiversData(_checkbox)
{
	try
	{		
		//var _data =_User.getLastData('opendata.river', 1000, null);   	
		var _data = Api_GetCloudLastData("opendata", "river", 30000);
		var _result = [];
		
		_data=JSON.parse(_data); 
		_data=_data.results;
		
		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i]; 
			//if(_item.name == '基隆河' || _item.name == '大漢溪' || _item.name == '石門水庫' || _item.name == '淡水河')
			{
				  var _points = _item.points;
				  var _path = [];
				  var _index = 0;
				  
				  if(_points.length > 0)
				  {
					  _item.ukey = "river_"+_item.ukey;
					  
					  for (var j = 0; j < _points.length; j++)
					  {
						  var _p = TGOS.WGS84toTWD97(_points[j].lng, _points[j].lat);					  
						  _path.push(new TGOS.TGPoint(_p.x, _p.y));
					  }
					  
					  CreatePolycon(_item.ukey, _path, '#FF3300', 1, '#FF3300');
					  
					  if(_points[_index] != null)
					  {
						  var _p = TGOS.WGS84toTWD97(_points[_index].lng, _points[_index].lat);
						  var _desc = '<br>';
						  
						  _desc += '名稱 : '+_item.name+'<br>';					  
						  _desc += '座標:'+_p.x+','+_p.y+'<br>';
						  //console.log(_item.name+' -> '+_p.x+','+_p.y);				  
						  CreateMarkerWithSize(_item.ukey, _p.x, _p.y, 16, 16, "./image/mark_river.png", _desc);
					  }
					  
					  if(_checkbox.checked == false)
					  {
						  HideMarker(_item.ukey);
						  HideDrawings(_item.ukey);
					  }
				  }
			}
		}
	}
	catch(e){console.log(e);}
}
//-----
function RefreshRivers()
{
	try
	{		  
		var _cb_rivers = document.getElementById('cb_rivers');
		
		_cb_rivers.addEventListener("click", function() { ShowRiversData(_cb_rivers); });
	}
	catch(e){console.log(e);}
}
//-----
function ShowRainData(_checkbox)
{
	try
	{		
		//var _data=_User.getLastData('opendata.twrain', 1, null);   	
		var _data = Api_GetCloudLastData("opendata", "twrain", 1);
		var _result = [];
		
		_data=JSON.parse(_data); 
		_data=_data.results[0].packet;	
		
		for (var i = 0; i < _data.length; i++)
		{
			var _item = _data[i]; 
			//if(_item.city == '基隆市' || _item.city == '臺北市' || _item.city == '新北市' || _item.city == '桃園市')
			{
				var _p = TGOS.WGS84toTWD97(_item.lng, _item.lat);  
				var _desc = "<br>";
				
				_item.value = _item.value > 0 ? _item.value : 0;
				_item.value_12hr = _item.value_12hr > 0 ? _item.value_12hr : 0;
				_item.value_24hr = _item.value_24hr > 0 ? _item.value_24hr : 0;
				
				_desc += '編號 : '+_item.id+'<br>';
				_desc += '所在 : '+_item.city+'<br>';
				_desc += '區域 : '+_item.town+'<br><br>';
				_desc += '即時 : '+_item.value+' mm<br>';
				_desc += '12hr : '+_item.value_12hr+' mm<br>';
				_desc += '24hr : '+_item.value_24hr+' mm<br>';
				_desc += '座標:'+_p.x+','+_p.y+'<br>';
				
				CreateMarkerWithSize(_item.id, _p.x, _p.y, 24, 24, "./image/mark_rain.png", _desc);			
				
				if(_checkbox.checked == false || _item.value_24hr <= 0) {
					HideMarker(_item.id);
				}
				//if(_checkbox.checked == false) HideMarker(_item.id);
			}
		}
	}
	catch(e){console.log(e);}
}
//-----
function RefreshRainCurrent()
{
	try
	{		  
		var _cb_cur = document.getElementById('cb_rain_current');
		
		_cb_cur.addEventListener("click", function() { ShowRainData(_cb_cur); });
	}
	catch(e){console.log(e);}
}
//-----
function RefreshTaipeiDikeLayer()
{
  try
  {
	  var _checkbox = document.getElementById('cb_dike_taipei');
	  
	  _checkbox.addEventListener("click", function() 
	  { 
		  try
		  {
			  //var _data=_User.getLastData('opendata.dike', 300, null);  
			  var _data = Api_GetCloudLastData("opendata", "dike-taipei", 30000); 
			  			  
			  _data = JSON.parse(_data); 
			  _data = _data.results;
			  
			  for (var i = 0; i < _data.length; i++)
			  {
				  var _item = _data[i];
				  var _points = _item.points;
				  var _path = [];
				  var _index = 0;
				  
				  if(_points.length > 0)
				  {
					  _item.ukey = "dike_taipei_"+_item.ukey;
					  
					  for (var j = 0; j < _points.length; j++)
					  {
						  var _p = TGOS.WGS84toTWD97(_points[j].lng, _points[j].lat);					  
						  _path.push(new TGOS.TGPoint(_p.x, _p.y));
					  }
					  
					  if(_points[_index] != null)
					  {
						  var _p = TGOS.WGS84toTWD97(_points[_index].lng, _points[_index].lat);
						  var _desc = '<br>';
						  
						  _desc += '名稱 : '+_item.name+'<br>';
						  if(_item.river.length > 0){
						  	_desc += '河川 : '+_item.river+'<br>';
						  }
						  _desc += '長度 : '+parseInt(_item.len, 10)+' 公尺<br>';
						  
						  //console.log(_item.name+' -> '+_p.x+','+_p.y);				  
						  //CreateMarkerWithSize(_item.ukey, _p.x, _p.y, 24, 24, "./image/mark_dike.png", _desc);

						  CreateLine(_item.ukey, _path, '#0033FF', 2, _p.x, _p.y, _desc);
					  }
					  
					  if(_checkbox.checked == false)
					  {
						  HideMarker(_item.ukey);
						  HideDrawings(_item.ukey);
					  }
				  }
			  }
		  }
		  catch(e){console.log(e);}
		  
	  });
  }
  catch(e){console.log(e);}
}
//-----
function RefreshTaiwanDikeLayer()
{
  try
  {
	  var _checkbox = document.getElementById('cb_dike_taiwan');
	  
	  _checkbox.addEventListener("click", function() 
	  { 
		  try
		  {
			  //var _data=_User.getLastData('opendata.dike', 300, null);  
			  var _data = Api_GetCloudLastData("opendata", "dike-taiwan", 30000); 
			  			  
			  _data = JSON.parse(_data); 
			  _data = _data.results;
			  			  
			  for (var i = 0; i < _data.length; i++)
			  {
				  var _item = _data[i];
				  var _points = _item.points;
				  var _path = [];
				  var _index = 0;
				  
				  if(_points.length > 0)
				  {
					  _item.ukey = "dike_taiwan_"+_item.ukey;
					  
					  for (var j = 0; j < _points.length; j++)
					  {
						  var _p = TGOS.WGS84toTWD97(_points[j].lng, _points[j].lat);					  
						  _path.push(new TGOS.TGPoint(_p.x, _p.y));
					  }
					  
					  if(_points[_index] != null)
					  {
						  var _p = TGOS.WGS84toTWD97(_points[_index].lng, _points[_index].lat);
						  var _desc = '<br>';
						  
						  _desc += '名稱 : '+_item.name+'<br>';
						  if(_item.river.length > 0){
						  	_desc += '河川 : '+_item.river+'<br>';
						  }
						  _desc += '長度 : '+parseInt(_item.len, 10)+' 公尺<br>';
						  
						  //console.log(_item.name+' -> '+_p.x+','+_p.y);				  
						  //CreateMarkerWithSize(_item.ukey, _p.x, _p.y, 24, 24, "./image/mark_dike.png", _desc);

						  CreateLine(_item.ukey, _path, '#0033FF', 2, _p.x, _p.y, _desc);
					  }
					  
					  if(_checkbox.checked == false)
					  {
						  HideMarker(_item.ukey);
						  HideDrawings(_item.ukey);
					  }
				  }
			  }
		  }
		  catch(e){console.log(e);}
		  
	  });
  }
  catch(e){console.log(e);}
}
//-----
function RefreshRiskMapLayer()
{
  try
  {
	  var _checkbox = document.getElementById('cb_riskmap');
	  
	  _checkbox.addEventListener("click", function() 
	  { 
		  try
		  {
			  //var _data=_User.getLastData('opendata.dike', 300, null);  
			  var _data = Api_GetCloudLastData("opendata", "riskmap", 30000); 
			  			  
			  _data = JSON.parse(_data); 
			  _data = _data.results;
			  			  
			  for (var i = 0; i < _data.length; i++)
			  {
				  var _item = _data[i];
				  var _points = _item.points;
				  var _path = [];
				  var _index = 0;
				  
				  if(_points.length > 0)
				  {
					  _item.ukey = "risk_"+_item.ukey;
					  
					  for (var j = 0; j < _points.length; j++)
					  {
						  var _p = TGOS.WGS84toTWD97(_points[j].lng, _points[j].lat);					  
						  _path.push(new TGOS.TGPoint(_p.x, _p.y));
					  }
					  
					  if(_points[_index] != null)
					  {
						  var _p = TGOS.WGS84toTWD97(_points[_index].lng, _points[_index].lat);
						  var _desc = '<br>';
						  
						  _desc += '名稱 : '+_item.name+'<br>';
						  _desc += '類型 : '+_item.type+'<br>';
						  _desc += '風險 : '+_item.risk+'<br>';
						  _desc += '長度 : '+parseInt(_item.len, 10)+' 公尺<br>';
						  
						  var color = "#CDDC39";

						  switch(_item.risk)
						  {
						  	case "低度風險": color = "#FFFF00"; break;
						  	case "中度風險": color = "#FF9800"; break;
						  	case "高度風險": color = "#F44336"; break;
						  	case "極度風險": color = "#FF00FF"; break;
						  }						  
						  
						  //console.log(_item.name+' -> '+_p.x+','+_p.y);				  
						  //CreateMarkerWithSize(_item.ukey, _p.x, _p.y, 24, 24, "./image/mark_dike.png", _desc);

						  CreateLine(_item.ukey, _path, color, 2, _p.x, _p.y, _desc);
					  }
					  
					  if(_checkbox.checked == false)
					  {
						  HideMarker(_item.ukey);
						  HideDrawings(_item.ukey);
					  }
				  }
			  }
		  }
		  catch(e){console.log(e);}
		  
	  });
  }
  catch(e){console.log(e);}
}
//-----
function RefreshPumpStationMarkList()
{
  try
  {
	  var _checkbox = document.getElementById('cb_pumpstation');
	  
	  _checkbox.addEventListener("click", function() 
	  {
 
		  try
		  {
			  //var _data=_User.getLastData('opendata.pumpstation', 300, null);   
			  var _data = Api_GetCloudLastData("opendata", "pumpstation", 30000); 
			  
			  _data = JSON.parse(_data); 
			  _data = _data.results;
			  
			  for (var i = 0; i < _data.length; i++)
			  {
				  var _item = _data[i];
				  
				  var _p = TGOS.WGS84toTWD97(_item.lng, _item.lat);
				  var _desc = '<br>';
				  
				  _item.ukey = "pump_"+_item.ukey;
				  
				  _desc += '名稱 : '+_item.name+'<br>';
				  _desc += '單位 : '+_item.owner+'<br>';
				  _desc += '地址 : '+_item.address+'<br>';
				  _desc += '電話 : '+_item.phone+'<br>';
				  _desc += '座標:'+_p.x+','+_p.y+'<br>';
				  
				  //console.log(_item.name+' -> '+_p.x+','+_p.y);				  
				  CreateMarkerWithSize(_item.ukey, _p.x, _p.y, 24, 24, "./image/mark_pump.png", _desc);	
		          
			      	if(_checkbox.checked == false) {
			      		HideMarker(_item.ukey);
			      	}
			  }
		  }
		  catch(e){console.log(e);}
		  
	  });
  }
  catch(e){console.log(e);}
}
//-----
function RefreshWaterGateMarkList()
{
  try
  {
	  var _checkbox = document.getElementById('cb_watergate');
	  
	  _checkbox.addEventListener("click", function() 
	  {
 
		  try
		  {
			  //var _data=_User.getLastData('opendata.watergate', 3000, null);   
			  var _data = Api_GetCloudLastData("opendata", "watergate", 30000); 
			  
			  _data = JSON.parse(_data); 
			  _data = _data.results;
			  
			  for (var i = 0; i < _data.length; i++)
			  {
				  var _item = _data[i];
				  
				  //if(_item.river == '淡水河' || _item.river == '二重疏洪道' || _item.river == '大漢溪' || _item.river == '景美溪')
				  {
					  var _p = TGOS.WGS84toTWD97(_item.lng, _item.lat);
					  var _desc = '<br>';
					  
					  _item.ukey = "gate_"+_item.ukey;
					  
					  _desc += '名稱 : '+_item.name+'<br>';
					  _desc += '水域 : '+_item.river+'<br>';
					  _desc += '所在 : '+_item.city+'<br>';
					  _desc += '區域 : '+_item.town+'<br>';
					  _desc += '堤防 : '+_item.dike+'<br>';
					  _desc += '類型 : '+_item.type+'<br>';
					  _desc += '座標 : '+_p.x+','+_p.y+'<br>';
					  
					  //console.log(_item.name+' -> '+_p.x+','+_p.y);				  
					  CreateMarkerWithSize(_item.ukey, _p.x, _p.y, 24, 24, "./image/mark_sluice_gate.png", _desc);	
			          
			      	if(_checkbox.checked == false) {
			      		HideMarker(_item.ukey);
			      	}
				  }
			  }
		  }
		  catch(e){console.log(e);}
		  
	  });
  }
  catch(e){console.log(e);}
}
//-----
function SetSensorMarkers(_type , _checked)
{
  try
  {
    var _flag = false;
    for(var _sensorSUID in _Sensorlist)
    {
      var _obj = Api_GetSensorData(_sensorSUID);
      var _array = [];
      if(_obj != null && _obj.suid != null)
      {
        var _suid = _obj.suid;
        var _sensorType = _suid.split("-")[0];
        if(_sensorType == _type)
        {
          if(_checked)
          {
            //var _x = _obj.coordinate.x;
            var _x = _obj.coordinate.xTemp;
            var _y = _obj.coordinate.y;
            if(_x != null && _y != null) 
            {
            	var _desc = '<br>';
            	_desc += '監測站:' 	+ _obj.gateway_name + '<br>';
            	_desc += '名稱:'		+ _obj.name + '<br>';
            	_desc += '機關:'		+ _obj.admin+ '<br>';
            	_desc += '區域:'		+ _obj.area + '<br>';
            	_desc += '岸別:'		+ _obj.river + '<br>';
            	_desc += '座標:'+_x+','+_y+'<br>';
            	
            	console.log("SUID:" + _suid + " x:" + _x + " y:" + _y);
              _flag = CreateMarker(_suid , _x , _y  , "./image/mark_sensor.png" , _desc);

            }
          }
          else
          {
            _flag = HideMarker(_suid);
            //HideAllMarkers();
          }
        }
      }
    } 
  }
  catch(e){console.log(e);}
}