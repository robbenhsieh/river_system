


var _SensorInfo = null;
var _ColorDisplay=['#AA6666', '#66AA66', '#6666AA'];
//----- 
function Adm_Init()
{
  try
  {
    if(WasLogin() == false) return;

    if(_User.GetCurrentUserGroup() != "administrator"){
      window.location='/scripts/river_system/dashboard/index.html';
    }

    document.getElementById("info_CurUser").innerHTML = _User.GetCurrentUserName();

    document.getElementById("LogOutButton").onclick =  function(){ OnLogOutClick(); };
    setInterval(WasLogin, 60000);

    API_Init();
    LoadDataSrcInfo();

  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function WasLogin()
{
	try
	{
		if(_User.WasLogin() == null)
		{
			DoLogOut();
			return false;
		}
			
		return true;
	}
	catch(e){console.log(e);}
	return false;
}
//-----
function OnLogOutClick()
{
	try
	{
		if(confirm('是否要登出本系統?'))
			DoLogOut();
	}
	catch(e){console.log(e);}
}
//-----
function DoLogOut()
{
	try
	{
		_User.DoLogout();
		
		window.location = '/scripts/river_system/dashboard/login.html';
	}
	catch(e){console.log(e);}
}
//-----
function LoadDataSrcInfo()
{  
  try
  {
    document.getElementById('dtp_day').value = new Date().YMD();

    var uuid = "ServerInfo";
    var suid = "RiverSystem";

    var resp = Api_GetDataSrcInfo(uuid, suid);

    if(resp != null)
    {
      _SensorInfo = resp;
      _SensorInfo.id = uuid + "." + _SensorInfo.suid;
      document.getElementById('label_dev_id').innerHTML = _SensorInfo.id;
      document.getElementById('tb_sensor_name').innerHTML = _SensorInfo.name;
      document.getElementById('label_profile').innerHTML = "電腦主機";

      UpdateCurrentData();
      $('#dtp_day').change(UpdateCurrentData);
    }
  }
  catch(e){console.log(e);}
}
//-----  
function ParseData(_profile)
{             
  var _div_chart = document.getElementById('sensor_chart');
  var _dtp_day = document.getElementById('dtp_day');      
  var _date = new Date(_dtp_day.value);
  var _day = _date.YMD();
  var _totla_results = [];              
  //var _time_zone_offset = _date.getTimezoneOffset()*60000;
  //var _start_time = _date.getTime();
  var _start_time = moment(_day, "YYYY-MM-DD").utc().valueOf();
  var _seconds_of_hour = 3600;
  var _scripts = [ { key:'cpu_usage', name:'CPU' }, { key:'mem_usage_p', name:'Memory' } ];     
  //var _data = _User.getOneDayReport(_profile.id, _start_time+_time_zone_offset, null);
  var _data = _User.GetOneDayReport("data", _profile.id, _start_time, null);
  var _root = JSON.parse(_data);

    if(_data != null && _data.length > 0)
    {   
      _start_time = _start_time/1000;
      //_time_zone_offset = _time_zone_offset/1000;
      
      for(var sindex=0;sindex<_scripts.length;sindex++)
      {                 
        var i;
        var _last_value;
        var _values=[];         
        var _ticks=[];          
          var _result=[];                     
                                              
          for(i=0; i<24; i++) { _values.push(0); _ticks.push(0) };
          for(i=0; i < _root.results.length; i++)          
          {
            var _ts=_root.results[i].system_time/1000;
            var _hour_index=Math.floor((_ts - _start_time)/_seconds_of_hour);
                                      
            if(_hour_index >=0 && _hour_index < _values.length)
            {
              var _res=_root.results[i];                
              var _value= _res.system != null ? (_res.system[_scripts[sindex].key]) : null ;
              
              if(_value != null)
              {
                //if(_scripts[sindex].name == 'Memory') _value = _value/1024;
                  
                _values[_hour_index]=_values[_hour_index]+_value;
                _ticks[_hour_index]++;
              }
            }                           
          }
                                
          for(i=0; i<_values.length; i++)
          {            
            if(_values[i] > 0)
            {
              _last_value=Math.floor(_values[i]/_ticks[i]);
              _result.push({x: i, y: _last_value});
            }  
            else
              _result.push({x: i, y: 0});  
          }           
                      
          _totla_results.push({values: _result, key: _scripts[sindex].name, color: _ColorDisplay[sindex], area: true, fillOpacity: 0.3});  
      }
    }
  
  return _totla_results;
}
//----- 
function UpdateCurrentData()
{
  var _dlg = document.getElementById('dlg_Content');      
  var _chart = document.getElementById('sensor_chart');
  
  document.getElementById('sensor_chart').innerHTML = '<center><img src="/images/wait/balls.svg"</img></center>';  
  
  setTimeout(function()
  {
    var _data = ParseData(_SensorInfo);   
      if( _data != null)
      {       
        var _chart=null;        
        nv.addGraph(function()
        {
          _chart = nv.models.lineChart();
          _chart.useInteractiveGuideline(true);
          _chart.showLegend(true);
          _chart.showYAxis(true);  
          _chart.showXAxis(true);
              
          _chart.options
                ({
                    transitionDuration: 0,
                    useInteractiveGuideline: true
                });                
                
          _chart.xAxis.showMaxMin(false).axisLabelDistance(0)
              .tickFormat(function(d)
            {
                return ("0"+d).slice(-2)+":00";
            });
                                    
          _chart.lines.forceY([0]);
          _chart.lines.padData(false);
          
          var _graphObj = d3.select('#sensor_chart');     
          _graphObj.selectAll("*").remove();
          _graphObj.append('svg').attr('vertical-align', 'top').attr('height', Math.floor(document.getElementById('sensor_chart').offsetHeight * 1.15) + 'px').datum(_data).call(_chart);
          
          nv.utils.windowResize(_chart.update);
        });
      } 
      
  }, 500);
}
//-----  
function OnPrevDayClick()
{  
  var _dtp_day = document.getElementById('dtp_day');
  _dtp_day.value = new Date(_dtp_day.value).addDays(-1).YMD();
  
  UpdateCurrentData();
}
//-----  
function OnNextDayClick()
{  
  var _dtp_day = document.getElementById('dtp_day');
  _dtp_day.value = new Date(_dtp_day.value).addDays(1).YMD();
  
  UpdateCurrentData();
}
//-----