

var current_sensor = {};
var wfb_array = [];

//----- 
function Adm_Init()
{
  try
  {
    if(WasLogin() == false) return;

    if(_User.GetCurrentUserGroup() != "administrator"){
      window.location='/scripts/river_system/dashboard/index.html';
    }

    document.getElementById("info_CurUser").innerHTML = _User.GetCurrentUserName();

    document.getElementById("LogOutButton").onclick =  function(){ OnLogOutClick(); };
    setInterval(WasLogin, 60000);

    API_Init();
    InitSensorTable();
    RefreshSensorList();

  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function WasLogin()
{
	try
	{
		if(_User.WasLogin() == null)
		{
			DoLogOut();
			return false;
		}
			
		return true;
	}
	catch(e){console.log(e);}
	return false;
}
//-----
function OnLogOutClick()
{
	try
	{
		if(confirm('是否要登出本系統?'))
			DoLogOut();
	}
	catch(e){console.log(e);}
}
//-----
function DoLogOut()
{
	try
	{
		_User.DoLogout();
		
		window.location = '/scripts/river_system/dashboard/login.html';
	}
	catch(e){console.log(e);}
}
//-----
function InitSensorTable()
{
  try
  {
    var table_list = document.getElementById("adm_sensor_table_list");
    table_list.innerHTML = "";

    var gw_select = document.getElementById("adm_sensor_gw_select");
    var area_select = document.getElementById("adm_sensor_area_select");
    var river_select = document.getElementById("adm_sensor_river_select");
    var sensor_type_select = document.getElementById("adm_sensor_type_select");
    
    var gw_options    = "<option value='0'>全部</option>";
    var area_options  = "<option value='0'>全部</option>";
    var river_options = "<option value='0'>全部</option>";
    var sensor_options = "<option value='0'>全部</option>";

    var area_list = [];
    var river_list = [];

    for(var gw in _Gatewaylist) {
      var tmp = _Gatewaylist[gw];
      gw_options += '<option value={index}>{name}</option>'.replace(/{index}/g, tmp.uuid+"."+tmp.suid).replace(/{name}/g, tmp.name);

      area_list[tmp.area] = tmp.area;
      river_list[tmp.river] = tmp.river;
    }

    for(var area in area_list) {
      area_options += '<option value={index}>{name}</option>'.replace(/{index}/g, area).replace(/{name}/g, area);
    }
    
    for(var river in river_list) {
      river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
    }

    sensor_options += '<option value='+WaterPressure+'>水位計</option>';
    sensor_options += '<option value='+Acceleration+'>加速度計</option>';
    sensor_options += '<option value='+Inclinometer+'>傾斜儀</option>';
    sensor_options += '<option value='+Earthquake+'>地震儀</option>';

    gw_select.innerHTML = gw_options;
    area_select.innerHTML = area_options;
    river_select.innerHTML = river_options;
    sensor_type_select.innerHTML = sensor_options;

    gw_select.onchange = function(){ RefreshSensorList(); };
    area_select.onchange = function(){ RefreshSensorList(); };
    river_select.onchange = function(){ RefreshSensorList(); };
    sensor_type_select.onchange = function(){ RefreshSensorList(); };
  }
  catch(e){console.log(e);}
}
//-----
function RefreshSensorList()
{
  try
  {
    DestroyTable("adm_sensor_table");
   
    var edit_btn = _User.http_get_unauth('../template/edit_btn_sensor_alert.js');

    var gw = document.getElementById("adm_sensor_gw_select").value;
    var area = document.getElementById("adm_sensor_area_select").value;
    var river = document.getElementById("adm_sensor_river_select").value;
    var type = document.getElementById("adm_sensor_type_select").value;

    var table_list = document.getElementById("adm_sensor_table_list");
    table_list.innerHTML = "";

    var index = 1;
    for(var suid in _Sensorlist)
    {
      var obj = Api_GetSensorData(suid);

      if(obj != null)
      {
        var gw_ukey = obj.gateway_uuid + "." + obj.gateway_suid;
        var item_ukey = "sensoreditbtn_" + obj.uuid + "_" + obj.suid;

        var area_flag  = (area  == obj.area)  || (area  == "0") ? true : false;
        var river_flag = (river == obj.river) || (river == "0") ? true : false;
        var type_flag  = (type  == obj.type)  || (type  == "0") ? true : false;
        var gw_flag    = (gw    == gw_ukey)   || (gw    == "0") ? true : false;

        if(gw_flag && area_flag && river_flag && type_flag)
        {
          var str = "";
          str += "<tr>";
          str += "<td>"+(obj.name==null?"--":obj.name)+"</td>";
          str += "<td>"+(obj.type_name==null?"--":obj.type_name)+"</td>";
          str += "<td>"+(obj.gateway_name==null?"--":obj.gateway_name)+"</td>";
          str += "<td>"+(obj.area==null?"--":obj.area)+"</td>";
          str += "<td>"+(obj.admin==null?"--":obj.admin)+"</td>";
          str += "<td>"+(obj.river==null?"--":obj.river)+"</td>";
          str += "<td>"+(obj.bank==null?"--":obj.bank)+"</td>";
          str += "<td>"+(obj.coordinate==null?"--":obj.coordinate.x)+"</td>";
          str += "<td>"+(obj.coordinate==null?"--":obj.coordinate.y)+"</td>";
          str += "<td>"+ edit_btn.replace("{edit_btn_id}", item_ukey) + "</td>";
          str += "</tr>";

          table_list.innerHTML += str;

          index = index + 1;
        }
      }
    }
    //-------------------------------------------------------

     InitTable("adm_sensor_table");
  }
  catch(e){console.log(e);}
}
//---------------------
function InitModalSetup03(type)
{
  try
  {
    wfb_array = [];

    var modal_setup = document.getElementById("modal_setup_03");
    var modal_setup_template = _User.http_get_unauth('../template/modal_setup_03.js');

    modal_setup.innerHTML = modal_setup_template;

    //---------------------------------------------

    if(type == Earthquake) 
    { 
      var tab_template = _User.http_get_unauth('../template/modal_setup_03_tab01_eq.js'); 
      document.getElementById("modal_03_tab_setup_01").innerHTML = tab_template;
    }
    else if(type == WaterPressure || type == Acceleration || type == Inclinometer) 
    { 
      var tab_template = _User.http_get_unauth('../template/modal_setup_03_tab01_nt.js'); 
      document.getElementById("modal_03_tab_setup_01").innerHTML = tab_template;
    }

    //---------------------------------------------

    document.getElementById("modal_sensor_tittle_name").innerHTML = "";
    document.getElementById("surveillance_ctrl").options[1].selected = true;

    if(type == WaterPressure || type == Acceleration || type == Inclinometer) 
    {
      var l_limit = document.getElementById("l_limit");
      var h_limit = document.getElementById("h_limit");

      l_limit.oninput = function(){ CheckValue("l_limit", "h_limit") };
      h_limit.oninput = function(){ CheckValue("l_limit", "h_limit") };

      l_limit.value = 0;
      h_limit.value = 0;

      wfb_array["l_limit"] = true;
      wfb_array["h_limit"] = true;
    }
    else if(type == Earthquake) 
    {
      var x_l_limit = document.getElementById("x_l_limit");
      var x_h_limit = document.getElementById("x_h_limit");
      var y_l_limit = document.getElementById("y_l_limit");
      var y_h_limit = document.getElementById("y_h_limit");
      var z_l_limit = document.getElementById("z_l_limit");
      var z_h_limit = document.getElementById("z_h_limit");
      var mix_l_limit = document.getElementById("mix_l_limit");
      var mix_h_limit = document.getElementById("mix_h_limit");
      var mmi_l_limit = document.getElementById("mmi_l_limit");
      var mmi_h_limit = document.getElementById("mmi_h_limit");

      x_l_limit.oninput = function(){ CheckValue("x_l_limit", "x_h_limit") };
      x_h_limit.oninput = function(){ CheckValue("x_l_limit", "x_h_limit") };
      y_l_limit.oninput = function(){ CheckValue("y_l_limit", "y_h_limit") };
      y_h_limit.oninput = function(){ CheckValue("y_l_limit", "y_h_limit") };
      z_l_limit.oninput = function(){ CheckValue("z_l_limit", "z_h_limit") };
      z_h_limit.oninput = function(){ CheckValue("z_l_limit", "z_h_limit") };
      mix_l_limit.oninput = function(){ CheckValue("mix_l_limit", "mix_h_limit") };
      mix_h_limit.oninput = function(){ CheckValue("mix_l_limit", "mix_h_limit") };
      mmi_l_limit.oninput = function(){ CheckValue("mmi_l_limit", "mmi_h_limit") };
      mmi_h_limit.oninput = function(){ CheckValue("mmi_l_limit", "mmi_h_limit") };

      x_l_limit.value = 0;
      x_h_limit.value = 0;
      y_l_limit.value = 0;
      y_h_limit.value = 0;
      z_l_limit.value = 0;
      z_h_limit.value = 0;
      mix_l_limit.value = 0;
      mix_h_limit.value = 0;
      mmi_l_limit.value = 0;
      mmi_h_limit.value = 0;

      wfb_array["x_l_limit"] = true;
      wfb_array["x_h_limit"] = true;
      wfb_array["y_l_limit"] = true;
      wfb_array["y_h_limit"] = true;
      wfb_array["z_l_limit"] = true;
      wfb_array["z_h_limit"] = true;
      wfb_array["mix_l_limit"] = true;
      wfb_array["mix_h_limit"] = true;
      wfb_array["mmi_l_limit"] = true;
      wfb_array["mmi_h_limit"] = true;
    }

    //------------------------------------------------------------------
    
    var arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
          
    $('#m_datepicker_warn').datepicker({
        language: 'zh-TW',
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows,
        format: 'yyyy/mm/dd'
    });

    //------------------------------------------------------------------

    $('#m_datepicker_warn').datepicker('update', new Date());

    document.getElementById("maintain_reminder_select").options[0].selected = true;
    document.getElementById("reminder_interval_1").checked = true;

    //------------------------------------------------------------------

  }
  catch(e){console.log(e);}
}
//---------------------
function ShowModalSetup03(info_btn)
{
  try
  {
    var id = info_btn.id;
    var tmp_array = id.split("_");
    var sensor_uuid = tmp_array[1];
    var sensor_suid = tmp_array[2];

    current_sensor = Api_GetSensorData(sensor_suid);

    //---------------------------------------------

    InitModalSetup03(current_sensor.type); 

    //---------------------------------------------

    document.getElementById("modal_sensor_tittle_name").innerHTML = current_sensor.name;

    var reminder = current_sensor.reminder;
    var alert_limit = current_sensor.alert_limit;

    if(alert_limit != null)
    {
      var alert_action = ((alert_limit.action == true) ? 0 : 1);
      document.getElementById("surveillance_ctrl").options[alert_action].selected = true;

      if(current_sensor.type == WaterPressure) 
      {
        document.getElementById("l_limit_unit").innerHTML = "範圍下限 (公分)";
        document.getElementById("h_limit_unit").innerHTML = "範圍上限 (公分)";

        document.getElementById("l_limit").value = alert_limit.water_stage.low;
        document.getElementById("h_limit").value = alert_limit.water_stage.high;

        CheckValue("l_limit", "h_limit");
      }
      else if(current_sensor.type == Acceleration)
      {
        document.getElementById("l_limit_unit").innerHTML = "範圍下限 (gal)";
        document.getElementById("h_limit_unit").innerHTML = "範圍上限 (gal)";

        document.getElementById("l_limit").value = alert_limit.acceleration.low;
        document.getElementById("h_limit").value = alert_limit.acceleration.high;

        CheckValue("l_limit", "h_limit");
      } 
      else if(current_sensor.type == Inclinometer)
      {
        document.getElementById("l_limit_unit").innerHTML = "範圍下限 (度)";
        document.getElementById("h_limit_unit").innerHTML = "範圍上限 (度)";

        document.getElementById("l_limit").value = alert_limit.inclinometer.low;
        document.getElementById("h_limit").value = alert_limit.inclinometer.high;

        CheckValue("l_limit", "h_limit");
      }
      else if(current_sensor.type == Earthquake) 
      {
        document.getElementById("axis_x_lab").innerHTML = "X加速度 (gal)";
        document.getElementById("axis_y_lab").innerHTML = "Y加速度 (gal)";
        document.getElementById("axis_z_lab").innerHTML = "Z加速度 (gal)";
        document.getElementById("axis_c_lab").innerHTML = "三軸加速度 (gal)";
        document.getElementById("axis_m_lab").innerHTML = "震度 (級)";

        document.getElementById("x_l_limit").value = alert_limit.axis_x.low;
        document.getElementById("x_h_limit").value = alert_limit.axis_x.high;
        document.getElementById("y_l_limit").value = alert_limit.axis_y.low;
        document.getElementById("y_h_limit").value = alert_limit.axis_y.high;
        document.getElementById("z_l_limit").value = alert_limit.axis_y.low;
        document.getElementById("z_h_limit").value = alert_limit.axis_y.high;
        document.getElementById("mix_l_limit").value = alert_limit.axis_compose.low;
        document.getElementById("mix_h_limit").value = alert_limit.axis_compose.high;
        document.getElementById("mmi_l_limit").value = alert_limit.mmi_level.low;
        document.getElementById("mmi_h_limit").value = alert_limit.mmi_level.high;

        CheckValue("x_l_limit", "x_h_limit");
        CheckValue("y_l_limit", "y_h_limit");
        CheckValue("z_l_limit", "z_h_limit");
        CheckValue("mix_l_limit", "mix_h_limit");
        CheckValue("mmi_l_limit", "mmi_h_limit");
      }
    }

    //---------------------------------------------

    if(reminder != null)
    {
      var action = ((reminder.action == true) ? 0 : 1);
      document.getElementById("maintain_reminder_select").options[action].selected = true;

      $('#m_datepicker_warn').datepicker('update', new Date(reminder.time));

      if(reminder.interval != 1)    document.getElementById("reminder_interval_1").checked = true;
      if(reminder.interval == 1)    document.getElementById("reminder_interval_2").checked = true;
    }

    //---------------------------------------------
  }
  catch(e){console.log(e);}
}
//-----
function UpdateSensorAlertSetting()
{
  try
  {
    var flag = true;

    for(var id in wfb_array) {
      flag = (flag && wfb_array[id]);
    }

    if(flag == false)
    {
      alert("輸入的數值不合規範，請重新檢查。");
      return;
    }

    //-----------------------------------------------------------------
    var gateway_uuid = current_sensor.gateway_uuid;
    var gateway_suid = current_sensor.gateway_suid;
    var sensor_uuid = current_sensor.uuid;
    var sensor_suid = current_sensor.suid;

    var alert_ukey = "alert-" + sensor_suid;
    var reminder_ukey = "reminder-" + sensor_suid;

    var reminder = {};
    var alert_limit = {};

    //-------------------------------------------------------------------

    alert_limit.action = (document.getElementById("surveillance_ctrl").value == 0) ? false : true;

    if(current_sensor.type == WaterPressure) 
    {
      alert_limit.water_stage = {};
      alert_limit.water_stage.low  = parseFloat(document.getElementById("l_limit").value);
      alert_limit.water_stage.high = parseFloat(document.getElementById("h_limit").value);
    }
     else if(current_sensor.type == Acceleration)
    {
      alert_limit.acceleration = {};
      alert_limit.acceleration.low  = parseFloat(document.getElementById("l_limit").value);
      alert_limit.acceleration.high = parseFloat(document.getElementById("h_limit").value);
    } 
    else if(current_sensor.type == Inclinometer)
    {
      alert_limit.inclinometer = {};
      alert_limit.inclinometer.low  = parseFloat(document.getElementById("l_limit").value);
      alert_limit.inclinometer.high = parseFloat(document.getElementById("h_limit").value);
    }
    else if(current_sensor.type == Earthquake) 
    {
      alert_limit.axis_x = {};
      alert_limit.axis_y = {};
      alert_limit.axis_z = {};
      alert_limit.axis_compose = {};
      alert_limit.mmi_level = {};

      alert_limit.axis_x.low         = parseFloat(document.getElementById("x_l_limit").value);
      alert_limit.axis_x.high        = parseFloat(document.getElementById("x_h_limit").value);
      alert_limit.axis_y.low         = parseFloat(document.getElementById("y_l_limit").value);
      alert_limit.axis_y.high        = parseFloat(document.getElementById("y_h_limit").value);
      alert_limit.axis_z.low         = parseFloat(document.getElementById("z_l_limit").value);
      alert_limit.axis_z.high        = parseFloat(document.getElementById("z_h_limit").value);
      alert_limit.axis_compose.low   = parseFloat(document.getElementById("mix_l_limit").value);
      alert_limit.axis_compose.high  = parseFloat(document.getElementById("mix_h_limit").value);
      alert_limit.mmi_level.low      = parseFloat(document.getElementById("mmi_l_limit").value);
      alert_limit.mmi_level.high     = parseFloat(document.getElementById("mmi_h_limit").value);
    }

    //------------------------------------------------------------------

    var s_date = $("#m_datepicker_warn").val();
    reminder.time = moment(s_date + " " + "00:00:00.000", "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
    reminder.interval = (document.getElementById("reminder_interval_1").checked == true) ? 0.5 : 1;
    reminder.action = (document.getElementById("maintain_reminder_select").value == 0) ? false : true;

    //------------------------------------------------------------------

    current_sensor.reminder = reminder;
    current_sensor.alert_limit = alert_limit;

    var alert_config = {};
    alert_config.cmd = "set.alert_setting";
    alert_config.suid = sensor_suid;
    alert_config.alert_limit = alert_limit;

    var result = Api_PutCloudEvent(gateway_uuid, gateway_suid, alert_ukey, alert_config);

    if(result != null && result == "Ok")
    {
	    var reminder_config = {};
	    reminder_config.cmd = "set.reminder_setting";
	    reminder_config.suid = sensor_suid;
	    reminder_config.reminder = reminder;

	    result = Api_PutCloudEvent(gateway_uuid, gateway_suid, reminder_ukey, reminder_config);

	    if(result != null && result == "Ok")
	    {
		    API_Init();
		    RefreshSensorList();
		    alert("更新成功");

		    document.getElementById("modal_alert_close").click();

		    //------------------------------------------------------------------

        var _log = {};
        _log.type = "system";
        _log.msg = _User.GetCurrentUserID() + " 變更感測器「" + current_sensor.name + "」的警示設定";

		    _User.addSystemLog(_log);

		    return;
	    }
	}

  }
  catch(e){console.log(e);}
  alert("更新失敗");

}
//-----
function CheckValue(l_id, h_id)
{
  try
  {
    var low = document.getElementById(l_id).value;
    var high = document.getElementById(h_id).value;

    document.getElementById(l_id + "_fb").style.display = "none";
    document.getElementById(h_id + "_fb").style.display = "none";

    document.getElementById(l_id + "_fb_msg").innerHTML = "";
    document.getElementById(h_id + "_fb_msg").innerHTML = "";

    var l_flag = isNaN(low);
    var h_flag = isNaN(high);

    if(l_flag == false && h_flag == false)
    {
      low = parseFloat(low);
      high = parseFloat(high);

      if(low > high)
      {
        document.getElementById(l_id + "_fb").style.display = "";
        document.getElementById(l_id + "_fb_msg").innerHTML = "下限值大於上限值";

        wfb_array[l_id] = false;
      }
      else { wfb_array[l_id] = true;  wfb_array[h_id] = true;}
    }
    else
    {
      if(l_flag == true)
      {
        document.getElementById(l_id + "_fb").style.display = "";
        document.getElementById(l_id + "_fb_msg").innerHTML = "請輸入數字";

        wfb_array[l_id] = false;
      }
      else { wfb_array[l_id] = true; }

      if(h_flag == true)
      {
        document.getElementById(h_id + "_fb").style.display = "";
        document.getElementById(h_id + "_fb_msg").innerHTML = "請輸入數字";

        wfb_array[h_id] = false;
      }
      else { wfb_array[h_id] = true; }
    }
  }
  catch(e){console.log(e);}
}
//-----
function InitTable(table_id) 
{
  try
  {
    var table = "#" + table_id;

    $(table).DataTable({
      scrollCollapse: true,
      scrollX: true,
      retrieve: true,
      destroy: true,

      //== DOM Layout settings-筆數
      dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",

      lengthMenu: [5, 10, 20, 30, 50],
      pageLength: 20,
      language: {
	    "loadingRecords": "載入中...",
	    'lengthMenu': '資料筆數 _MENU_',
	    "zeroRecords":  "沒有符合的結果",
	    "info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
	    "infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
	    "infoFiltered": "(從 _MAX_ 項結果中過濾)",
	    "infoPostFix":  "",
	    "search":       "搜尋:",
	    /*"paginate": {
	        "first":    "第一頁",
	        "previous": "上一頁",
	        "next":     "下一頁",
	        "last":     "最後一頁"
	    },*/
	    "aria": {
	        "sortAscending":  ": 升冪排列",
	        "sortDescending": ": 降冪排列"
	    }
      }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
       $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
  }
  catch(e){console.log(e);}
}
//-----
function DestroyTable(table_id) 
{
  try
  {
    var table = "#" + table_id;
    if($.fn.DataTable.isDataTable(table)) 
    {
      $(table).DataTable().destroy();

      $(table_id + ' tbody').empty();
    }
  }
  catch(e){console.log(e);}
}