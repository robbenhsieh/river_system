var user_info_list = [];
var current_user = null;
//----- 
function Init()
{
  try
  {
    current_user = null;
    
    if(WasLogin() == false) return;    

    if(_User.GetCurrentUserGroup() != "administrator"){
      window.location='/scripts/river_system/dashboard/index.html';
    }

    document.getElementById("info_CurUser").innerHTML = _User.GetCurrentUserName();
    
    document.getElementById("LogOutButton").onclick =  function(){ OnLogOutClick(); };
    setInterval(WasLogin, 60000);
    

    document.getElementById("group_select").onchange = function(){ RefreshUserList(); };
    document.getElementById("active_select").onchange = function(){ RefreshUserList(); };
    document.getElementById("account_filter").oninput = function(){ RefreshUserList(); };
    
    RefreshUserList();

  }
  catch(e){console.log(e);}
  return null;
}
//----- 
function WasLogin()
{
  try
  {
    if(_User.WasLogin() == null)
    {
      DoLogOut();
      return false;
    }
      
    return true;
  }
  catch(e){console.log(e);}
  return false;
}
//-----
function OnLogOutClick()
{
  try
  {
    if(confirm('是否要登出本系統?'))
      DoLogOut();
  }
  catch(e){console.log(e);}
}
//-----
function DoLogOut()
{
  try
  {
    _User.DoLogout();
    
    window.location = '/scripts/river_system/dashboard/login.html';
  }
  catch(e){console.log(e);}
}
//----- 
function RefreshUserList()
{ 
  try
  {
    DestroyTable("account_table");

    user_info_list = [];

    var edit_btn = _User.http_get_unauth('../template/edit_btn_account.js');

    var group = document.getElementById("group_select").value;
    var active = document.getElementById("active_select").value;
    var account = document.getElementById("account_filter").value;

    var account_table_list = document.getElementById("account_table_list");
    account_table_list.innerHTML = "";
    
    var tmp_list = "";

    //------------------------------------------------------

    if(account.length <= 0) account = "*";
    
    var resp = _User.GetUserList(account);

    if(resp != null)
    {
      var data = JSON.parse(resp);  
      var user_list = data.results;

      for(var i = 0; i < user_list.length; i++)
      {
        var account_id = user_list[i];
        var resp = _User.GetUserInfo(account_id);

        var user_info = null;

        if(resp != null)  user_info = JSON.parse(resp);

        if(user_info != null && user_info.id != null)
        {
          var user_group = user_info.group.toLowerCase();
          var user_active = (user_info.active == true) ? "1" : "2";

          var group_flag = ((group == user_group) || group == "0") ? true : false;
          var active_flag = ((active == user_active) || active == "0") ? true : false;

          if(group_flag && active_flag)
          {
            var item = "<tr><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>"; 

            item = item.replace('{1}', user_info.name);
            item = item.replace('{2}', user_info.id);
            item = item.replace('{3}', ((user_group == "administrator") ? "系統管理員" : "一般用戶"));
            item = item.replace('{4}', ((user_active == "1" )? "啟用" : "停用"));
            item = item.replace('{5}', edit_btn.replace("{index}", user_info_list.length));

            tmp_list += item

            user_info_list.push(user_info);
          }
        }
      }
    }

    account_table_list.innerHTML = tmp_list;

    InitTable("account_table");

  }
  catch(e){console.log(e);}
}
//----- 
function ShowModalSetup01(edit_btn)
{     
  try
  {
    var id = edit_btn.id;
    var tmp_array = id.split("_");
    var index = tmp_array[1];

    var user_info = user_info_list[index];
    current_user = user_info;

    //----------------------------------------------------------------------------------

    var modal_setup_template = _User.http_get_unauth('../template/modal_setup_01_2.js');
    var modal_setup = document.getElementById("modal_setup_01");

    modal_setup.innerHTML = "";
    modal_setup.innerHTML = modal_setup_template;

    document.getElementById("account_title").innerHTML = user_info.name;

    //----------------------------------------------------------------------------------

    document.getElementById("user_name").value     = user_info.name;     
    document.getElementById("user_account").value  = user_info.id;  
    document.getElementById("user_password").value = user_info.password;  
    document.getElementById("user_email").value    = user_info.email;
    document.getElementById("user_phone").value    = user_info.phone1;
    document.getElementById("user_group_select").value = user_info.group.toLowerCase();
    
    //if(user_info.sex == 1)    document.getElementById("gender_male").checked = true;
    //if(user_info.sex != 1)    document.getElementById("gender_female").checked = true;
    if(user_info.active == true)   document.getElementById("active_true").checked = true;
    if(user_info.active == false)  document.getElementById("active_false").checked = true;

    if(user_info.alert_msg != null && user_info.alert_msg.length > 0)
    {
      var alert_msg = user_info.alert_msg;
      for(var i = 0; i < alert_msg.length; i++)
      {
        var tmp = alert_msg[i];
        if(tmp.indexOf("26") != -1)  document.getElementById("ac_gateway_type_26").checked = true;
        if(tmp.indexOf("43") != -1)  document.getElementById("ac_sensor_type_43").checked = true;
        if(tmp.indexOf("44") != -1)  document.getElementById("ac_sensor_type_44").checked = true;
        if(tmp.indexOf("45") != -1)  document.getElementById("ac_sensor_type_45").checked = true;
        if(tmp.indexOf("46") != -1)  document.getElementById("ac_sensor_type_46").checked = true;
        if(tmp.indexOf("maintain") != -1)  document.getElementById("maintain").checked = true;
      }
    }
  }
  catch(e){console.log(e);}
}
//-----
function UpdateUserInfo()
{
  try
  {
    var alert_msg = [];
    if(document.getElementById("ac_gateway_type_26").checked == true){  alert_msg.push(GatewayType); }
    if(document.getElementById("ac_sensor_type_43").checked == true) {  alert_msg.push(WaterPressure); }
    if(document.getElementById("ac_sensor_type_44").checked == true) {  alert_msg.push(Acceleration); }
    if(document.getElementById("ac_sensor_type_45").checked == true) {  alert_msg.push(Inclinometer); }
    if(document.getElementById("ac_sensor_type_46").checked == true) {  alert_msg.push(Earthquake); }
    if(document.getElementById("maintain").checked == true)          {  alert_msg.push("maintain"); }

    current_user.name     = document.getElementById("user_name").value;
    current_user.password = document.getElementById("user_password").value; 
    current_user.email    = document.getElementById("user_email").value;
    current_user.phone1   = document.getElementById("user_phone").value;
    current_user.group    = document.getElementById("user_group_select").value;
    current_user.active   = document.getElementById("active_true").checked;
    //current_user.sex      = document.getElementById("gender_male").checked == true ? 1 : 0;
    current_user.alert_msg = alert_msg;
    
    var resp = _User.UpdateUserAdv(current_user.id, current_user, true);
    
    if(resp == true)
    {
      var payload = {};
      
      payload.cmd = "set.user_info";
      payload.id = current_user.id;
      payload.name = current_user.name;
      payload.email = current_user.email;
      payload.phone1 = current_user.phone1;
      payload.group = current_user.group;
      payload.active = current_user.active;
      payload.alert_msg = alert_msg;

      var result = Api_PutCloudEvent("update", "userinfo", null, payload);

      if(result != null && result == "Ok")
      {
        RefreshUserList();
        alert("更新成功");

        var _log = {};
        _log.type = "system";
        _log.msg = _User.GetCurrentUserID() + " 變更使用者「" + current_user.name + "」的帳號設定";

        _User.addSystemLog(_log);

        document.getElementById("modal_user_close").click();

        return;
      }      
    }
  }
  catch(e){console.log(e);}
  alert("更新失敗");
}
//-----
function RemoveUser()
{
  try
  {
    if(confirm('是否要刪除 ' + current_user.id+"/"+current_user.name+ ' 這個使用者?'))
    {     
      var resp = _User.DelUser(current_user.id);     
      if(resp == true)
      {
        RefreshUserList();
        alert("移除成功");
        return;
      }
    }
  }
  catch(e){console.log(e);}
  alert("移除失敗");
}
//-----
function InitRegistryAccount()
{
  try
  {
    document.getElementById("adm_account").value = "";
    document.getElementById("adm_password").value = "";
    document.getElementById("adm_confirm_password").value = "";
    document.getElementById("adm_email").value = "";
    document.getElementById("adm_group_select").options[0].selected = true;

    //document.getElementById("reg_info").style.display = "none";
  }
  catch(e){console.log(e);}
}
//----
function RegistryNewAccount()
{
  var account = document.getElementById("adm_account").value;
  var password = document.getElementById("adm_password").value;
  var confirm_password = document.getElementById("adm_confirm_password").value;
  var email = document.getElementById("adm_email").value;
  var group = document.getElementById("adm_group_select").value;

  var chars_filter = /^[0-9a-z_]*$/;              
         
  if(chars_filter.test(account) == false)
  {
    alert("帳號格式錯誤，帳號請使用英文小寫與數字，並請勿使用特殊字元。");
    return;
  }

  if(account.length <= 0 || password.length <= 0 || confirm_password.length <= 0 || email.length <= 0)
  {
    alert("帳號、密碼、信箱不能為空。");
    return;
  }

  var email_pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if(email_pattern.test(email) == false)
  {
    alert("信箱格式錯誤。");
    return;
  }

  /*if(chars_filter.test(password) == false)
  {
    //alert( "Do not use special characters as PASSWORD");
    //return;
  }*/

  if(password.length > 0 && password != confirm_password)
  {
    alert("密碼不正確");
    return;
  }

  //-------------------------------------------------------

  var data = {}
  data.id = account;
  data.name = account;
  data.email = email;
  data.password = password;
  data.group = group;
  data.activate_type = "link_by_email";

  //document.getElementById("reg_info").style.display = "";

 // var stime = new Date().getTime();
  if(_User.DoRegistry(data))
  {
    //var etime = new Date().getTime();

   //alert("time: " + (etime - stime));

    alert("帳號新增成功");
    document.getElementById("m_modal_add_close").click();
  }
  else
  {
    alert("帳號新增失敗，帳號可能有使用特殊字元。");
  }

  RefreshUserList();
}
//-----
function InitTable(table_id) 
{
  try
  {
    var table = "#" + table_id;

    $(table).DataTable({
      scrollCollapse: true,
      scrollX: true,
      retrieve: true,
      destroy: true,

      //== DOM Layout settings-筆數
      dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",

      lengthMenu: [5, 10, 20, 30, 50],
      pageLength: 20,
      language: {
		"loadingRecords": "載入中...",
	    'lengthMenu': '資料筆數 _MENU_',
	    "zeroRecords":  "沒有符合的結果",
	    "info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
	    "infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
	    "infoFiltered": "(從 _MAX_ 項結果中過濾)",
	    "infoPostFix":  "",
	    "search":       "搜尋:",
	    /*"paginate": {
	        "first":    "第一頁",
	        "previous": "上一頁",
	        "next":     "下一頁",
	        "last":     "最後一頁"
	    },*/
	    "aria": {
	        "sortAscending":  ": 升冪排列",
	        "sortDescending": ": 降冪排列"
	    }
      }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
       $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
  }
  catch(e){console.log(e);}
}
//-----
function DestroyTable(table_id) 
{
  try
  {
    var table = "#" + table_id;
    if($.fn.DataTable.isDataTable(table)) 
    {
      $(table).DataTable().destroy();

      $(table_id + ' tbody').empty();
    }
  }
  catch(e){console.log(e);}
}