
var current_gateway = {};
var current_sensor = {};
var current_checklist = null;
var checklist_flag = true;

var em_check_list = [];

var cl_file_array = [];
var cl_image_array = [];

//----- 
function Adm_Init()
{
  try
  {
    if(_User.GetCurrentUserGroup() != "administrator"){
      window.location='/scripts/river_system/dashboard/index.html';
    }

    document.getElementById("info_CurUser").innerHTML = _User.GetCurrentUserName();

    if(WasLogin() == false) return;
    document.getElementById("LogOutButton").onclick = function(){ OnLogOutClick(); };
    setInterval(WasLogin, 60000);
    
    API_Init();

    SubscribeData();

    InitGatewayTable();
    InitSensorTable();
    InitCheckListTable();

    RefreshGatewayList();
    RefreshSensorList();
    RefreshCheckList();
  }
  catch(e){console.log(e);}
  return null;
}

//----- 
function WasLogin()
{
	try
	{
		if(_User.WasLogin() == null)
		{
			DoLogOut();
			return false;
		}
			
		return true;
	}
	catch(e){console.log(e);}
	return false;
}
//-----
function OnLogOutClick()
{
	try
	{
		if(confirm('是否要登出本系統?'))
			DoLogOut();
	}
	catch(e){console.log(e);}
}
//-----
function DoLogOut()
{
	try
	{
		_User.DoLogout();
		
		window.location = '/scripts/river_system/dashboard/login.html';
	}
	catch(e){console.log(e);}
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------
function InitGatewayTable()
{
  try
  {
    var table_list = document.getElementById("adm_gw_table_list");
    table_list.innerHTML = "";

    var area_select = document.getElementById("adm_gw_area_select");
    var river_select = document.getElementById("adm_gw_river_select");

    var area_options = "<option value='0'>全部</option>";
    var river_options = "<option value='0'>全部</option>";

    var area_list = [];
    var river_list = [];

    for(var gw in _Gatewaylist) {
      var tmp = _Gatewaylist[gw];

      area_list[tmp.area] = tmp.area;
      river_list[tmp.river] = tmp.river;
    }

    for(var area in area_list) {
      area_options += '<option value={index}>{name}</option>'.replace(/{index}/g, area).replace(/{name}/g, area);
    }
    
    for(var river in river_list) {
      river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
    }

    area_select.innerHTML = area_options;
    river_select.innerHTML = river_options;

    //------------------------------------------------------

    area_select.onchange = function(){ RefreshGatewayList(); };
    river_select.onchange = function(){ RefreshGatewayList(); };

  }
  catch(e){console.log(e);}
}
//-----
function RefreshGatewayList()
{
  try
  {
    DestroyTable("adm_gw_table");

    var edit_btn = _User.http_get_unauth('../template/edit_btn_gateway.js');

    var area = document.getElementById("adm_gw_area_select").value;
    var river = document.getElementById("adm_gw_river_select").value;
    var table_list = document.getElementById("adm_gw_table_list");
    table_list.innerHTML = "";

    var index = 1;
    var list = [];
    for(var gateway_suid in _Gatewaylist)
    {
      var obj = Api_GetGatewayData(gateway_suid);

      if(obj != null)
      {
        var btn_id = "gweditbtn_" + obj.uuid + "_" + obj.suid;
        var gw_ukey = obj.uuid + "_" + obj.suid;

        var area_flag  = (area == obj.area)   || (area == "0")  ? true : false;
        var river_flag = (river == obj.river) || (river == "0") ? true : false;

        if(area_flag && river_flag)
        {
          var str = "";
          str += "<tr>";
          str += "<td>"+(obj.name==null?"--":obj.name)+"</td>";
          str += "<td>"+(obj.area==null?"--":obj.area)+"</td>";
          str += "<td>"+(obj.admin==null?"--":obj.admin)+"</td>";
          str += "<td>"+(obj.river==null?"--":obj.river)+"</td>";
          str += "<td>"+(obj.bank==null?"--":obj.bank)+"</td>";
          str += "<td id=base_status_" + gw_ukey +">--</th>";
          str += "<td id=lora_status_" + gw_ukey +">--</th>";
          str += "<td>"+(obj.coordinate==null?"--":obj.coordinate.x)+"</td>";
          str += "<td>"+(obj.coordinate==null?"--":obj.coordinate.y)+"</td>";
          str += "<td>"+ edit_btn.replace("{edit_btn_id}", btn_id) + "</td>";
          str += "</tr>";

          table_list.innerHTML += str;

          index = index + 1;

          list.push({uuid: obj.uuid, suid: obj.suid});
        }
      }
    }

    //----------------------------------------------------------

    for(var i = 0; i < list.length; i++)
    {
      var gw = list[i];
      SensorGetLastData(gw.uuid, "status-" + gw.suid, 1);
    }

    //----------------------------------------------------------

    InitTable("adm_gw_table");
  }
  catch(e){console.log(e);}
}
//-----
function InitModalSetup01()
{
  try
  {
    var modal_setup_template = _User.http_get_unauth('../template/modal_setup_01.js');
    var modal_setup = document.getElementById("modal_setup_01");

    modal_setup.innerHTML = "";
    modal_setup.innerHTML = modal_setup_template;

    //---------------------------------------------

    var admin_select_options = document.getElementById("modal_station_admin_select").options;
    var area_select = document.getElementById("modal_station_area_select");
    var river_select = document.getElementById("modal_station_river_select");

    var area_options  = "";
    var river_options = "";

    for(var i = 0; i < _AreaList.length; i++) {
      var area = _AreaList[i];
      area_options += '<option value={index}>{name}</option>'.replace(/{index}/g, area).replace(/{name}/g, area);
    }

    for(var i = 0; i < _RiverList.length; i++) {
      var river = _RiverList[i];
      river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
    }

    area_select.innerHTML = area_options;
    river_select.innerHTML = river_options;

  }
  catch(e){console.log(e);}
}
//-----
function ShowModalSetup01(info_btn)
{
  try
  {
    InitModalSetup01();

    //--------------------------------------------

    var id = info_btn.id;
    var tmp_array = id.split("_");
    var gateway_uuid = tmp_array[1];
    var gateway_suid = tmp_array[2];

    current_gateway = Api_GetGatewayData(gateway_suid);

    //--------------------------------------------

    document.getElementById("modal_station_tittle_name").innerHTML = current_gateway.name;
    document.getElementById("modal_station_name").value  = current_gateway.name;

    //--------------------------------------------
    
    document.getElementById("modal_station_area_select").value = current_gateway.area;
    document.getElementById("modal_station_river_select").value = current_gateway.river;
    document.getElementById("modal_station_admin_select").value = current_gateway.admin;
    document.getElementById("modal_station_bank_select").value = current_gateway.bank;

    document.getElementById("modal_station_x").value = current_gateway.coordinate.x;
    document.getElementById("modal_station_y").value = current_gateway.coordinate.y;
    document.getElementById("modal_station_coordinate_type_select").options[0].selected = true;

    //--------------------------------------------

  }
  catch(e){console.log(e);}
}
//-----
function UpdateGatewayInfo()
{
  try
  {
    var uuid = current_gateway.uuid;
    var suid = current_gateway.suid;
    var ukey = "setting";

    var obj = JSON.parse(Api_GetCloudData(uuid, suid , ukey));
    var result = Api_GetResultArray(obj);

    if(result != null)
    {
      var name = document.getElementById("modal_station_name").value;
      var area = document.getElementById("modal_station_area_select").value;
      var river = document.getElementById("modal_station_river_select").value;
      var bank  = document.getElementById("modal_station_bank_select").value;
      var admin = document.getElementById("modal_station_admin_select").value;
      var coordinate_x = 0.0;
      var coordinate_y = 0.0;

      if(document.getElementById("modal_station_coordinate_type_select").value == "97")
      {
        coordinate_x = parseFloat(document.getElementById("modal_station_x").value);
        coordinate_y = parseFloat(document.getElementById("modal_station_y").value);
      }
      else
      {
        var wgs84_lng = parseFloat(document.getElementById("modal_station_x").value);
        var wgs84_lat = parseFloat(document.getElementById("modal_station_y").value);
        var p = TGOS.WGS84toTWD97(wgs84_lng, wgs84_lat); 

        coordinate_x = p.x;
        coordinate_y = p.y;
      }

      //---------------------------------------------

      var gateway_setting = result[0];
      gateway_setting.cmd = "set.gateway_setting";
      gateway_setting.name = name;
      gateway_setting.area = area;
      gateway_setting.river = river;
      gateway_setting.bank = bank;
      gateway_setting.admin = admin;
      gateway_setting.coordinate.x = coordinate_x;
      gateway_setting.coordinate.y = coordinate_y;

      var resp = Api_PutCloudEvent(uuid, suid, ukey, gateway_setting);

      if(resp != null && resp == "Ok")
      {
        API_Init();
        RefreshGatewayList();
        alert("更新成功");

        document.getElementById("modal_station_close").click();

        var _log = {};
        _log.type = "system";
        _log.msg = _User.GetCurrentUserID() + " 變更監測站「" + name + "」的資訊";

        _User.addSystemLog(_log);

        return;
      }
    }
  }
  catch(e){console.log(e);}
  alert("更新失敗");
}
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
function InitSensorTable()
{
  try
  {
    var table_list = document.getElementById("adm_sensor_table_list");
    table_list.innerHTML = "";

    var gw_select = document.getElementById("adm_sensor_gw_select");
    var area_select = document.getElementById("adm_sensor_area_select");
    var river_select = document.getElementById("adm_sensor_river_select");
    var sensor_type_select = document.getElementById("adm_sensor_type_select");

    var gw_options    = "<option value='0'>全部</option>";  
    var area_options  = "<option value='0'>全部</option>";
    var river_options = "<option value='0'>全部</option>";
    var sensor_options = "<option value='0'>全部</option>";

    var area_list = [];
    var river_list = [];

    for(var gw in _Gatewaylist) {
      var tmp = _Gatewaylist[gw];
      gw_options += '<option value={index}>{name}</option>'.replace(/{index}/g, tmp.uuid+"_"+tmp.suid).replace(/{name}/g, tmp.name);

      area_list[tmp.area] = tmp.area;
      river_list[tmp.river] = tmp.river;
    }

    for(var area in area_list) {
      area_options += '<option value={index}>{name}</option>'.replace(/{index}/g, area).replace(/{name}/g, area);
    }
    
    for(var river in river_list) {
      river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
    }

    sensor_options += '<option value='+WaterPressure+'>水位計</option>';
    sensor_options += '<option value='+Acceleration+'>加速度計</option>';
    sensor_options += '<option value='+Inclinometer+'>傾斜儀</option>';
    sensor_options += '<option value='+Earthquake+'>地震儀</option>';

    gw_select.innerHTML = gw_options;
    area_select.innerHTML = area_options;
    river_select.innerHTML = river_options;
    sensor_type_select.innerHTML = sensor_options;

    //------------------------------------------------------

    gw_select.onchange = function(){ RefreshSensorList(); };
    area_select.onchange = function(){ RefreshSensorList(); };
    river_select.onchange = function(){ RefreshSensorList(); };
    sensor_type_select.onchange = function(){ RefreshSensorList(); };
  }
  catch(e){console.log(e);}
}
//-----
function RefreshSensorList()
{
  try
  {
    DestroyTable("adm_sensor_table");

    var edit_btn = _User.http_get_unauth('../template/edit_btn_sensor.js');

    var gw = document.getElementById("adm_sensor_gw_select").value;
    var area = document.getElementById("adm_sensor_area_select").value;
    var river = document.getElementById("adm_sensor_river_select").value;
    var type = document.getElementById("adm_sensor_type_select").value;

    var table_list = document.getElementById("adm_sensor_table_list");
    table_list.innerHTML = "";

    var index = 1;
    for(var suid in _Sensorlist)
    {
      var obj = Api_GetSensorData(suid);

      if(obj != null)
      {
        var gw_ukey = obj.gateway_uuid + "_" + obj.gateway_suid;
        var sensor_ukey = obj.uuid + "_" + obj.suid;

        var btn_id = "sensoreditbtn_" + obj.uuid + "_" + obj.suid;

        var area_flag  = (area  == obj.area)  || (area  == "0") ? true : false;
        var river_flag = (river == obj.river) || (river == "0") ? true : false;
        var type_flag  = (type  == obj.type)  || (type  == "0") ? true : false;
        var gw_flag    = (gw    == gw_ukey)   || (gw    == "0") ? true : false;

        if(gw_flag && area_flag && river_flag && type_flag)
        {
          var str = "";
          str += "<tr>";
          str += "<td>"+(obj.name==null?"--":obj.name)+"</td>";
          str += "<td>"+(obj.type_name==null?"--":obj.type_name)+"</td>";
          str += "<td>"+(obj.gateway_name==null?"--":obj.gateway_name)+"</td>";
          str += "<td>"+(obj.area==null?"--":obj.area)+"</td>";
          str += "<td>"+(obj.admin==null?"--":obj.admin)+"</td>";
          str += "<td>"+(obj.river==null?"--":obj.river)+"</td>";
          str += "<td>"+(obj.bank==null?"--":obj.bank)+"</td>";
          str += "<td>"+(obj.coordinate==null?"--":obj.coordinate.x)+"</td>";
          str += "<td>"+(obj.coordinate==null?"--":obj.coordinate.y)+"</td>";
          str += "<td>"+ edit_btn.replace("{edit_btn_id}", btn_id) + "</td>";
          str += "</tr>";

          table_list.innerHTML += str;

          index = index + 1;
        }
      }
    }

    //-------------------------------------------------------

    InitTable("adm_sensor_table");
  }
  catch(e){console.log(e);}
}
//-----
function InitModalSetup02()
{
  try
  {
    var modal_setup_template = _User.http_get_unauth('../template/modal_setup_02.js');
    var modal_setup = document.getElementById("modal_setup_02");

    modal_setup.innerHTML = "";
    modal_setup.innerHTML = modal_setup_template;
  }
  catch(e){console.log(e);}
}
//-----
function ShowModalSetup02(info_btn)
{
  try
  {
    InitModalSetup02();

    //---------------------------------------------

    var id = info_btn.id;
    var tmp_array = id.split("_");
    var sensor_uuid = tmp_array[1];
    var sensor_suid = tmp_array[2];

    current_sensor = Api_GetSensorData(sensor_suid);
    current_gateway = Api_GetGatewayData(current_sensor.gateway_suid);

    //---------------------------------------------

    document.getElementById("modal_sensor_tittle_name").innerHTML = current_sensor.name;

    document.getElementById("modal_sensor_name").value  = current_sensor.name;
    document.getElementById("modal_sensor_type").value  = current_sensor.type_name;
    document.getElementById("modal_sensor_gw").value    = current_sensor.gateway_name;
    document.getElementById("modal_sensor_area").value  = current_sensor.area;
    document.getElementById("modal_sensor_admin").value = current_sensor.admin;
    document.getElementById("modal_sensor_river").value = current_sensor.river;
    document.getElementById("modal_sensor_bank").value  = current_sensor.bank;
    document.getElementById("modal_sensor_remark").value = current_sensor.remark;

    document.getElementById("modal_sensor_x").value = current_sensor.coordinate.x;
    document.getElementById("modal_sensor_y").value = current_sensor.coordinate.y;
    document.getElementById("modal_sensor_coordinate_type_select").options[0].selected = true;


    //---------------------------------------------
  }
  catch(e){console.log(e);}
}
//-----
function UpdateSensorInfo()
{
  try
  {
    var gateway_uuid = current_sensor.gateway_uuid;
    var gateway_suid = current_sensor.gateway_suid;
    var sensor_uuid = current_sensor.uuid;
    var sensor_suid = current_sensor.suid;

    var info_ukey = "info-" + sensor_suid;

    var obj = JSON.parse(Api_GetCloudData(gateway_uuid, gateway_suid, info_ukey));
    var result = Api_GetResultArray(obj);

    if(result != null)
    {
      var coordinate_x = 0.0;
      var coordinate_y = 0.0;
      var name = document.getElementById("modal_sensor_name").value;
      var remark = document.getElementById("modal_sensor_remark").value;

      if(document.getElementById("modal_sensor_coordinate_type_select").value == "97")
      {
        coordinate_x = parseFloat(document.getElementById("modal_sensor_x").value);
        coordinate_y = parseFloat(document.getElementById("modal_sensor_y").value);
      }
      else
      {
        var wgs84_lng = parseFloat(document.getElementById("modal_sensor_x").value);
        var wgs84_lat = parseFloat(document.getElementById("modal_sensor_y").value);
        var p = TGOS.WGS84toTWD97(wgs84_lng, wgs84_lat);

        coordinate_x = p.x;
        coordinate_y = p.y;
      }

      //--------------------------------------------------------

      var sensor_setting = result[0];
      sensor_setting.cmd = "set.sensor_setting";
      sensor_setting.name = name;
      sensor_setting.coordinate.x = coordinate_x;
      sensor_setting.coordinate.y = coordinate_y;
      sensor_setting.remark = remark;

      var result = Api_PutCloudEvent(gateway_uuid, gateway_suid, info_ukey, sensor_setting);

      if(result != null && result == "Ok")
      {
     
        API_Init();
        RefreshSensorList();
        alert("更新成功");

        document.getElementById("modal_sensor_close").click();

        var _log = {};
        _log.type = "system";
        _log.msg = _User.GetCurrentUserID() + " 變更感測器「" + sensor_setting.name + "」的資訊";

        _User.addSystemLog(_log);
      }
    }
  }
  catch(e){console.log(e);}
}
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
function InitCheckListTable()
{
  try
  {
    var table_list = document.getElementById("adm_em_table_list");
    var river_select = document.getElementById("adm_em_river_select");
    var embankment_select = document.getElementById("adm_em_embankment_select");
    var search_btn = document.getElementById("cl_search_btn");
    var create_btn  = document.getElementById("create_checklist_btn");
    
    table_list.innerHTML = "";
    river_select.innerHTML = "";
    embankment_select.innerHTML = "";

    var river_options = "<option value='-1'>全部</option>";
    var embankment_options = "<option value='-1'>全部</option>";

    for(var river in _RiverEmbankmentList){
      river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
    }

    river_select.innerHTML = river_options;
    embankment_select.innerHTML = embankment_options;

    //------------------------------------------------------

    river_select.options[0].selected = true;

    OnRiverSelectChange();

    //------------------------------------------------------

    var arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }

    $('#m_datepicker_1, #m_datepicker_2').datepicker({
        language: 'zh-TW',
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows,
        format: 'yyyy/mm/dd'
    });

    $('#m_datepicker_1').datepicker('update', moment(new Date()).subtract(7,'d').format('YYYY-MM-DD'));
	$('#m_datepicker_2').datepicker('update', new Date());

	var m_datepicker_1 = document.getElementById("m_datepicker_1");
	var m_datepicker_2 = document.getElementById("m_datepicker_2");

	document.getElementById("m_datepicker_1").onchange = function(){ CheckDateTimeRange1(); };
	document.getElementById("m_datepicker_2").onchange = function(){ CheckDateTimeRange2(); };

	m_datepicker_1.time_range_flag = m_datepicker_2.time_range_flag = false;

  }
  catch(e){console.log(e);}
}
//-----
function RefreshCheckList()
{
  try
  {
    DestroyTable("adm_em_table");

    var uuid = "opendata";
    var suid = "checklist";
    var sh_uuid = uuid + "." + suid;

    var s_date = $("#m_datepicker_1").val();
    var e_date = $("#m_datepicker_2").val();
    var s_time = "00:00:00.000";
    var e_time = "23:59:59.999";
    var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
    var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

    var sh_start = start - (30 * 86400 * 1000);
    var sh_end = end + (7 * 86400 * 1000);

    var river = document.getElementById("adm_em_river_select").value;
    var embankment = document.getElementById("adm_em_embankment_select").value;

    //-------------------------------------------------------------------------------------

    em_check_list = [];

    //var resp = _User.getData(sh_uuid, sh_start, sh_end, null);
    var resp = Api_GetCloudDataByTime(uuid, suid, sh_start, sh_end, null);
   
    if(resp != null && resp.length > 0)
    {
      var resp = JSON.parse(resp);
      var results = resp.results;

      for(var i = 0; i < results.length; i++)
      {
        var item = results[i];

        var river_flag      = (river == item.river) || (river == "-1") ? true : false;
        var embankment_flag = (embankment == item.em_id) || (embankment == "-1") ? true : false;
        var time_flag       = (item.time >= start && item.time <= end) ? true : false;

        if(river_flag && embankment_flag && time_flag)
        {
          em_check_list.push(item);
        }
      }
    }

    //-------------------------------------------------------------------------------------

    var edit_btn = _User.http_get_unauth('../template/edit_btn_checklist2.js');

    var table_list = document.getElementById("adm_em_table_list");
    table_list.innerHTML = "";

    for(var i = 0; i < em_check_list.length; i++)
    {
      var item = em_check_list[i];

      var str = "";
      str += "<tr>";
      str += "<td>" + (item.time == null ? "--" : TimestampToDate(item.time)) + "</td>";
      str += "<td>" + ((item.admin== null || item.admin == "") ? "--" : item.admin) + "</td>";
      str += "<td>" + ((item.em_name == null ||　item.em_name == "") ? "--" : item.em_name) + "</td>";
      str += "<td>" + ((item.type == null || item.type == "") ? "--" : item.type) + "</td>";
      str += "<td>" + (item.pile == null ? "--" : item.pile) + "</td>";
      str += "<td>" + ((item.river == null || item.river == "") ? "--" : item.river) +"</td>";
      str += "<td>" + (item.coordinate == null ? "--" : item.coordinate.x) + "</td>";
      str += "<td>" + (item.coordinate == null ? "--" : item.coordinate.y) + "</td>";
      str += "<td>" + (item.inspection == null ? "--" : item.inspection) +"</td>";
      str += "<td>" + ((item.security == null || item.security == "") ? "--" : item.security) + "</td>";
      str += "<td>" + (item.description == null ? "--" : item.description) + "</td>";
      str += "<td>" + (item.improve == null ? "--" : item.improve) + "</td>";
      str += edit_btn.replace("{edit_btn_id}", "checklist_" + i).replace("{file_btn_id}", "file_" + i).replace("{image_btn_id}", "image_" + i);
      str += "</tr>";

      table_list.innerHTML += str;
    }

    //----------------------------------------------------------------------------------------

    InitTable("adm_em_table");
  }
  catch(e){console.log(e);}
}
//-------------
function InitModalCheckList01()
{
  try
  {
    var n_time = new Date().getTime();

    checklist_flag = true;
    current_checklist = null;
    current_checklist = {};
    current_checklist.id = n_time.toString();
    current_checklist.ukey = null;
    current_checklist.edit_date = n_time;

    var modal_setup_template = _User.http_get_unauth('../template/modal_checklist_02.js');
    var modal_setup = document.getElementById("modal_checklist_01");

    modal_setup.innerHTML = "";
    modal_setup.innerHTML = modal_setup_template;

    //------------------------------------------------------

    document.getElementById("modal_cl_delete").style.display = "none";

    document.getElementById("modal_cl_description_textarea").value = "";
    document.getElementById("modal_cl_improve_textarea").value = "";
    document.getElementById("modal_cl_inspection_text").value = "";
    document.getElementById("modal_cl_creater").value = _User.GetCurrentUserName();
    document.getElementById("modal_cl_editor").value = _User.GetCurrentUserName();
    document.getElementById("modal_cl_edit_date").value = TimestampToDate(n_time);
    
    document.getElementById("modal_cl_admin_select").options[0].selected = true;
    document.getElementById("modal_cl_security_select").options[0].selected = true;
    document.getElementById("modal_cl_type_select").options[0].selected = true;
    document.getElementById("modal_cl_pile_text").value = "";

    document.getElementById("modal_cl_embankment_X").value = "";
    document.getElementById("modal_cl_embankment_Y").value = "";

    //------------------------------------------

    var river_select = document.getElementById("modal_cl_river_select");
    var river_options = "<option value='-1'>必填</option>";

    for(var river in _RiverEmbankmentList){
      river_options += '<option value={index}>{name}</option>'.replace(/{index}/g, river).replace(/{name}/g, river);
    }

    river_select.innerHTML = river_options;

    //---------------------------------------------

    river_select.options[0].selected = true;
    OnModalRiverSelectChange();

    //---------------------------------------------

    var arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }

    $('#m_datepicker_checkday').datepicker({
        language: 'zh-TW',
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows,
        format: 'yyyy/mm/dd'
    });

    $('#m_datepicker_checkday').datepicker('update', new Date());

    //------------------------------------------

    cl_file_array = [];
    cl_image_array = [];
  }
  catch(e){console.log(e);}
}
//-------------
function ShowModalCheckList01(info_btn)
{
  try
  {
    InitModalCheckList01();

    //------------------------------------------

    var id = info_btn.id;
    var array = id.split("_");
    var index = parseInt(array[1]);

    var check_list = em_check_list[index];
    current_checklist = check_list;

    checklist_flag = false;

    //------------------------------------------

    document.getElementById("modal_cl_delete").style.display = "";

    $('#m_datepicker_checkday').datepicker('update', new Date(current_checklist.time));

    document.getElementById("modal_cl_improve_textarea").value = current_checklist.improve;
    document.getElementById("modal_cl_inspection_text").value = current_checklist.inspection;
    document.getElementById("modal_cl_description_textarea").value = current_checklist.description;

    document.getElementById("modal_cl_creater").value = current_checklist.creater;
    document.getElementById("modal_cl_editor").value = current_checklist.editor;
    document.getElementById("modal_cl_edit_date").value = TimestampToDate(current_checklist.edit_date);

    document.getElementById("modal_cl_embankment_X").value = current_checklist.coordinate.x;
    document.getElementById("modal_cl_embankment_Y").value = current_checklist.coordinate.y;

    document.getElementById("modal_cl_pile_text").value = current_checklist.pile;

    //---------------------------------------------

    var river_select = document.getElementById("modal_cl_river_select");
    var type_select = document.getElementById("modal_cl_type_select");
    var admin_select = document.getElementById("modal_cl_admin_select");
    var security_select = document.getElementById("modal_cl_security_select");
    var embankment_select = document.getElementById("modal_cl_embankment_select");

    river_select.value = current_checklist.river;
    if(river_select.selectedIndex == -1) { river_select.options[0].selected = true; }

    OnModalRiverSelectChange(); 

    type_select.value = current_checklist.type;
    admin_select.value = current_checklist.admin;
    security_select.value = current_checklist.security;
    embankment_select.value = current_checklist.em_id;

    if(type_select.selectedIndex == -1)  { type_select.options[0].selected = true; }
    if(admin_select.selectedIndex == -1)  { admin_select.options[0].selected = true; }
    if(security_select.selectedIndex == -1)  { security_select.options[0].selected = true; }
    if(embankment_select.selectedIndex == -1)  { embankment_select.options[0].selected = true; }

    //---------------------------------------------

    OnModalEmbankmentSelectChange();

    //---------------------------------------------

    var checklist_file_group1 = _User.http_get_unauth('../template/checklist_file_group1.js');
    var checklist_file_group2 = _User.http_get_unauth('../template/checklist_file_group2.js');

    var checklist_file_list = document.getElementById("checklist_file_list");
    var checklist_image_list = document.getElementById("checklist_image_list");
    checklist_file_list.innerHTML = "";
    checklist_image_list.innerHTML = "";

    //--------------------------------------------

    var file_list = current_checklist.file_list;
    var image_list = current_checklist.image_list;

    if(file_list != null)
    {
      for(var i = 0; i < file_list.length; i++)
      {
        var file = file_list[i];
        file.status = "none";

        cl_file_array.push(file);

        checklist_file_list.innerHTML += checklist_file_group1.replace(/{name}/g, file.name).replace(/{index}/g, i);
      }
    }

    if(image_list != null)
    {
      for(var i = 0; i < image_list.length; i++)
      {
        var image = image_list[i];
        image.status = "none";

        cl_image_array.push(image);

        checklist_image_list.innerHTML += checklist_file_group2.replace(/{name}/g, image.name).replace(/{index}/g, i);
      }
    }
  }
  catch(e){console.log(e);}
}
//----
function UpdateCheckList()
{
  try
  {
    var admin = document.getElementById("modal_cl_admin_select").value;
    var river = document.getElementById("modal_cl_river_select").value;
    var em_typ = document.getElementById("modal_cl_type_select").value;
    var security = document.getElementById("modal_cl_security_select").value;
    var em_tmp = document.getElementById("modal_cl_embankment_select").value;
    var em_select = document.getElementById("modal_cl_embankment_select");

    if(admin == "0")    { alert("請選擇管理單位"); return; }
    if(security == "0") { alert("請選擇安全等級"); return; }
    if(river  == "-1")   { alert("請選擇河川");     return; }
    if(em_tmp == "-1")   { alert("請選擇堤防");     return; }
    if(em_typ == "0")   { alert("請選擇堤防型式"); return; }

    //-------------------------------------------

    var date = $('#m_datepicker_checkday').val();
    var time = moment(date + " 00:00:00.000", "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

    //------------------------------------------

    current_checklist.time = time;

    current_checklist.pile = document.getElementById("modal_cl_pile_text").value;
    current_checklist.improve =  document.getElementById("modal_cl_improve_textarea").value;
    current_checklist.inspection = document.getElementById("modal_cl_inspection_text").value;
    current_checklist.description = document.getElementById("modal_cl_description_textarea").value;

    current_checklist.coordinate = {};
    current_checklist.coordinate.x = parseFloat(document.getElementById("modal_cl_embankment_X").value);
    current_checklist.coordinate.y = parseFloat(document.getElementById("modal_cl_embankment_Y").value);

    current_checklist.creater = document.getElementById("modal_cl_creater").value;
    current_checklist.editor = _User.GetCurrentUserName();
    current_checklist.edit_date = new Date().getTime();

    current_checklist.admin = (admin == "0") ? "" : admin;
    current_checklist.river = (river == "-1") ? "" : river;
    current_checklist.security = (security == "0") ? "" : security;
    current_checklist.type = (em_typ == "0") ? "" : em_typ;
  
    current_checklist.em_name = em_select.options[em_select.selectedIndex].text;
    current_checklist.em_id = em_select.options[em_select.selectedIndex].value;

    //---------------------------------------------

    var tmp_file_list = [];
    var tmp_image_list = [];

    for(var i = 0; i < cl_file_array.length; i++)
    {
      var cl_file = cl_file_array[i];
      if(cl_file.status == "none" || cl_file.status == "add")
      {
        var tmp = {};
        tmp.id = cl_file.id;
        tmp.name = cl_file.name;
        tmp.path = cl_file.path;
        tmp_file_list.push(tmp);
      }
    }

    for(var i = 0; i < cl_image_array.length; i++)
    {
      var cl_image = cl_image_array[i];
      if(cl_image.status == "none" || cl_image.status == "add")
      {
        var tmp = {};
        tmp.id = cl_image.id;
        tmp.name = cl_image.name;
        tmp.path = cl_image.path;

        tmp_image_list.push(tmp);
      }
    }

    current_checklist.file_list = tmp_file_list;
    current_checklist.image_list = tmp_image_list;

    //---------------------------------------------

    var uuid = "opendata";
    var suid = "checklist";

    Api_PutCloudData(uuid, suid, current_checklist.ukey, current_checklist);

    //---------------------------------------------

    var file_payload = {};
    file_payload.type = "check"
    file_payload.image_list = cl_file_array;

    Api_UploadDikeImage(file_payload);

    //-------------------------------------------------

    var image_payload = {};
    image_payload.type = "image"
    image_payload.image_list = cl_image_array;

    Api_UploadDikeImage(image_payload);

    //---------------------------------------------

    API_Init();
    RefreshCheckList();

    document.getElementById("modal_cl_close").click();

    //---------------------------------------------

    if(checklist_flag == true){

      var _log = {};
      _log.type = "system";
      _log.msg = _User.GetCurrentUserID() + " 新增安全檢查表「" + date + "_" + current_checklist.em_name + "」";

      _User.addSystemLog(_log);
    }
    else
    {
      var _log = {};
      _log.type = "system";
      _log.msg = _User.GetCurrentUserID() + " 變更安全檢查表「" + date + "_" + current_checklist.em_name + "」的資訊";

      _User.addSystemLog(_log);
    }
  }
  catch(e){console.log(e);}
}
//-------------
function DeleteCheckList()
{
  try
  {
    var uuid = "opendata";
    var suid = "checklist";
    var ukey = current_checklist.ukey;
    var date = TimestampToDate(current_checklist.time);

    if(confirm("確認是否刪除此檢查表「" + date + "_" + current_checklist.em_name + "」？") == true)
    {
      Api_DeleteCloudData(uuid, suid, ukey);

      //-------------------------------------------------

      for(var i = 0; i < cl_file_array.length; i++)
      {
        if(cl_file_array[i].status == "add") {
          cl_file_array.splice(i, 1);
          i = i-1;
        }
        else{ cl_file_array[i].status = "delete"; }
      }

      for(var i = 0; i < cl_image_array.length; i++)
      {
        if(cl_image_array[i].status == "add") {
          cl_image_array.splice(i, 1);
          i = i-1;
        }
        else{ cl_image_array[i].status = "delete"; }
      }

      //-------------------------------------------------

      var file_payload = {};
      file_payload.type = "check"
      file_payload.image_list = cl_file_array;

      Api_UploadDikeImage(file_payload);

      //-------------------------------------------------

      var image_payload = {};
      image_payload.type = "image"
      image_payload.image_list = cl_image_array;

      Api_UploadDikeImage(image_payload);

      //-------------------------------------------------

      API_Init();
      RefreshCheckList();

      document.getElementById("modal_cl_close").click();

      var _log = {};
      _log.type = "system";
      _log.msg = _User.GetCurrentUserID() + " 刪除安全檢查表「" + date + "_" + current_checklist.em_name + "」";

      _User.addSystemLog(_log);
    }
  }
  catch(e){console.log(e);}
}
//-------------
function LoadFileList()
{
  try
  {
    var checklist_file_group1 = _User.http_get_unauth('../template/checklist_file_group1.js');
    var checklist_file_list = document.getElementById("checklist_file_list");
    checklist_file_list.innerHTML = "";

    for(var i = 0; i < cl_file_array.length; i++)
    {
      var file = cl_file_array[i];
      if(file.status != "delete")
      {
        checklist_file_list.innerHTML += checklist_file_group1.replace(/{name}/g, file.name).replace(/{index}/g, i);
      }
    }
  }
  catch(e){console.log(e);}
}
//-------------
function LoadImageList()
{
  try
  {
    var checklist_file_group2 = _User.http_get_unauth('../template/checklist_file_group2.js');
    var checklist_image_list = document.getElementById("checklist_image_list");
    checklist_image_list.innerHTML = "";

    for(var i = 0; i < cl_image_array.length; i++)
    {
      var image = cl_image_array[i];
      if(image.status != "delete")
      {
        checklist_image_list.innerHTML += checklist_file_group2.replace(/{name}/g, image.name).replace(/{index}/g, i);
      }
    }
  }
  catch(e){console.log(e);}
}
//---------------
function AddCheckListFile()
{
  try
  {
    var custom_file = document.getElementById("customFile_checklist");

    var freader = new FileReader();
    freader.onloadend = function(event) 
    {
      var image_info = {};
      image_info.id = "CL_LIST_" +  current_checklist.id + "_" + new Date().getTime() + "_" + cl_file_array.length;
      image_info.name = custom_file.files[0].name;
      image_info.status = "add";
      image_info.image = event.target.result;
      image_info.path = "/scripts/river_system/dashboard/dike/check/" + image_info.id + ".jpeg";

      cl_file_array.push(image_info);

      LoadFileList();
    }

    freader.readAsDataURL(custom_file.files[0]);
  }
  catch(e){console.log(e);}
}
//-----
function AddCheckListImage()
{
  try
  {
    var custom_file = document.getElementById("customFile_img");

    var freader = new FileReader();
    freader.onloadend = function(event) 
    {
      var image_info = {};
      image_info.id = "CL_IMAGE_" + current_checklist.id + "_" + new Date().getTime() + "_" + cl_image_array.length;
      image_info.name = custom_file.files[0].name;
      image_info.status = "add";
      image_info.image = event.target.result;
      image_info.path = "/scripts/river_system/dashboard/dike/image/" + image_info.id + ".jpeg";

      cl_image_array.push(image_info);

      LoadImageList();
    }

    freader.readAsDataURL(custom_file.files[0]);
  }
  catch(e){console.log(e);}
}
//-----
function DeleteCheckListFile(index)
{
  try
  {
    if(cl_file_array[index].status == "add")
    {
      cl_file_array.splice(index, 1);
    }
    else
    {
      cl_file_array[index].status = "delete";
    }

    LoadFileList();
  }
  catch(e){console.log(e);}
}
//-----
function DeleteCheckListImage(index)
{
  try
  {
    if(cl_image_array[index].status == "add")
    {
      cl_image_array.splice(index, 1);
    }
    else
    {
      cl_image_array[index].status = "delete";
    }

    LoadImageList();
  }
  catch(e){console.log(e);}
}
//-----
function ShowModalCheckListFile(index)
{
  try
  {
    var iamge_data = "/scripts/river_system/dashboard/image/Image.jpg";


    var file = cl_file_array[index];

    //-----------------------------------------------------------------------

    var check_list_image = document.getElementById("check_list_image");
    check_list_image.innerHTML = "";

    if(file != null)
    {
      var path = file.path;
      var status = file.status;

      if(status == "none")
        check_list_image.innerHTML += "<img src=" + path +">";
      else if(status == "add")
        check_list_image.innerHTML += "<img src=" + file.image +">";
    }
    else
    {
      check_list_image.innerHTML = "<img src=" + iamge_data +">";
    }
  }
  catch(e){console.log(e);}
}
//-----
function ShowModalCheckListImage(index)
{
  try
  {
    var iamge_data = "/scripts/river_system/dashboard/image/Image.jpg";

    var image = cl_image_array[index];

    //-----------------------------------------------------------------------

    var check_list_image = document.getElementById("check_list_image");
    check_list_image.innerHTML = "";

    if(image != null)
    {
      var path = image.path;
      var status = image.status;

      if(status == "none")
        check_list_image.innerHTML += "<img src=" + path +">";
      else if(status == "add")
        check_list_image.innerHTML += "<img src=" + image.image +">";
    }
    else
    {
      check_list_image.innerHTML = "<img src=" + iamge_data +">";
    }
  }
  catch(e){console.log(e);}
}
//-----
function OnRiverSelectChange()
{
  try
  {  
    var river_value = document.getElementById("adm_em_river_select").value;

    var embankment_select = document.getElementById("adm_em_embankment_select");
    var embankment_options = "<option value='-1'>全部</option>";

    if(embankment_select != null)
    {
      embankment_select.innerHTML = "";

      for(var river_id in _RiverEmbankmentList) 
      {
        if(river_value == river_id || river_value == "-1")
        {
          var river_item = _RiverEmbankmentList[river_id];
          var embankment_list = river_item.embankment;

          for(var em_id in embankment_list)
          {
            var embankment = embankment_list[em_id];
            embankment_options += '<option value={index}>{name}</option>'.replace(/{index}/g, embankment.ukey).replace(/{name}/g, embankment.name);
          }
        }
      }

      embankment_select.innerHTML = embankment_options;
      embankment_select.options[0].selected = true;
    }
  }
  catch(e){console.log(e);}
}
//-----
function OnModalRiverSelectChange()
{
  try
  {  
    var river_value = document.getElementById("modal_cl_river_select").value;

    var embankment_select = document.getElementById("modal_cl_embankment_select");
    var embankment_options = "<option value='-1'>必填</option>";

    if(embankment_select != null)
    {
      embankment_select.innerHTML = "";

      if(river_value != "-1")
      {
        var river_item = _RiverEmbankmentList[river_value];
        var embankment_list = river_item.embankment;
        for(var em_id in embankment_list)
        {
          var embankment = embankment_list[em_id];
          embankment_options += '<option value={index}>{name}</option>'.replace(/{index}/g, embankment.ukey).replace(/{name}/g, embankment.name);
        }
      }
      embankment_select.innerHTML = embankment_options;
    }

    embankment_select.options[0].selected = true;

    OnModalEmbankmentSelectChange();
  }
  catch(e){console.log(e);}
}
//-------------
function OnModalEmbankmentSelectChange()
{
  try
  {
    document.getElementById("modal_cl_embankment_X").value = "";
    document.getElementById("modal_cl_embankment_Y").value = "";

    var river_value = document.getElementById("modal_cl_river_select").value;
    var em_id = document.getElementById("modal_cl_embankment_select").value;

    var river = _RiverEmbankmentList[river_value];

    if(river_value != "-1" && river != null)
    {
      var embankment = river.embankment[em_id];

      if(embankment != null)
      {
        var p = TGOS.WGS84toTWD97(embankment.points[0].lng, embankment.points[0].lat); 

        document.getElementById("modal_cl_embankment_X").value = p.x;
        document.getElementById("modal_cl_embankment_Y").value = p.y;
      }
    }
  }
  catch(e){console.log(e);}
}
//-----
function UpdateGatewayStatus(gateway)
{
  try
  {
    var ukey = gateway.uuid + "_" + gateway.suid;

    var td_base_status = document.getElementById("base_status_" + ukey);
    var td_lora_status = document.getElementById("lora_status_" + ukey);

    //------------------------------------------------------------------------

    var status_info = gateway.status_info;
    var base_tunnel = (status_info.base_tunnel == null) ?　{} : status_info.base_tunnel;
    var lora_tunnel = (status_info.lora_tunnel == null) ?　{} : status_info.lora_tunnel;
    var base_status = (base_tunnel.status == null) ? "--" : base_tunnel.status;
    var lora_status = (lora_tunnel.status == null) ? "--" : lora_tunnel.status;
    var base_status_str = GetStatusString(base_status);
    var lora_status_str = GetStatusString(lora_status);

    if(td_base_status != null)  td_base_status.innerHTML = base_status_str;
    if(td_lora_status != null)  td_lora_status.innerHTML = lora_status_str;

    //--------------------------------------------
  }
  catch(e){console.log(e);}
}
//-----
function GetStatusString(status)
{       
  try
  {

    var status_str = "--";  
    if(status == "normal")      status_str = "正常";
    if(status == "error")       status_str = "設備異常";
    if(status == "data_error")  status_str = "數據錯誤";
    if(status == "data_warning")status_str = "超出安全值";
    if(status == "disconnect")  status_str = "斷線";
    if(status == "timeout")     status_str = "斷線";
    if(status == "connecting")  status_str = "連線中";
    if(status == "pending")     status_str = "等待連線";

    return status_str;
  }
  catch(e){console.log(e);}
}
//-----
function OnSensorsData(payload)
{       
  try
  {
    var payload = JSON.parse(payload.data);
    var data = payload.data == null ? payload : payload.data;
    var value = data.values != null ? data.values[0] : data.results[0];
    var realtime = (data.flag == null) ? true : false;

    if(data.uuid != null && data.uuid.length > 0 && data.suid != null && data.suid.length > 0 && value != null)
    {
      var uuid = data.uuid;
      var suid = data.suid;
      var event_type = (suid.indexOf("status") != -1) ? "status" : "value";
      if(event_type == "status") { suid = suid.split("status-")[1]; }

      if(data.uuid == "Gateway")
      {
        var gateway = Api_GetGatewayData(suid);

        if(gateway != null)
        {
          if(event_type == "status") 
          {
            gateway.status_info = value;
            UpdateGatewayStatus(gateway);
          }
        }
      }
    }
  }
  catch(e){console.log(e);}
}
//-----
function SensorGetLastData(uuid, suid, count)
{
  try
  {
    var resp = Api_GetCloudLastData(uuid, suid, count);

    if(resp != null && resp.length > 0) 
    { 
      resp = JSON.parse(resp);
      resp.uuid = uuid;
      resp.suid = suid;
      resp.flag = false;

      var content = {};
      content.data = JSON.stringify(resp); 

      OnSensorsData(content);
    }
  }
  catch(e){console.log(e);}
}
//-----
function SubscribeData()
{
  try
  {
    Api_SubscribeData('*', OnSensorsData);
    //_User.SubscribeData('*', OnSensorsData);
  }
  catch(e){console.log(e);}
}
function TimestampToDate(timestamp)
{
  try
  {
    var _datetime = new Date(timestamp);

    var _year = _datetime.getFullYear();
    var _month = _datetime.getMonth() + 1;
    var _date = _datetime.getDate();

    var _yearStr  = _datetime.getFullYear();
    var _monthStr = ( _month < 10) ? ('0'+ _month) : ('' + _month);
    var _dateStr  = ( _date < 10)  ? ('0'+ _date)  : ('' + _date);
 
    var _datetimeStr = _yearStr + '/' + _monthStr +  '/' + _dateStr;

    return _datetimeStr;
  }
  catch(e){console.log(e);}

  return null;
}
//-----
function InitTable(table_id) 
{
  try
  {
    var table = "#" + table_id;

    $(table).DataTable({
      scrollCollapse: true,
      scrollX: true,
      retrieve: true,
      destroy: true,
      order: [[ 0, 'desc' ]],

      //== DOM Layout settings-筆數
      dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",

      lengthMenu: [5, 10, 20, 30, 50],
      pageLength: 20,
      language: {
	    "loadingRecords": "載入中...",
	    'lengthMenu': '資料筆數 _MENU_',
	    "zeroRecords":  "沒有符合的結果",
	    "info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
	    "infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
	    "infoFiltered": "(從 _MAX_ 項結果中過濾)",
	    "infoPostFix":  "",
	    "search":       "搜尋:",
	    /*"paginate": {
	        "first":    "第一頁",
	        "previous": "上一頁",
	        "next":     "下一頁",
	        "last":     "最後一頁"
	    },*/
	    "aria": {
	        "sortAscending":  ": 升冪排列",
	        "sortDescending": ": 降冪排列"
	    }
      }
    });
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
       $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
  }
  catch(e){console.log(e);}
}
//-----
function DestroyTable(table_id) 
{
  try
  {
    var table = "#" + table_id;
    if($.fn.DataTable.isDataTable(table)) 
    {
      $(table).DataTable().destroy();

      $(table_id + ' tbody').empty();
    }
  }
  catch(e){console.log(e);}
}
//-----
function CheckDateTimeRange1()
{
	try
	{
		var m_datepicker_1 = document.getElementById("m_datepicker_1");
		var m_datepicker_2 = document.getElementById("m_datepicker_2");
		var m_timepicker_1 = document.getElementById("m_timepicker_1");
		var m_timepicker_2 = document.getElementById("m_timepicker_2");

		if(m_datepicker_1.time_range_flag == true) { return; }

		m_datepicker_1time_range_flag = true;

		var s_date = (m_datepicker_1 != null) ? $("#m_datepicker_1").val() : "";
		var e_date = (m_datepicker_2 != null) ? $("#m_datepicker_2").val() : "";
		var s_time = (m_timepicker_1 != null) ? $('#m_timepicker_1').val() + ":00.000" : "";
		var e_time = (m_timepicker_2 != null) ? $('#m_timepicker_2').val() + ":59.999" : "";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

		if(start > end)
		{
			if(m_datepicker_2 != null) { $('#m_datepicker_2').datepicker('update', s_date); }
			if(m_timepicker_2 != null) { $('#m_timepicker_2').timepicker('setTime', s_time); }
		}

		m_datepicker_1.time_range_flag = false;

	}
	catch(e){console.log(e);}
}
//-----
function CheckDateTimeRange2()
{
	try
	{
		var m_datepicker_1 = document.getElementById("m_datepicker_1");
		var m_datepicker_2 = document.getElementById("m_datepicker_2");
		var m_timepicker_1 = document.getElementById("m_timepicker_1");
		var m_timepicker_2 = document.getElementById("m_timepicker_2");

		if(m_datepicker_1.time_range_flag == true) { return; }

		document.getElementById("m_datepicker_1").time_range_flag = true;

		var s_date = (m_datepicker_1 != null) ? $("#m_datepicker_1").val() : "";
		var e_date = (m_datepicker_2 != null) ? $("#m_datepicker_2").val() : "";
		var s_time = (m_timepicker_1 != null) ? $('#m_timepicker_1').val() + ":00.000" : "";
		var e_time = (m_timepicker_2 != null) ? $('#m_timepicker_2').val() + ":59.999" : "";
		var start = moment(s_date + " " + s_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();
		var end   = moment(e_date + " " + e_time, "YYYY/MM/DD HH:mm:ss.SSS").utc().valueOf();

		if(start > end)
		{
			if(m_datepicker_1 != null) { $('#m_datepicker_1').datepicker('update', e_date); }
			if(m_timepicker_1 != null) { $('#m_timepicker_1').timepicker('setTime', e_time); }
		}

		document.getElementById("m_datepicker_1").time_range_flag = false;

	}
	catch(e){console.log(e);}
}
//-----